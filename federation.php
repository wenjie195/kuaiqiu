<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://kuaiqiu.tech/federation.php" />
<link rel="canonical" href="https://kuaiqiu.tech/federation.php" />
<meta property="og:title" content="Federation | Kuai Qiu" />
<title>Federation | Kuai Qiu</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding min-height grey-bg menu-distance overflow">
	
<h1 class="green-text h1-title margin-bottom0 text-center">选择足协</h1>  
    <div class="width100 text-center">
    	<div class="green-border margin-bottom30 margin-auto"></div>
    </div>
  <!-- <form action="match.php" method="POST" target="_blank"> -->
  <form action="match.php" method="POST" >
    <div class="width100 text-center margin-bottom30">
    <p class="input-top-p ow-grey-text">足协种类</p>
      <select id="federation_type" class="select-width" name="federation_type">
        <option value="UEFA">UEFA / 欧洲</option>
        <option value="CAF">CAF / 非洲</option>
        <option value="OFC">OFC / 大洋洲</option>
        <option value="CONMEBOL">CONMEBOL / 南美洲</option>
        <option value="CONCACAF">CONCACAF / 中北美洲及加勒比海足球协会</option>
        <option value="AFC">AFC / 亚洲</option>
      </select>
    </div>


    <div class="width100 text-center">    
      <button class="pill-button green-bg white-text clean fix-width-button opacity-hover"  name="Submit">提交</button> 
    </div>

  </form>      

  <div class="clear"></div>
    <div class="width100 text-center">
    	<a href="matchEuro.php"><img src="img/euro2020.png" class="euro-link-img opacity-hover"></a>
        <p class="euro-link-p"><a href="matchEuro.php" class="green-link">View Win Against Table</a></p>
    </div>     

    <div class="clear"></div>

    <div class="width100 text-center">
        <p class="euro-link-p"><a href="matchEuroGS.php" class="green-link">View Group State Winning</a></p>
    </div> 

    <div class="clear"></div>

</div>
<?php include 'js.php'; ?>

</body>
</html>