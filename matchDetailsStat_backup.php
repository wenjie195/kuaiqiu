<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$matchID = $_SESSION['match_id'];
$originalUri = "https://football-prediction-api.p.rapidapi.com/api/v2/predictions/".$matchID."";

$curl = curl_init();

curl_setopt_array($curl, [

	CURLOPT_URL => $originalUri,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => [
		"x-rapidapi-host: football-prediction-api.p.rapidapi.com",
		"x-rapidapi-key: 16c81199b8msh057448939d0cc57p135fd8jsn8c6fb196197a"
	],
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	// echo $response;
	$exchangeRates = json_decode($response, true);
}

?>

<div class="width100 same-padding min-height grey-bg menu-distance overflow ow-same-padding">

	<?php
	if ($exchangeRates)
	{
		for ($cnt=0; $cnt <count($exchangeRates['data']) ; $cnt++)
		{
		?>
			<h1 class="black-text stadium-title text-center margin-bottom0">
				<?php echo $exchangeRates['data'][$cnt]['competition_cluster']; ?> - <?php echo $exchangeRates['data'][$cnt]['competition_name']; ?>
			</h1>

			<?php $datetime = $exchangeRates['data'][$cnt]['start_date']; ?>

			<div class="text-center width100"></div>

			<div class="overflow-div width100\">
				<div class="width100 overflow text-center">
                    <h1 class="team-title team-title-left ow-black-text"><?php echo $exchangeRates['data'][$cnt]['home_team']; ?></h1>
                    <img src="img/vs.png" class="vs" alt="vs" title="vs">
                    <h1 class="team-title team-title-right ow-black-text"><?php echo $exchangeRates['data'][$cnt]['away_team']; ?></h1>
				</div>
				<div class="overflow-div width100">
					<table class="odds-table2 small-text-table">	
						<!-- <tr>
							<td colspan="3" class=" td-title"></td>
							<td colspan="3" class="font-weight900 td-title">Statistical Odds</td>
						</tr>
						<tr>
							<td colspan="3" class=" td-title"></td>
							<td class="font-weight900 td-title">AI Generated Odds</td>
							<td colspan="2" class="font-weight900 td-title">Bookmarker Odds</td>
						</tr>   -->

						<tr>
							<td colspan="3" class=" td-title"></td>
							<td colspan="3" class="font-weight900 td-title">Statistical Odds</td>
						</tr>

						<tr>
							<td class="font-weight900 td-title">Date</td>
							<td class="font-weight900 td-title">Time</td>
							<td class="font-weight900 td-title">Match</td>
							<!-- <td class="font-weight900 td-title">Match Prediction</td> -->

							<!-- <td class="font-weight900 td-title">Home - Away</td> -->

							<!-- <td class="font-weight900">Bet365</td>
							<td class="font-weight900">WilliamHill</td> -->

							<td class="td-title">主队取胜赔率</td>
							<td class="td-title">客队取胜赔率</td>

							<!-- <td class="td-title">Bet365</td>
							<td class="td-title">WilliamHill</td> -->

							<!--<td class="font-weight900">Yes</td>
							<td class="font-weight900">No</td>
							<td class="font-weight900">Yes</td>
							<td class="font-weight900">No</td>                         
							<td  class="font-weight900">1</td>
							<td  class="font-weight900">X</td>
							<td  class="font-weight900">2</td> -->                         
						</tr>     
						<tr>
							<td><?php echo $date = date("Y-m-d",strtotime($datetime));?></td>
							<td><?php echo $time = date("H:i",strtotime($datetime));?></td>

							<!-- <td><?php //echo $date = date("Y-m-d",strtotime($datetime));?> <?php //echo $time = date("H:i",strtotime($datetime));?></td> -->

							<td><?php echo $exchangeRates['data'][$cnt]['home_team']; ?> vs <?php echo $exchangeRates['data'][$cnt]['away_team']; ?></td>
							<?php $homeStrength = $exchangeRates['data'][$cnt]['home_strength']; ?> <?php $awayStrength = $exchangeRates['data'][$cnt]['away_strength']; ?>
							<?php $homeStr = sprintf("%.3f", $homeStrength); ?> - <?php $awayStr =  sprintf("%.3f", $awayStrength); ?>
							<!-- <td>
								<?php
								if($homeStrength > $awayStrength)
								{
									echo $exchangeRates['data'][$cnt]['home_team'];
									echo " / ";
									echo $homeStr;
								}
								else
								{
									echo $exchangeRates['data'][$cnt]['away_team'];
									echo " / ";
									echo $awayStr;
								}
								?>
							</td> -->

							<!-- <td><?php echo $homeStr; ?> - <?php echo $awayStr; ?></td> -->

							<td><?php echo $homeStr; ?></td>
							<td><?php echo $awayStr; ?></td>

							<!-- <td>Home - Away</td> -->
							<!-- <td>Bet365</td>
							<td>WilliamHill</td> -->
							<!-- <td></td>
							<td></td> -->
							<!-- <td colspan="2"></td> -->
							<!--<td>Yes</td>
							<td>No</td>
							<td>Yes</td>
							<td>No</td>                         
							<td>1</td>
							<td>X</td>
							<td>2</td>  -->                        
						</tr>                                            
					</table>
				</div>

				<div class="clear"></div>

				<p class="result-p ow-black-text text-center font-weight900"><?php if (isset($exchangeRates['data'][$cnt]['available_markets']))
				{
					echo "Available Markets : ";
					foreach ($exchangeRates['data'][$cnt]['available_markets'] as $key => $market)
					{
						$asd = $market;
						$stringOne = $asd;
						$updatedCompName = str_replace('_', ' ', trim($stringOne));
						// echo "Available Markets :";
						// echo $updatedCompName . ', ';

						// if($updatedCompName == 'classic')
						if($updatedCompName == 'classic' || $updatedCompName == 'btts')
						{
							echo $updatedCompName . ', ';
						}
						else
						{
							if(strlen($updatedCompName) >= 10)
							{	
								echo $newstring = substr($updatedCompName, 0, 11);
								echo ".";
								echo $newstring = substr($updatedCompName, -1);
								echo ", ";
							}
							else
							{	
								echo $newstring = substr($updatedCompName, 0,6);
								echo ".";
								echo $newstring = substr($updatedCompName, -1);
								echo ", ";
							}
						}
					}
				}
				?>
				</p>

				<div class="overflow-div width100 margin-top50">
						<?php if (isset($exchangeRates['data'][$cnt]['prediction_per_market']))
						{
							foreach ($exchangeRates['data'][$cnt]['prediction_per_market'] as $market => $prediction)
							{

								// $replaceMarket = str_replace('_', ' ', trim($market));
								// if($replaceMarket == 'classic' || $replaceMarket == 'btts')
								// {
								// 	echo $replaceMarket . ', ';
								// }
								// else
								// {
								// 	if(strlen($replaceMarket) >= 10)
								// 	{	
								// 		$replaceMarket = $newstring1.$dot.$newstring1;
								// 		$newstring1 = substr($updatedCompName, 0, 11);
								// 		$dot = ".";
								// 		$newstring2 = substr($updatedCompName, -1);
								// 		$replaceMarket = $newstring1.$dot.$newstring2;
								// 	}
								// 	else
								// 	{	
								// 		$newstring1 = substr($updatedCompName, 0,6);
								// 		$dot = ".";
								// 		$newstring2 = substr($updatedCompName, -1);
								// 		$replaceMarket = $newstring1.$dot.$newstring2;
								// 	}
								// }

								// echo '<table class="odds-table2 odds-table3"><tr><td class="font-weight900 text-center td-title" colspan="100%">'.$market . ' </td></tr>';
								echo '<table class="odds-table2 odds-table3"><tr><td class="font-weight900 text-center td-title" colspan="100%">'.str_replace('_', ' ', trim($market)).' </td></tr>';
								// echo '<table class="odds-table2 odds-table3"><tr><td class="font-weight900 text-center td-title" colspan="100%">'.$replaceMarket.' </td></tr>';
								if (isset($exchangeRates['data'][$cnt]['prediction_per_market'][$market]))
								{
									foreach ($exchangeRates['data'][$cnt]['prediction_per_market'][$market] as $status => $prediction)
									{
										if ($status == 'probabilities' || $status == 'odds')
										{
											foreach ($exchangeRates['data'][$cnt]['prediction_per_market'][$market][$status] as $stat => $probability)
											{
												echo '<td><p class="td-top">'.$stat . '</p><p class="td-bottom">'.$probability . '</p></td>';
											}
										}
										// else
										// {
										// 	echo $status . ' : ';
										// 	echo $prediction . ', ';
										// }
									}
								}
							}
						}
						?>
					</table>
				</div>

			</div>

		<?php
		}
	}
	?>

    <div class="clear"></div>
    
</div>
