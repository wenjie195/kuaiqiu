<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://kuaiqiu.tech/login.php" />
<link rel="canonical" href="https://kuaiqiu.tech/login.php" />
<meta property="og:title" content="Login | Kuai Qiu" />
<title>Login | Kuai Qiu</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding min-height field-bg overflow menu-distance">
	<div class="glass login-div">
    	
    		<img src="img/logo-white.png" class="login-logo" alt="Kuai Qiu" title="Kuai Qiu">
        
        <div class="white-border"></div>
        <!-- <h1 class="white-text h1-title">Login</h1> -->
        <h1 class="white-text h1-title">登录</h1>
        <form action="utilities/loginFunction.php" method="POST" >
          <!-- <p class="input-top-p">Username</p> -->
          <p class="input-top-p">用户名</p>
            <input class="input-name clean" type="text" placeholder="用户名" name="username" id="username" required>
          <!-- <p class="input-top-p">Password</p> -->
          <p class="input-top-p">密码</p>
          <div class="password-input-div">
            <input class="input-name clean password-input" type="password" placeholder="密码" name="password" id="password" required> 
            <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionD()" alt="View Password" title="View Password">                 
          </div>
          <!-- <button class="pill-button white-background width100 clean register-button opacity-hover"  name="login">Login</button> -->
          <button class="pill-button white-background width100 clean register-button opacity-hover"  name="login">登录</button>
          <div class="clear"></div>
          <div class="width100 text-center margin-top10">
            <!-- <a href="register.php" class="white-text opacity-hover bottom-a">Register</a> -->
            <a href="register.php" class="white-text opacity-hover bottom-a">注册</a>
          </div>
          <div class="clear"></div>
          <div class="width100 text-center margin-top10">
            <!-- <a href="forgotPassword.php" class="white-text opacity-hover bottom-a">Forgot Password</a> -->
            <a href="forgotPassword.php" class="white-text opacity-hover bottom-a">忘记密码</a>
          </div>        
        </form>        
    </div>
</div>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Login Successfully ! "; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Login ERROR !"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Wrong Password";
        }
        elseif($_GET['type'] == 4)
        {
            $messageType = "Please Register";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<style>
::-webkit-input-placeholder { /* Edge */
  color: #eaeaea;
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: #eaeaea;
}

::placeholder {
  color: #eaeaea;
}
</style>
</body>
</html>