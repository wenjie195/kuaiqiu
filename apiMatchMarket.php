<?php

// date_default_timezone_set('Asia/Kuala_Lumpur');
// $date = date('Y-m-d H:i', time());
// $dateOnly = date('Y-m-d', time());

$No = 0;
$curl = curl_init();

curl_setopt_array($curl, [
	// CURLOPT_URL => "https://football-prediction-api.p.rapidapi.com/api/v2/predictions?market=classic&iso_date=".$dateOnly."&federation=UEFA",

	CURLOPT_URL => "https://football-prediction-api.p.rapidapi.com/api/v2/head-to-head/150712?limit=10",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => [
		"x-rapidapi-host: football-prediction-api.p.rapidapi.com",
		"x-rapidapi-key: 16c81199b8msh057448939d0cc57p135fd8jsn8c6fb196197a"
	],
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	echo $response;
	$exchangeRates = json_decode($response, true);
}
?>