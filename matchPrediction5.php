<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Prediction.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$matchID = $_SESSION['match_id'];
$predictionDetails = getPrediction($conn,"WHERE fixture_id = ? ", array("fixture_id") ,array($matchID),"s");

$conn->close();
?>

<div class="overflow-div width100">	

<h1 class="green-text h1-title margin-bottom0 text-center">推荐</h1>  
<?php
if($predictionDetails)
{
	for($cnt = 0;$cnt < count($predictionDetails) ;$cnt++)
	{
	?>    
			<!-- <table> -->
			<table class="transparent-table time-table">
				<tr>
					<?php 
						$winningTeam = $predictionDetails[$cnt]->getWinningTeam();
						if($winningTeam != "")
						{
						?>
							<tr>
								<td>
									<h1 class="black-text stadium-title text-center margin-bottom0">
										关注一方 : <?php echo $predictionDetails[$cnt]->getWinningTeam();?>
									</h1>
								</td>
							</tr>  
						<?php
						}
					?>

					<?php 
						$bigSmall = $predictionDetails[$cnt]->getBigSmall();
						if($bigSmall != "")
						{
						?>
							<tr>
								<td>
									<h1 class="black-text stadium-title text-center margin-bottom0">
										大小球 : <?php echo $predictionDetails[$cnt]->getBigSmall();?>
									</h1>
								</td>
							</tr>   
						<?php
						}
					?>

					<?php 
						$eatLet = $predictionDetails[$cnt]->getEatLet();
						if($eatLet != "")
						{
						?>
							<tr>
								<td>
									<h1 class="black-text stadium-title text-center margin-bottom0">
										吃 / 放 : <?php echo $predictionDetails[$cnt]->getEatLet();?>
									</h1>
								</td>
							</tr>  
						<?php
						}
					?>

					<?php 
						$correctScore = $predictionDetails[$cnt]->getScore();
						if($correctScore != "")
						{
						?>
							<tr>
								<td>
									<h1 class="black-text stadium-title text-center margin-bottom0">
										比分 : <?php echo $predictionDetails[$cnt]->getScore();?>
									</h1>
								</td>
							</tr>  
						<?php
						}
					?>
					
					<?php 
						$remark = $predictionDetails[$cnt]->getRemark();
						if($remark != "")
						{
						?>
							<tr>
								<td>
									<h1 class="black-text stadium-title text-center margin-bottom0">
										<?php echo $predictionDetails[$cnt]->getRemark();?>
									</h1>
								</td>
							</tr> 
						<?php
						}
					?>
				</tr>
			</table>
	<?php
	}
}
?>  

</div>	
