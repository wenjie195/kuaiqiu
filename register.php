<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://kuaiqiu.tech/register.php" />
<link rel="canonical" href="https://kuaiqiu.tech/register.php" />
<meta property="og:title" content="Register | Kuai Qiu" />
<title>Register | Kuai Qiu</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding min-height field-bg overflow menu-distance">
	<div class="glass login-div">
    	
    		<img src="img/logo-white.png" class="login-logo" alt="Kuai Qiu" title="Kuai Qiu">
        
        <div class="white-border"></div>
        <h1 class="white-text h1-title">Register</h1>
        <!-- <form action="" method="POST" > -->
        <form method="POST"  action="utilities/registerFunction.php">
          <!-- <p class="input-top-p">Username</p> -->
          <p class="input-top-p">用户名</p>
            <input class="input-name clean" type="text" placeholder="用户名" name="register_username" id="register_username" required>
          <!-- <p class="input-top-p">Password</p> -->
          <p class="input-top-p">密码</p>
          <div class="password-input-div">
            <input class="input-name clean password-input" type="password" placeholder="密码" name="register_password" id="register_password" required> 
            <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">                 
          </div>
          <!-- <p class="input-top-p">Retype Password</p> -->
          <p class="input-top-p">重打密码</p>
          <div class="password-input-div">
            <input class="input-name clean password-input" type="password" placeholder="重打密码" name="register_retype_password" id="register_retype_password" required> 
            <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">                 
          </div>
           <!-- <p class="input-top-p">Email</p> -->
           <p class="input-top-p">电邮</p>
            <input class="input-name clean" type="email" placeholder="电邮" name="register_email" id="register_email" required>
           <!-- <p class="input-top-p">Contact Number</p> -->
           <p class="input-top-p">电话号码</p>
            <input class="input-name clean" type="text" placeholder="电话号码" name="register_phone" id="register_phone" required>
                   
          <button class="pill-button white-background width100 clean register-button opacity-hover"  name="submit">提交</button>
          <div class="clear"></div>
          <div class="width100 text-center margin-top10">
            <a href="index.php" class="white-text opacity-hover bottom-a">登录</a>
          </div>
        
        </form>        
    </div>
</div>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully ! "; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Your registration is fail !"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Username / Email has been used by others !";
        }
        elseif($_GET['type'] == 4)
        {
            $messageType = "Password length must more than 5 !";
        }
        elseif($_GET['type'] == 5)
        {
            $messageType = "Password must me same as Retype Password !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<style>
::-webkit-input-placeholder { /* Edge */
  color: #eaeaea;
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: #eaeaea;
}

::placeholder {
  color: #eaeaea;
}
</style>
</body>
</html>