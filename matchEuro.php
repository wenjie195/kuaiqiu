<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// if($_SERVER['REQUEST_METHOD'] == 'POST')
// {
//     $conn = connDB();
// }
// else 
// {
//     // header('Location: ../index.php');
// }
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://kuaiqiu.tech/matchEuro.php" />
<link rel="canonical" href="https://kuaiqiu.tech/matchEuro.php" />
<meta property="og:title" content="Match Euro | Kuai Qiu" />
<title>Match Euro | Kuai Qiu</title>
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding min-height grey-bg menu-distance overflow ow-same-padding">
			<div class="width100 text-center">
            	<img src="img/euro2020.png" class="euro-logo" alt="EURO 2020" title="EURO 2020">
            </div>
			

			<div class="text-center width100"></div>

			<div class="overflow-div width100">
				<div class="width100 overflow text-center"></div>
				<div class="overflow-div width100 win-table-div">
					<table class="odds-table2 win-table">	
						<tr class="top-tr">
							<td>Wins Against Table</td>
							
							  
							<td  class="font-weight900 top-td"><img src="flag/hungary.jpg" class="flag top-flag"><br>Hungary</td>
							<td  class="font-weight900 top-td"><img src="flag/north-macedonia.jpg" class="flag top-flag"><br>North Macedonia</td>
							<td  class="font-weight900 top-td"><img src="flag/finland.jpg" class="flag top-flag"><br>Finland</td>
							<td  class="font-weight900 top-td"><img src="flag/slovakia.jpg" class="flag top-flag"><br>Slovakia</td>
							<td  class="font-weight900 top-td"><img src="flag/ukraine.jpg" class="flag top-flag"><br>Ukraine</td>
							<td  class="font-weight900 top-td"><img src="flag/czech-republic.jpg" class="flag top-flag"><br>Czech Republic</td>
							<td  class="font-weight900 top-td"><img src="flag/russia.jpg" class="flag top-flag"><br>Russia</td>
							<td  class="font-weight900 top-td"><img src="flag/scotland.jpg" class="flag top-flag"><br>Scotland</td>
							<td  class="font-weight900 top-td"><img src="flag/wales.jpg" class="flag top-flag"><br>Wales</td>
							<td  class="font-weight900 top-td"><img src="flag/turkey.jpg" class="flag top-flag"><br>Turkey</td>
							<td  class="font-weight900 top-td"><img src="flag/sweden.jpg" class="flag top-flag"><br>Sweden</td>
							<td  class="font-weight900 top-td"><img src="flag/poland.jpg" class="flag top-flag"><br>Poland</td>
							<td  class="font-weight900 top-td"><img src="flag/austria.jpg" class="flag top-flag"><br>Austria</td>
							<td  class="font-weight900 top-td"><img src="flag/switzerland.jpg" class="flag top-flag"><br>Switzerland</td>
							<td  class="font-weight900 top-td"><img src="flag/croatia.jpg" class="flag top-flag"><br>Croatia</td>
							<td  class="font-weight900 top-td"><img src="flag/denmark.jpg" class="flag top-flag"><br>Denmark</td>
							<td  class="font-weight900 top-td"><img src="flag/netherlands.jpg" class="flag top-flag"><br>Netherlands</td>
							<td  class="font-weight900 top-td"><img src="flag/italy.jpg" class="flag top-flag"><br>Italy</td>
							<td  class="font-weight900 top-td"><img src="flag/belgium.jpg" class="flag top-flag"><br>Belgium</td>
							<td  class="font-weight900 top-td"><img src="flag/germany.jpg" class="flag top-flag"><br>Germany</td>
							<td  class="font-weight900 top-td"><img src="flag/portugal.jpg" class="flag top-flag"><br>Portugal</td>
							<td  class="font-weight900 top-td"><img src="flag/spain.jpg" class="flag top-flag"><br>Spain</td>
							<td  class="font-weight900 top-td"><img src="flag/england.jpg" class="flag top-flag"><br>England</td>
							<td  class="font-weight900 top-td"><img src="flag/france.jpg" class="flag top-flag"><br>France</td>  

						</tr>
						<tr class="tr2">
							<td  class="font-weight900 left-td1"><img src="flag/france.jpg" class="flag left-flag">France</td>
							<td >86.8%</td>
							<td >86.3%</td>
							<td >84.3%</td>
							<td >82.6%</td>
							<td >83.2%</td>
							<td >80.7%</td>
							<td >82.7%</td>
							<td >77.1%</td>
							<td >82.0%</td>
							<td >75.5%</td>
							<td >76.9%</td>
							<td >75.5%</td>
							<td >73.3%</td>
							<td >72.5%</td>
							<td >66.0%</td>
							<td >61.0%</td>
							<td >58.6%</td>
							<td >59.9%</td>
							<!-- <td >59.9%</td> -->
							<td >55.5%</td>
							<td >56.3%</td>
							<td >51.7%</td>
							<td >55.7%</td>
							<td >52.5%</td>
							<td ></td>
						</tr>  
						<tr class="tr1">
							<td  class="font-weight900 left-td1"><img src="flag/england.jpg" class="flag left-flag">England</td>                     
							<td >84.9%</td>
							<td >83.7%</td>
							<td >82.3%</td>
							<td >81.7%</td>
							<td >83.5%</td>
							<td >81.4%</td>
							<td >84.1%</td>
							<td >80.5%</td>
							<td >81.7%</td>
							<td >80.0%</td>
							<td >79.0%</td>
							<td >77.7%</td>
							<td >75.3%</td>
							<td >72.7%</td>
							<!-- <td >66.0%</td> -->
							<td >66.0%</td>
							<td >64.9%</td>
							<td >63.0%</td>
							<td >58.0%</td>
							<td >5.0%</td>
							<td >53.5%</td>
							<td >54.2%</td>
							<td >52.7%</td>
							<td ></td>
							<td >47.5%</td>
						</tr>  
						<tr class="tr2">
							<td  class="font-weight900 left-td1"><img src="flag/spain.jpg" class="flag left-flag">Spain</td>                     
							<td >85.4%</td>
							<td >83.6%</td>
							<td >82.9%</td>
							<td >83.4%</td>
							<td >84.3%</td>
							<td >78.8%</td>
							<td >83.7%</td>
							<td >76.8%</td>
							<td >79.7%</td>
							<td >73.4%</td>
							<td >73.0%</td>
							<td >70.9%</td>
							<td >72.9%</td>
							<td >65.3%</td>
							<td >63.2%</td>
							<td >61.2%</td>
							<td >58.7%</td>
							<td >56.7%</td>
							<td >55.6%</td>
							<td >52.0%</td>
							<td >52.0%</td>
							<td ></td>
							<td >43.7%</td>
							<!-- <td >47.3</td> -->
							<td >44.3%</td>
						</tr>
						<tr class="tr1">
							<td  class="font-weight900 left-td1"><img src="flag/portugal.jpg" class="flag left-flag">Portugal</td>                     
							<td >83.8%</td>
							<td >82.8%</td>
							<td >82.7%</td>
							<td >79.1%</td>
							<td >77.9%</td>
							<td >75.8%</td>
							<td >72.9%</td>
							<td >72.9%</td>
							<td >73.0%</td>
							<td >68.8%</td>
							<td >66.3%</td>
							<td >68.7%</td>
							<td >70.3%</td>
							<td >62.9%</td>
							<td >62.6%</td>
							<td >58.6%</td>
							<td >57.9%</td>
							<td >54.8%</td>
							<td >58.8%</td>
							<td >50.2%</td>
							<td ></td>
							<td >48.0%</td>
							<td >45.8%</td>
							<td >48.3%</td>
						</tr>
						<tr class="tr2">
							<td  class="font-weight900 left-td1"><img src="flag/germany.jpg" class="flag left-flag">Germany</td>                     
							<td >85.3%</td>
							<td >85.7%</td>
							<td >83.5%</td>
							<td >81.0%</td>
							<td >81.2%</td>
							<td >80.8%</td>
							<td >80.8%</td>
							<td >75.8%</td>
							<td >77.1%</td>
							<td >73.2%</td>
							<td >72.9%</td>
							<td >72.1%</td>
							<td >68.8%</td>
							<td >65.0%</td>
							<td >63.6%</td>
							<td >58.6%</td>
							<td >52.9%</td>
							<td >54.4%</td>
							<td >55.1%</td>
							<td ></td>
							<td >49.8%</td>
							<td >48.0</td>
							<td >46.5%</td>
							<td >43.7%</td>
						</tr>        
						<tr class="tr1">
							<td  class="font-weight900 left-td1"><img src="flag/belgium.jpg" class="flag left-flag">Belgium</td>                     
							<td >81.5%</td>
							<td >83.0%</td>
							<td >81.0%</td>
							<td >77.7%</td>
							<td >75.2%</td>
							<td >70.7%</td>
							<td >72.7%</td>
							<td >72.0%</td>
							<td >70.8%</td>
							<td >74.0%</td>
							<td >68.4%</td>
							<td >67.8%</td>
							<td >68.5%</td>
							<td >63.9%</td>
							<td >61.0%</td>
							<td >57.3%</td>
							<td >53.5%</td>
							<td >48.4%</td>
							<td ></td>
							<td >44.9%</td>
							<td >41.2%</td>
							<td >44.4%</td>
							<td >43.0%</td>
							<td >44.5%</td>
						</tr>                
						<tr class="tr2">
							<td  class="font-weight900 left-td1"><img src="flag/italy.jpg" class="flag left-flag">Italy</td>                     
							<td >84.1%</td>
							<td >85.0%</td>
							<td >82.2%</td>
							<td >78.2%</td>
							<td >77.8%</td>
							<td >72.7%</td>
							<td >73.0%</td>
							<td >72.4%</td>
							<td >71.9%</td>
							<td >72.0%</td>
							<td >65.6%</td>
							<td >66.8%</td>
							<td >66.7%</td>
							<td >63.0%</td>
							<td >59.7%</td>
							<td >57.9%</td>
							<td >51.1%</td>
							<td ></td>
							<td >51.6%</td>
							<td >45.6%</td>
							<td >45.2%</td>
							<td >43.3%</td>
							<td >42.0%</td>
							<td >40.1%</td>
						</tr>   
						<tr class="tr1">
							<td  class="font-weight900 left-td1"><img src="flag/netherlands.jpg" class="flag left-flag">Netherlands</td>    
							<td >81.6%</td>
							<td >84.3%</td>
							<td >80.6%</td>
							<td >71.1%</td>
							<td >73.2%</td>
							<td >67.8%</td>
							<td >69.5%</td>
							<td >69.7%</td>
							<td >61.3%</td>
							<td >68.2%</td>
							<td >57.3%</td>
							<td >60.5%</td>
							<td >62.4%</td>
							<td >58.6%</td>
							<td >54.3%</td>
							<td >50.2%</td>
							<td ></td>
							<td >48.9%</td>
							<td >46.5%</td>
							<td >47.1%</td>
							<td >42.1%</td>
							<td >41.3%</td>
							<td >37.0%</td>
							<td >41.4%</td>                
						</tr>        
						<tr class="tr2">
							<td  class="font-weight900 left-td1"><img src="flag/denmark.jpg" class="flag left-flag">Denmark</td>        
							<td >75.7%</td>
							<td >82.2%</td>
							<td >76.2%</td>
							<td >64.9%</td>
							<td >70.8%</td>
							<td >70.7%</td>
							<td >65.3%</td>
							<td >64.5%</td>
							<td >61.5%</td>
							<td >61.7%</td>
							<td >59.6%</td>
							<td >60.9%</td>
							<td >60.1%</td>
							<td >56.8%</td>
							<td >48.1%</td>
							<td ></td>
							<td >49.8%</td>
							<td >42.1%</td>
							<td >42.7%</td>
							<td >41.4%</td>
							<td >41.4%</td>
							<td >38.8%</td>
							<td >35.1%</td>
							<td >39.0%</td>             
						</tr>  
						<tr class="tr1">
							<td  class="font-weight900 left-td1"><img src="flag/croatia.jpg" class="flag left-flag">Croatia</td>        
							<td >74.5%</td>
							<td >73.2%</td>
							<td >71.7%</td>
							<td >62.2%</td>
							<td >64.8%</td>
							<td >65.4%</td>
							<td >62.7%</td>
							<td >65.1%</td>
							<td >60.2%</td>
							<td >56.1%</td>
							<td >56.9%</td>
							<td >52.9%</td>
							<td >56.4%</td>
							<td >52.6%</td>
							<td ></td>
							<td >51.9%</td>
							<td >45.7%</td>
							<td >40.3%</td>
							<td >39.0%</td>
							<td >36.4%</td>
							<td >37.8%</td>
							<td >36.8%</td>
							<td >34.0%</td>
							<td >34.0%</td>               
						</tr>  
						<tr class="tr2">
							<td  class="font-weight900 left-td1"><img src="flag/switzerland.jpg" class="flag left-flag">Switzerland</td>      
							<td >68.7%</td>
							<td >74.2%</td>
							<td >71.6%</td>
							<td >61.9%</td>
							<td >61.8%</td>
							<td >61.9%</td>
							<td >63.4%</td>
							<td >53.0%</td>
							<td >57.2%</td>
							<td >58.8%</td>
							<td >53.9%</td>
							<td >52.6%</td>
							<td >50.7%</td>
							<td ></td>
							<td >47.4%</td>
							<td >43.2%</td>
							<td >41.4%</td>
							<td >37.0%</td>
							<td >36.1%</td>
							<td >35.0%</td>
							<td >37.1%</td>
							<td >34.7%</td>
							<td >27.3%</td>
							<td >27.5%</td>
						</tr>  
						<tr class="tr1">
							<td  class="font-weight900 left-td1"><img src="flag/austria.jpg" class="flag left-flag">Austria</td>       
							<td >60.8%</td>
							<td >74.7%</td>
							<td >64.8%</td>
							<td >54.2%</td>
							<td >58.8%</td>
							<td >58.2%</td>
							<td >59.5%</td>
							<td >51.3%</td>
							<td >55.6%</td>
							<td >55.3%</td>
							<td >53.0%</td>
							<td >50.8%</td>
							<td ></td>
							<td >49.3%</td>
							<td >43.6%</td>
							<td >39.9%</td>
							<td >37.6%</td>
							<td >33.3%</td>
							<td >31.5%</td>
							<td >31.4%</td>
							<td >29.7%</td>
							<td >27.1%</td>
							<td >24.7%</td>
							<td >26.7%</td>                
						</tr>      
						<tr class="tr2">
							<td  class="font-weight900 left-td1"><img src="flag/poland.jpg" class="flag left-flag">Poland</td>            
							<td >62.0%</td>
							<td >67.6%</td>
							<td >68.7%</td>
							<td >57.6%</td>
							<td >54.0%</td>
							<td >57.7%</td>
							<td >58.0%</td>
							<td >51.6%</td>
							<td >58.3%</td>
							<td >52.5%</td>
							<td >54.9%</td>
							<td ></td>
							<td >49.2%</td>
							<td >47.4%</td>
							<td >47.1%</td>
							<td >39.1%</td>
							<td >39.5%</td>
							<td >33.2%</td>
							<td >32.2%</td>
							<td >27.9%</td>
							<td >31.3%</td>
							<td >29.1%</td>
							<td >22.3%</td>
							<td >24.5%</td>           
						</tr> 
						<tr class="tr1">
							<td  class="font-weight900 left-td1"><img src="flag/sweden.jpg" class="flag left-flag">Sweden</td>   
							<td >64.0%</td>
							<td >68.5%</td>
							<td >63.2%</td>
							<td >55.6%</td>
							<td >55.0%</td>
							<td >53.2%</td>
							<td >53.8%</td>
							<td >49.7%</td>
							<td >53.6%</td>
							<td >52.9%</td>
							<td ></td>
							<td >45.1%</td>
							<td >47.0%</td>
							<td >46.1%</td>
							<td >43.1%</td>
							<td >40.4%</td>
							<td >42.7%</td>
							<td >34.4%</td>
							<td >31.6%</td>
							<td >27.1%</td>
							<td >33.7%</td>
							<td >27.0%</td>
							<td >21.0%</td>
							<td >23.1%</td>                    
						</tr>      
						<tr class="tr2">
							<td  class="font-weight900 left-td1"><img src="flag/turkey.jpg" class="flag left-flag">Turkey</td>          
							<td >59.0%</td>
							<td >64.8%</td>
							<td >63.5%</td>
							<td >53.2%</td>
							<td >52.6%</td>
							<td >55.6%</td>
							<td >55.3%</td>
							<td >52.3%</td>
							<td >51.5%</td>
							<td ></td>
							<td >47.1%</td>
							<td >47.5%</td>
							<td >44.7%</td>
							<td >41.2%</td>
							<td >43.9%</td>
							<td >38.3%</td>
							<td >31.8%</td>
							<td >28.0%</td>
							<td >26.0%</td>
							<td >26.8%</td>
							<td >31.2%</td>
							<td >26.6%</td>
							<td >20.0%</td>
							<td >24.5%</td>           
						</tr> 
						<tr class="tr1">
							<td  class="font-weight900 left-td1"><img src="flag/wales.jpg" class="flag left-flag">Wales</td>        
							<td >59.9%</td>
							<td >62.4%</td>
							<td >58.3%</td>
							<td >51.4%</td>
							<td >53.6%</td>
							<td >52.1%</td>
							<td >50.6%</td>
							<td >45.3%</td>
							<td ></td>
							<td >48.5%</td>
							<td >46.4%</td>
							<td >41.7%</td>
							<td >44.4%</td>
							<td >42.8%</td>
							<td >39.8%</td>
							<td >38.5%</td>
							<td >38.7%</td>
							<td >28.1%</td>
							<td >29.2%</td>
							<td >22.9%</td>
							<td >27.0%</td>
							<td >20.3%</td>
							<td >18.3%</td>
							<td >18.0%</td>             
						</tr> 
						<tr class="tr2">
							<td  class="font-weight900 left-td1"><img src="flag/scotland.jpg" class="flag left-flag">Scotland</td>     
							<td >62.2%</td>
							<td >66.4%</td>
							<td >64.0%</td>
							<td >54.5%</td>
							<td >60.7%</td>
							<td >56.1%</td>
							<td >56.7%</td>
							<td ></td>
							<td >54.7%</td>
							<td >47.7%</td>
							<td >50.3%</td>
							<td >48.4%</td>
							<td >48.7%</td>
							<td >47.0%</td>
							<td >34.9%</td>
							<td >35.5%</td>
							<td >30.3%</td>
							<td >27.6%</td>
							<td >28.0%</td>
							<td >24.2%</td>
							<td >27.1%</td>
							<td >23.2%</td>
							<td >19.5%</td>
							<td >22.9%</td>                 
						</tr> 
						<tr class="tr1">
							<td  class="font-weight900 left-td1"><img src="flag/russia.jpg" class="flag left-flag">Russia</td>         
							<td >55.6%</td>
							<td >59.5%</td>
							<td >55.4%</td>
							<td >48.9%</td>
							<td >47.0%</td>
							<td >45.6%</td>
							<td ></td>
							<td >43.3%</td>
							<td >49.4%</td>
							<td >44.7%</td>
							<td >46.2%</td>
							<td >42.0%</td>
							<td >40.5%</td>
							<td >36.6%</td>
							<td >37.3%</td>
							<td >34.7%</td>
							<td >30.5%</td>
							<td >27.0%</td>
							<td >27.3%</td>
							<td >19.2%</td>
							<td >27.1%</td>
							<td >16.3</td>
							<td >15.9%</td>
							<td >17.3%</td>                              
						</tr> 
						<tr class="tr2">
							<td  class="font-weight900 left-td1"><img src="flag/czech-republic.jpg" class="flag left-flag">Czech Republic</td>      
							<td >55.7%</td>
							<td >56.1%</td>
							<td >54.6%</td>
							<td >47.2%</td>
							<td >53.7%</td>
							<td ></td>
							<td >54.4%</td>
							<td >43.9%</td>
							<td >47.9%</td>
							<td >44.4%</td>
							<td >46.8%</td>
							<td >42.3%</td>
							<td >41.8%</td>
							<td >38.1%</td>
							<td >34.6%</td>
							<td >29.3%</td>
							<td >32.2%</td>
							<td >27.3%</td>
							<td >29.3%</td>
							<td >19.2%</td>
							<td >24.2%</td>
							<td >21.2%</td>
							<td >18.6%</td>
							<td >19.3%</td>                                  
						</tr> 
						<tr class="tr1">
							<td  class="font-weight900 left-td1"><img src="flag/ukraine.jpg" class="flag left-flag">Ukraine</td>        
							<td >52.2%</td>
							<td >57.2%</td>
							<td >55.4%</td>
							<td >49.1%</td>
							<td ></td>
							<td >46.3%</td>
							<td >53.0%</td>
							<td >39.3%</td>
							<td >46.4%</td>
							<td >47.4%</td>
							<td >45.0%</td>
							<td >46.0%</td>
							<td >41.2%</td>
							<td >38.2%</td>
							<td >35.2%</td>
							<td >29.2%</td>
							<td >26.8%</td>
							<td >22.2%</td>
							<td >24.8%</td>
							<td >18.8%</td>
							<td >22.1%</td>
							<td >15.7%</td>
							<td >16.5%</td>
							<td >16.8%</td>                                
						</tr> 
						<tr class="tr2">
							<td  class="font-weight900 left-td1"><img src="flag/slovakia.jpg" class="flag left-flag">Slovakia</td>     
							<td >58.4%</td>
							<td >60.6%</td>
							<td >62.2%</td>
							<td ></td>
							<td >50.9%</td>
							<td >52.8%</td>
							<td >51.1%</td>
							<td >45.5%</td>
							<td >48.6%</td>
							<td >46.8%</td>
							<td >44.4%</td>
							<td >42.4%</td>
							<td >45.8%</td>
							<td >38.1%</td>
							<td >37.8%</td>
							<td >35.1%</td>
							<td >28.9%</td>
							<td >21.8%</td>
							<td >22.3%</td>
							<td >19.0%</td>
							<td >20.9%</td>
							<td >16.6%</td>
							<td >18.3%</td>
							<td >17.4%</td>              
						</tr> 
						<tr class="tr1">
							<td  class="font-weight900 left-td1"><img src="flag/finland.jpg" class="flag left-flag">Finland</td>       
							<td >44.8%</td>
							<td >48.5%</td>
							<td ></td>
							<td >37.8%</td>
							<td >44.6%</td>
							<td >45.4%</td>
							<td >44.6%</td>
							<td >36.0%</td>
							<td >41.7%</td>
							<td >36.5%</td>
							<td >36.8%</td>
							<td >31.3%</td>
							<td >35.2%</td>
							<td >28.4%</td>
							<td >28.3%</td>
							<td >23.8%</td>
							<td >19.4%</td>
							<td >17.8%</td>
							<td >19.0%</td>
							<td >16.5%</td>
							<td >17.3%</td>
							<td >17.1%</td>
							<td >17.7%</td>
							<td >15.7%</td>     
						</tr> 
						<tr class="tr2">
							<td  class="font-weight900 left-td1"><img src="flag/north-macedonia.jpg" class="flag left-flag">North Macedonia</td>        
							<td >45.7%</td>
							<td ></td>
							<td >51.5%</td>
							<td >39.4%</td>
							<td >42.8%</td>
							<td >43.9%</td>
							<td >40.5%</td>
							<td >33.6%</td>
							<td >37.6%</td>
							<td >35.2%</td>
							<td >31.5%</td>
							<td >32.4%</td>
							<td >25.3%</td>
							<td >25.8%</td>
							<td >26.8%</td>
							<td >17.8%</td>
							<td >15.7%</td>
							<td >15.0%</td>
							<td >17.0%</td>
							<td >14.3%</td>
							<td >17.2%</td>
							<td >16.4%</td>
							<td >16.3%</td>
							<td >13.7%</td>               
						</tr> 
						<tr class="tr1">
							<td  class="font-weight900 left-td1"><img src="flag/hungary.jpg" class="flag left-flag">Hungary</td>         
							<td ></td>
							<td >54.3%</td>
							<td >55.2%</td>
							<td >41.6%</td>
							<td >47.8%</td>
							<td >44.3%</td>
							<td >44.4%</td>
							<td >37.8%</td>
							<td >40.1%</td>
							<td >41.0%</td>
							<td >36.0%</td>
							<td >38.0%</td>
							<td >39.2%</td>
							<td >31.3%</td>
							<td >25.5%</td>
							<td >24.3%</td>
							<td >18.4%</td>
							<td >15.9%</td>
							<td >18.5%</td>
							<td >14.7%</td>
							<td >16.2%</td>
							<td >14.6%</td>
							<td >15.1%</td>
							<td >13.2%</td>              
						</tr> 

					</table>
				</div>
			</div>

    <div class="clear"></div>
    
</div>
<?php include 'js.php'; ?>

</body>
</html>