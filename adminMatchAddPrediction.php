<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $matchID = rewrite($_POST["match_id"]);
	$_SESSION['match_id'] = $matchID;

	$originalUri = "https://football-prediction-api.p.rapidapi.com/api/v2/predictions/".$matchID."";
}
else 
{
    header('Location: ../index.php');
}

// $matchID = $_SESSION['match_id'];
// $originalUri = "https://football-prediction-api.p.rapidapi.com/api/v2/predictions/".$matchID."";

$curl = curl_init();

curl_setopt_array($curl, [

	CURLOPT_URL => $originalUri,
	// CURLOPT_URL => "https://football-prediction-api.p.rapidapi.com/api/v2/predictions/172963",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => [
		"x-rapidapi-host: football-prediction-api.p.rapidapi.com",
		"x-rapidapi-key: 16c81199b8msh057448939d0cc57p135fd8jsn8c6fb196197a"
	],
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	// echo $response;
	$exchangeRates = json_decode($response, true);
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://kuaiqiu.tech/matchData.php" />
<link rel="canonical" href="https://kuaiqiu.tech/matchData.php" />
<meta property="og:title" content="Prediction | Kuai Qiu" />
<title>Prediction | Kuai Qiu</title>

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding min-height grey-bg menu-distance overflow ow-same-padding">

	<?php
	if ($exchangeRates)
	{
		for ($cnt=0; $cnt <count($exchangeRates['data']) ; $cnt++)
		{
		?>
			<h1 class="black-text stadium-title text-center margin-bottom0">
				<?php echo $exchangeRates['data'][$cnt]['competition_cluster']; ?> - <?php echo $exchangeRates['data'][$cnt]['competition_name']; ?>
			</h1>

			<?php $datetime = $exchangeRates['data'][$cnt]['start_date']; ?>

			<div class="text-center width100"></div>

			<div class="overflow-div width100\">
				<div class="width100 overflow text-center">
                    <h1 class="team-title team-title-left ow-black-text"><?php echo $exchangeRates['data'][$cnt]['home_team']; ?></h1>
                    <img src="img/vs.png" class="vs" alt="vs" title="vs">
                    <h1 class="team-title team-title-right ow-black-text"><?php echo $exchangeRates['data'][$cnt]['away_team']; ?></h1>
				</div>
				<div class="overflow-div width100">
					<table class="odds-table2 small-text-table">	
						<!-- <tr>
							<td colspan="3" class=" td-title"></td>
							<td colspan="3" class="font-weight900 td-title">Statistical Odds</td>
						</tr>
						<tr>
							<td colspan="3" class=" td-title"></td>
							<td class="font-weight900 td-title">AI Generated Odds</td>
							<td colspan="2" class="font-weight900 td-title">Bookmarker Odds</td>
						</tr>   -->

						<tr>
							<td colspan="3" class=" td-title"></td>
							<td colspan="3" class="font-weight900 td-title">Statistical Odds</td>
						</tr>

						<tr>
							<td class="font-weight900 td-title">Date</td>
							<td class="font-weight900 td-title">Time</td>
							<td class="font-weight900 td-title">Match</td>
							<!-- <td class="font-weight900 td-title">Match Prediction</td> -->

							<!-- <td class="font-weight900 td-title">Home - Away</td> -->

							<!-- <td class="font-weight900">Bet365</td>
							<td class="font-weight900">WilliamHill</td> -->

							<td class="td-title">主队取胜赔率</td>
							<td class="td-title">客队取胜赔率</td>

							<!-- <td class="td-title">Bet365</td>
							<td class="td-title">WilliamHill</td> -->

							<!--<td class="font-weight900">Yes</td>
							<td class="font-weight900">No</td>
							<td class="font-weight900">Yes</td>
							<td class="font-weight900">No</td>                         
							<td  class="font-weight900">1</td>
							<td  class="font-weight900">X</td>
							<td  class="font-weight900">2</td> -->                         
						</tr>     
						<tr>
							<td><?php echo $date = date("Y-m-d",strtotime($datetime));?></td>
							<td><?php echo $time = date("H:i",strtotime($datetime));?></td>

							<!-- <td><?php //echo $date = date("Y-m-d",strtotime($datetime));?> <?php //echo $time = date("H:i",strtotime($datetime));?></td> -->

							<td><?php echo $exchangeRates['data'][$cnt]['home_team']; ?> vs <?php echo $exchangeRates['data'][$cnt]['away_team']; ?></td>
							<?php $homeStrength = $exchangeRates['data'][$cnt]['home_strength']; ?> <?php $awayStrength = $exchangeRates['data'][$cnt]['away_strength']; ?>
							<?php $homeStr = sprintf("%.3f", $homeStrength); ?> - <?php $awayStr =  sprintf("%.3f", $awayStrength); ?>
							<!-- <td>
								<?php
								if($homeStrength > $awayStrength)
								{
									echo $exchangeRates['data'][$cnt]['home_team'];
									echo " / ";
									echo $homeStr;
								}
								else
								{
									echo $exchangeRates['data'][$cnt]['away_team'];
									echo " / ";
									echo $awayStr;
								}
								?>
							</td> -->

							<!-- <td><?php echo $homeStr; ?> - <?php echo $awayStr; ?></td> -->

							<td><?php echo $homeStr; ?></td>
							<td><?php echo $awayStr; ?></td>

							<!-- <td>Home - Away</td> -->
							<!-- <td>Bet365</td>
							<td>WilliamHill</td> -->
							<!-- <td></td>
							<td></td> -->
							<!-- <td colspan="2"></td> -->
							<!--<td>Yes</td>
							<td>No</td>
							<td>Yes</td>
							<td>No</td>                         
							<td>1</td>
							<td>X</td>
							<td>2</td>  -->                        
						</tr>                                            
					</table>
				</div>

				<div class="clear"></div>

			</div>

<h1 class="green-text h1-title margin-bottom0">Prediction</h1>
<div class="green-border margin-bottom30"></div>
<!-- <form action="utilities/adminPredictionAddFunction.php" method="POST" > -->
<form action="utilities/adminPredictionAddRenewFunction.php" method="POST" >

	<div class="dual-input">
		<p class="input-top-p ow-grey-text">Fixture ID</p>
		<input class="input-name clean grey-input" type="text" value="<?php echo $matchID ;?>" id="fixture_id" name="fixture_id" readonly>
	</div>

	<div class="clear"></div>

	<div class="dual-input">
		<p class="input-top-p ow-grey-text">交手球队名称</p>
		<input class="input-name clean grey-input" type="text" placeholder="交手球队名称" value="<?php echo $exchangeRates['data'][$cnt]['home_team']; ?> vs <?php echo $exchangeRates['data'][$cnt]['away_team']; ?>" id="team_name" name="team_name" readonly>
	</div>

	<div class="dual-input second-dual-input">
		<p class="input-top-p ow-grey-text">交手球队名称 (中文)</p>
		<input class="input-name clean grey-input" type="text" placeholder="交手球队名称 (中文)" id="team_name_ch" name="team_name_ch">
	</div>

	<div class="clear"></div>

	<div class="dual-input">
		<p class="input-top-p ow-grey-text">支持队伍 (Winning Team)</p>
		<input class="input-name clean grey-input" type="text" placeholder="支持队伍 (Winning Team)" id="support_team" name="support_team">
	</div>
	
	<div class="dual-input second-dual-input">
		<p class="input-top-p ow-grey-text">吃 / 放球推荐 (写法：曼联放0.5 / 莱斯特城吃0.5)</p>
		<input class="input-name clean grey-input" type="text" placeholder="吃 / 放球推荐" id="eat_let" name="eat_let">
	</div>

	<div class="clear"></div>

	<div class="dual-input">
		<p class="input-top-p ow-grey-text">大小球推荐 (写法：大球2.5 / 小球2.5)</p>
		<input class="input-name clean grey-input" type="text" placeholder="大小球推荐" id="big_small" name="big_small"> 
	</div>

	<div class="clear"></div>

	<!-- <div class="dual-input second-dual-input">
		<p class="input-top-p ow-grey-text">比分推荐</p>
		<input class="input-name clean grey-input" type="text" placeholder="比分推荐" id="correct_score" name="correct_score">
	</div>

	<div class="clear"></div>

	<div class="dual-input">
		<p class="input-top-p ow-grey-text">备注 （Remarks）</p>
		<textarea class="input-name clean grey-input" type="text" placeholder="备注 （Remarks）" id="remark" name="remark"></textarea>  	
	</div> -->

	<div class="width100 text-center">    
		<button class="pill-button green-bg white-text clean fix-width-button opacity-hover"  name="submit">提交</button> 
	</div>
</form>     

			<p class="result-p ow-black-text text-center font-weight900">
				<?php if (isset($exchangeRates['data'][$cnt]['available_markets']))
				{
					echo "Available Markets : ";
					foreach ($exchangeRates['data'][$cnt]['available_markets'] as $key => $market)
					{
						$asd = $market;
						$stringOne = $asd;
						$updatedCompName = str_replace('_', ' ', trim($stringOne));
						// echo "Available Markets :";
						// echo $updatedCompName . ', ';

						// if($updatedCompName == 'classic')
						if($updatedCompName == 'classic' || $updatedCompName == 'btts')
						{
							echo $updatedCompName . ', ';
						}
						else
						{
							if(strlen($updatedCompName) >= 10)
							{	
								echo $newstring = substr($updatedCompName, 0, 11);
								echo ".";
								echo $newstring = substr($updatedCompName, -1);
								echo ", ";
							}
							else
							{	
								echo $newstring = substr($updatedCompName, 0,6);
								echo ".";
								echo $newstring = substr($updatedCompName, -1);
								echo ", ";
							}
						}
					}
				}
				?>
			</p>

		<?php
		}
	}
	?>

    <div class="clear"></div>
    
	<div class="chat-section">
		<div id="divStatHomeAway"></div>
	</div>

	<div class="clear"></div>

	<div class="chat-section">
		<div id="divPast10Home"></div>
	</div>

	<div class="clear"></div>

	<div class="chat-section">
		<div id="divPast10Away"></div>
	</div>

	<div class="clear"></div>

</div>

<?php include 'js.php'; ?>

<script type="text/javascript">
    $(document).ready(function()
    {
        $("#divStatHomeAway").load("adminMatchDetails2.php");
    setInterval(function()
    {
        $("#divStatHomeAway").load("adminMatchDetails2.php");
    }, 100000);
    });
</script>

<script type="text/javascript">
    $(document).ready(function()
    {
        $("#divPast10Home").load("adminMatchP10Home.php");
    setInterval(function()
    {
        $("#divPast10Home").load("adminMatchP10Home.php");
    }, 100000);
    });
</script>

<script type="text/javascript">
    $(document).ready(function()
    {
        $("#divPast10Away").load("adminMatchP10Away.php");
    setInterval(function()
    {
        $("#divPast10Away").load("adminMatchP10Away.php");
    }, 100000);
    });
</script>

</body>
</html>