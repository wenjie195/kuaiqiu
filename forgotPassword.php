<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://kuaiqiu.tech/forgotPassword.php" />
<link rel="canonical" href="https://kuaiqiu.tech/forgotPassword.php" />
<meta property="og:title" content="Forgot Password | Kuai Qiu" />
<title>Forgot Password | Kuai Qiu</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding min-height field-bg overflow menu-distance">
	<div class="glass login-div">
    	
    		<img src="img/logo-white.png" class="login-logo" alt="Kuai Qiu" title="Kuai Qiu">
        
        <div class="white-border"></div>
        <h1 class="white-text h1-title">Forgot Password</h1>
        <form action="utilities/forgotPasswordFunction.php" method="POST" >
          <p class="input-top-p">Email</p>
            <input class="input-name clean" type="email" placeholder="Email" name="email" id="email" required>
          
          <button class="pill-button white-background width100 clean register-button opacity-hover"  name="submit">Submit</button>
          <div class="clear"></div>
          <div class="width100 text-center margin-top10">
            <a href="index.php" class="white-text opacity-hover bottom-a">Login</a>
          </div>
          <div class="clear"></div>
          <div class="width100 text-center margin-top10">
             <a href="register.php" class="white-text opacity-hover bottom-a">Register</a>
          </div>        
        </form>        
    </div>
</div>
<?php include 'js.php'; ?>
<style>
::-webkit-input-placeholder { /* Edge */
  color: #eaeaea;
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: #eaeaea;
}

::placeholder {
  color: #eaeaea;
}
</style>
</body>
</html>