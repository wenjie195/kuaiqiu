<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Prediction.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

$allPrediction = getPrediction($conn, "ORDER BY date_created DESC");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://kuaiqiu.tech/adminDateSelection.php" />
<link rel="canonical" href="https://kuaiqiu.tech/adminDateSelection.php" />
<meta property="og:title" content="Date Selection | Kuai Qiu" />
<title>Date Selection | Kuai Qiu</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding min-height grey-bg menu-distance overflow">
    <h1 class="green-text h1-title margin-bottom0 text-center">Date Selection</h1>  
    <div class="width100 text-center">
        <div class="green-border margin-bottom30 margin-auto"></div>
    </div>

    <form action="adminMatchAll.php" method="POST">
        <div class="width100 text-center margin-bottom30">
            <p class="input-top-p ow-grey-text">Date Selection</p>
            <input class="select-width" type="date" placeholder="Date" name="date" id="date" required>
        </div>
        <div class="width100 text-center">    
            <button class="pill-button green-bg white-text clean fix-width-button opacity-hover"  name="submit">提交</button> 
        </div>
    </form>      

    <div class="clear"></div>

    <div class="width100 text-center">
        <h1 class="green-text stadium-title text-center">All Prediction</h1>
    </div>

    <div class="overflow-div width100 win-table-div">
        <table class="odds-table2 win-table">	
            <thead>
                <tr class="tr2">
                	<td class="font-weight900 left-td1">No.</td>
                    <td class="font-weight900 left-td1">Match Name</td>
                    <td class="font-weight900 left-td1">Match ID</td>
                    <td class="font-weight900 left-td1">Date</td>
                    <td class="font-weight900 left-td1">View</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($allPrediction)
                    {
                        for($cnt = 0;$cnt < count($allPrediction) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $allPrediction[$cnt]->getNameEn();?></td>
                                <td><?php echo $allPrediction[$cnt]->getFixtureId();?></td>
                                <td><?php echo $allPrediction[$cnt]->getDateCreated();?></td>

                                <td>
                                    <form action="adminPredictionDetails.php" method="POST" class="hover1">
                                        <button class="clean blue-ow-btn" type="submit" name="item_uid" value="<?php echo $allPrediction[$cnt]->getFixtureId();?>">
                                           View
                                        </button>
                                    </form> 
                                </td>
                            </tr>
                        <?php
                        }
                    }
                ?>                                 
            </tbody>
        </table>	
    </div>

    <div class="clear"></div>
</div>
<?php include 'js.php'; ?>

</body>
</html>