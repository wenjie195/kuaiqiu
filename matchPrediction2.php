<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $matchID = "172963";

$matchID = $_SESSION['match_id'];
$originalUri = "https://football-prediction-api.p.rapidapi.com/api/v2/head-to-head/".$matchID."?limit=10";

$curl = curl_init();

curl_setopt_array($curl, [
	// CURLOPT_URL => "https://football-prediction-api.p.rapidapi.com/api/v2/head-to-head/150712?limit=10",
	CURLOPT_URL => $originalUri,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => [
		"x-rapidapi-host: football-prediction-api.p.rapidapi.com",
		"x-rapidapi-key: 16c81199b8msh057448939d0cc57p135fd8jsn8c6fb196197a"
	],
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err)
{
	echo "cURL Error #:" . $err;
}
else
{
	// echo $response;
	// echo '<br><br>';
	$exchangeRates = json_decode($response, true);
}
?>

<div class="overflow-div width100">	
    
	<table class="odds-table2 odds-table3 center-text-padding"><tr><td class="font-weight900 text-center td-title" colspan="100%">交手记录</td></tr>
			<tr>
			<td><p class="font-weight900">上半场成绩</p></td>
			<td><p class="font-weight900">赛季</p></td>

			<td><p class="font-weight900">赛事名称</p></td>
			<td><p class="font-weight900">日期</p></td>
			<td><p class="font-weight900">主队</p></td>
			<td><p class="font-weight900">客队</p></td>
			<td><p class="font-weight900">成绩</p></td>
			<td><p class="font-weight900">赛事主办协会</p></td>

			<!-- <td><p class="font-weight900">日期</p></td>
			<td><p class="font-weight900">主队</p></td>
			<td><p class="font-weight900">客队</p></td>
			<td><p class="font-weight900">赛事名称</p></td>
			<td><p class="font-weight900">成绩</p></td>
			<td><p class="font-weight900">赛事主办协会</p></td> -->
            </tr>
    
    	<?php
			

			foreach ($exchangeRates['data']['encounters'] as $key5 => $value5)
			{
				echo '<tr></tr>';
				foreach ($value5 as $key6 => $value6)
				{
					$asd6 = $key6;
					$stringSix = $asd6;
					$updatedKey6 = str_replace('_', ' ', trim($stringSix));
					echo '<td><p class="td-bottom">'.$value6 . '</p></td>';
				}
			}
		?>
	</table>
</div>
	