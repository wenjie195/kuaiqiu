<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://kuaiqiu.tech/resetPassword.php" />
<link rel="canonical" href="https://kuaiqiu.tech/resetPassword.php" />
<meta property="og:title" content="Reset Password | Kuai Qiu" />
<title>Reset Password | Kuai Qiu</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding min-height field-bg overflow menu-distance">
	<div class="glass login-div">
    	
    		<img src="img/logo-white.png" class="login-logo" alt="Kuai Qiu" title="Kuai Qiu">
        
        <div class="white-border"></div>
        <h1 class="white-text h1-title">Reset Password</h1>
        <form action="utilities/resetPasswordFunction.php" method="POST" >
          <p class="input-top-p">Key Code</p>
            <input class="input-name clean" type="text" placeholder="Key Code" name="verify_code" id="verify_code" required>
          <p class="input-top-p">New Password</p>
          <div class="password-input-div">
            <input class="input-name clean password-input" type="password" placeholder="New Password" name="new_password" id="new_password" required> 
            <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionE()" alt="View Password" title="View Password">                 
          </div>
          <p class="input-top-p">Retype New Password</p>
          <div class="password-input-div">
            <input class="input-name clean password-input" type="password" placeholder="Retype New Password" name="retype_new_password" id="retype_new_password" required> 
            <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionC()" alt="View Password" title="View Password">                 
          </div>
           
        
          <button class="pill-button white-background width100 clean register-button opacity-hover"  name="submit">Submit</button>
          <div class="clear"></div>
          <div class="width100 text-center margin-top10">
            <a href="index.php" class="white-text opacity-hover bottom-a">Login</a>
          </div>
          <div class="width100 text-center margin-top10">
            <a href="register.php" class="white-text opacity-hover bottom-a">Register</a>
          </div>        
        </form>        
    </div>
</div>
<?php include 'js.php'; ?>
<style>
::-webkit-input-placeholder { /* Edge */
  color: #eaeaea;
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: #eaeaea;
}

::placeholder {
  color: #eaeaea;
}
</style>
</body>
</html>