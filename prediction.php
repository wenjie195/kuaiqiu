<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Prediction.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allPrediction = getPrediction($conn);
$allPrediction = getPrediction($conn, " ORDER BY date_created DESC ");

$predictionWin = getPrediction($conn, " WHERE prediction_result > 0 ");
$predictionDraw = getPrediction($conn, " WHERE prediction_result  = 0 ");
$predictionLose = getPrediction($conn, " WHERE prediction_result < 0 ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://kuaiqiu.tech/" />
<link rel="canonical" href="https://kuaiqiu.tech/" />
<meta property="og:title" content="快球 | 超级智能预判赛果" />
<title>快球 | 超级智能预判赛果</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding min-height grey-bg menu-distance overflow ow-same-padding">

    <?php
        if($predictionWin)
        {   
            $totalWin = count($predictionWin);
        }
        else
        {   $totalWin = 0;   }

        if($predictionDraw)
        {   
            $totalDraw = count($predictionDraw);
        }
        else
        {   $totalDraw = 0;   }

        if($predictionLose)
        {   
            $totalLose = count($predictionLose);
        }
        else
        {   $totalLose = 0;   }
    ?>
<table class="odds-table2 odds-table3 resize-table">
	<tbody>
        <tr>
            <td class="font-weight900 text-center td-title" >赢率（累计）</td>
            <td class="font-weight900 text-center td-title" >赢率（近一个月）</td>
            <td class="font-weight900 text-center td-title" >赢率（近两周）</td>
        </tr>
        <tr>
            <td class="text-center">61%</td>
            <td class="text-center">80%</td>
            <td class="text-center">70%</td>
        </tr>
    </tbody>	
</table>


    <h1 class="black-text stadium-title text-center resize-title">今日比赛： 23/12/2021</h1>
    <div class="width100 overflow-auto">
<table class="odds-table2 td-center no-break-text resize-table">
	<tbody>
        <tr>
            <td class="font-weight900 text-center td-title" >英超</td>
            <td class="font-weight900 text-center td-title" >主队vs客队</td>
            <td class="font-weight900 text-center td-title" >关注一方</td>
            <td class="font-weight900 text-center td-title" >吃/放</td>
            <td class="font-weight900 text-center td-title" >时间</td>
            <td class="font-weight900 text-center td-title" >关注</td>
        </tr>
        <?php
        if($allPrediction)
        {
            for($cnt = 0;$cnt < count($allPrediction) ;$cnt++)
            {
            ?>    


                    <tr>
                        <td>比赛1</td>
                        <td><?php echo $allPrediction[$cnt]->getNameCh();?></td>
                        <td>Liverpool</td>
                        <td><?php echo $allPrediction[$cnt]->getEatLet();?></td>
                        <td>0430</td> 
                        <td>推荐</td>              
                    </tr>

            <?php
            }
        }
        ?>  
    </tbody>	
</table>
</div>
</div>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully ! "; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Your registration is fail !"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<style>
::-webkit-input-placeholder { /* Edge */
  color: #eaeaea;
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: #eaeaea;
}

::placeholder {
  color: #eaeaea;
}
</style>

</body>
</html>