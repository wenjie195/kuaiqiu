<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://kuaiqiu.tech/" />
<link rel="canonical" href="https://kuaiqiu.tech/" />
<meta property="og:title" content="快球 | 超级智能预判赛果" />
<title>快球 | 超级智能预判赛果</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 min-height grey-bg overflow menu-distance">
	<img src="img/banner-cn.jpg" class="width100">
    <div class=" same-padding width100 overflow">
    	<div class="width100 margin-top50 text-center">
        	<p class="link-p"><a href="indexEn.php" class="green-text opacity-hover">Switch to English Version</a></p>
            <div class="clear"></div>
        	<div class="left-div1">
            	<img src="img/pic2.png" class="index-img">
            </div>
            <div class="right-div1">
            	<p class="index-content-p">快球采用了当前最先进的超级智能，遥遥领先于人工智能并依靠参赛者的情绪、情感和表现成绩来分析和预判出该队伍的比赛结果。</p>
            </div>
            <div class="clear"></div>
        	<div class="left-div1 left-div2">
            	<img src="img/pic1.png" class="index-img">
            </div>
            <div class="right-div1 right-div2">
            	<p class="index-content-p">再者，快球是通过以往经济模式里的综合数据及概率模型来塑造出的全数据化人工智能大脑。这就是为什么快球会被称为超级智能！</p>
            </div>            
            <div class="clear"></div>
        	<div class="left-div1">
            	<img src="img/pic3.png" class="index-img">
            </div>
            <div class="right-div1">
            	<p class="index-content-p">因为它懂得如何运算和对比收集到的数据来推断出结果。通过统计数据、参赛者资料与数值、队形、主场优势以及各种因素，快球超级智能能有效给出稳而有力的判断。</p>
            </div>            
            <div class="clear"></div>            
        </div>
    	<div class="width100 text-center margin-top50">
        	<h1 class="green-text h1-title margin-bottom0 text-center">快球采用的统计模型包含</h1>  
            <div class="width100 text-center">
                <div class="green-border margin-bottom30 margin-auto"></div>
            </div>
            <div class="width100 overflow margin-top50">
            	<div class="three-div">
                	<img src="img/icon1.png" class="three-div-img" alt="Black-Litterman" title="Black-Litterman">
                    <p class="three-div-title">Black-Litterman</p>
                    <p class="three-div-content">Base performance and team scoring</p>
                </div>
            	<div class="three-div">
                	<img src="img/icon2.png" class="three-div-img" alt="Random-Forest" title="Random-Forest">
                    <p class="three-div-title">Random-Forest</p>
                    <p class="three-div-content">Deterministic decision making</p>
                </div>                
             	<div class="three-div">
                	<img src="img/icon3.png" class="three-div-img" alt="Random-Cloud Sampling" title="Random-Cloud Sampling">
                    <p class="three-div-title">Random-Cloud Sampling</p>
                    <p class="three-div-content">Shaper AI Data Pruning and Selection</p>
                </div>               
            	<div class="three-div">
                	<img src="img/icon4.png" class="three-div-img" alt="Bayesian Interpretations" title="Bayesian Interpretations">
                    <p class="three-div-title">Bayesian Interpretations</p>
                    <p class="three-div-content">Philosophical Understanding and Statistics</p>
                </div>
            	<div class="three-div">
                	<img src="img/icon5.png" class="three-div-img" alt="Weighted-Swarm Selection" title="Weighted-Swarm Selection">
                    <p class="three-div-title">Weighted-Swarm Selection</p>
                    <p class="three-div-content">Latest AI Capabilities</p>
                </div>                
             	<div class="three-div">
                	<img src="img/icon6.png" class="three-div-img" alt="Weighted-Average News Indicators" title="Weighted-Average News Indicators">
                    <p class="three-div-title">Weighted-Average News Indicators</p>
                    <p class="three-div-content">News Reader</p>
                </div>  
                <div class="clear"></div>                  
                <h1 class="green-text h1-title margin-bottom0 text-center margin-top50">快球于2021年6月1日成功预判的比赛</h1>  
                <div class="width100 text-center">
                    <div class="green-border margin-bottom30 margin-auto"></div>
                </div>                
                <img src="img/61.png" class="three-div-img ow-big-img" >
                <p class="ow-big-text">（总数为100场）</p>
            </div>
        </div>
    
    
    </div>
</div>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully ! "; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Your registration is fail !"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<style>
::-webkit-input-placeholder { /* Edge */
  color: #eaeaea;
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: #eaeaea;
}

::placeholder {
  color: #eaeaea;
}
</style>

</body>
</html>