<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
// require_once dirname(__FILE__) . '/mailerFunction.php';

function userRegistration($conn,$uid,$username,$email,$phone,$finalPassword,$salt,$usertype)
{
     if(insertDynamicData($conn,"user",array("uid","username","email","phone","password","salt","user_type"),
          array($uid,$username,$email,$phone,$finalPassword,$salt,$usertype),"ssssssi") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

// function userRegistration($conn,$uid,$nickname,$username,$email,$phone,$finalPassword,$salt,$usertype,$fullname,$mrmMember,$loginType)
// {
//      if(insertDynamicData($conn,"user",array("uid","nickname","username","email","phone","password","salt","user_type","fullname","mrm_member","login_type"),
//           array($uid,$nickname,$username,$email,$phone,$finalPassword,$salt,$usertype,$fullname,$mrmMember,$loginType),"sssssssissi") === null)
//      {
//           echo "gg";
//      }
//      else{    }
//      return true;
// }

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());
     // $nickname = rewrite($_POST['register_nickname']);
     // $nickname = rewrite($_POST['register_username']);
     
     $username = rewrite($_POST['register_username']);
     $email = rewrite($_POST['register_email']);
     $phone = rewrite($_POST['register_phone']);

     // $fullname = rewrite($_POST['register_fullname']);
     // $mrmMember = rewrite($_POST['mrm_member']);

     $register_password = rewrite($_POST['register_password']);
     $register_password_validation = strlen($register_password);
     $register_retype_password = rewrite($_POST['register_retype_password']);
     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     // $loginType = 2;
     // $usertype = 2;
     $usertype = 1;

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $nickname."<br>";
     // echo $username."<br>";

     if($register_password == $register_retype_password)
     {
          if($register_password_validation >= 6)
          {
               $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
               $usernameDetails = $usernameRows[0];

               $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($email),"s");
               $userEmailDetails = $userEmailRows[0];

               if (!$usernameDetails && !$userEmailDetails)
               {
                    if(userRegistration($conn,$uid,$username,$email,$phone,$finalPassword,$salt,$usertype))
                    // if(userRegistration($conn,$uid,$nickname,$username,$email,$phone,$finalPassword,$salt,$usertype,$fullname,$mrmMember,$loginType))
                    {
                         // $_SESSION['uid'] = $uid;
                         // $_SESSION['usertype'] = 1;
                         // header('Location: ../federation.php');
                         $_SESSION['messageType'] = 1;
                         header('Location: ../index.php?type=1');
                         // echo "SUCCESS";
                    }
                    else
                    {
                         $_SESSION['messageType'] = 1;
                         header('Location: ../register.php?type=2');
                         // echo "ERROR 2";
                    }
               }
               else
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../register.php?type=3');  
                    // echo "ERROR 3";
               }
          }
          else 
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../register.php?type=4');
               // echo "ERROR 4";
          }
     }
     else 
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../register.php?type=5');
          // echo "ERROR 5";
     }      
}
else 
{
     header('Location: ../index.php');
}
?>