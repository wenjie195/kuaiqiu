<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
// require_once dirname(__FILE__) . '/mailerFunction.php';

$conn = connDB();

$conn->close();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //todo validation on server side
    //TODO change login with email to use username instead
    //TODO add username field to register's backend
    $conn = connDB();

    if(isset($_POST['login']))
    {
        $username = rewrite($_POST['username']);
        $password = $_POST['password'];

        $userRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
        if($userRows)
        {
            $user = $userRows[0];
            $tempPass = hash('sha256',$password);
            $finalPassword = hash('sha256', $user->getSalt() . $tempPass);

            if($finalPassword == $user->getPassword()) 
            {
                $_SESSION['uid'] = $user->getUid();
                $_SESSION['usertype'] = $user->getUserType();
                
                if($user->getUserType() == 0)
                {
                    header('Location: ../adminDashboard.php');
                }
                elseif($user->getUserType() == 1)
                {
                    header('Location: ../federation.php');
                }
                // elseif($user->getUserType() == 2)
                // {
                //     $_SESSION['messageType'] = 1;
                //     header('Location: ../index.php?type=6');
                // }
                else
                {
                    // echo "unknown user type level !!";
                    $_SESSION['messageType'] = 1;
                    header('Location: ../login.php?type=2');
                }
            }
            else 
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../login.php?type=3');
            }
        }
        else
        {
          $_SESSION['messageType'] = 1;
          header('Location: ../login.php?type=4'); 
        }
    }
    $conn->close();
}
?>