<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = $_SESSION['uid'];

    $mobileNo = rewrite($_POST["update_phone"]);
    $email = rewrite($_POST["update_email"]);

    $user = getUser($conn,"WHERE uid = ?   ",array("uid"),array($uid),"s");    

    if($user)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($mobileNo)
        {
            array_push($tableName,"phone");
            array_push($tableValue,$mobileNo);
            $stringType .=  "s";
        }
        if($email)
        {
            array_push($tableName,"email");
            array_push($tableValue,$email);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../editProfile.php?type=1');
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../editProfile.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../editProfile.php?type=3');
    }
}
else 
{
    header('Location: ../index.php');
}
?>
