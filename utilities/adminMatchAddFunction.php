<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Matches.php';
require_once dirname(__FILE__) . '/../classes/Prediction.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $userUid = $_SESSION['uid'];

// function addPrediction($conn,$uid,$matchId,$homeTeam,$awayTeam,$matchDate,$matchTime,$datetime,$country,$league)
// {
//      if(insertDynamicData($conn,"matches",array("uid","fixture_id","home_team","away_team","date","time","date_time","country","league"),
//           array($uid,$matchId,$homeTeam,$awayTeam,$matchDate,$matchTime,$datetime,$country,$league),"sssssssss") === null)
//      {
//           echo "gg";
//      }
//      else{    }
//      return true;
// }

function addPrediction($conn,$uid,$matchId,$homeTeam,$awayTeam,$matchDate,$matchTime,$datetime,$country,$league,$homeStrength,$awayStrength)
{
     if(insertDynamicData($conn,"matches",array("uid","fixture_id","home_team","away_team","date","time","date_time","country","league","home_team_ch","away_team_ch"),
          array($uid,$matchId,$homeTeam,$awayTeam,$matchDate,$matchTime,$datetime,$country,$league,$homeStrength,$awayStrength),"sssssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());
     
     $matchId = rewrite($_POST['match_id']);
     $homeTeam = rewrite($_POST['home_team']);
     $awayTeam = rewrite($_POST['away_team']);

     $homeStrength = rewrite($_POST['home_strength']);
     $awayStrength = rewrite($_POST['away_strength']);

     $matchDate = rewrite($_POST['match_date']);
     $matchTime = rewrite($_POST['match_time']);

     $country = rewrite($_POST['country']);
     $league = rewrite($_POST['league']);

     $spacing = " ";
     $datetime = $matchDate.$spacing.$matchTime;

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $matchId."<br>";
     // echo $homeTeam."<br>";
     // echo $awayTeam."<br>";
     // echo $matchDate."<br>";
     // echo $matchTime."<br>";

     // if(addPrediction($conn,$uid,$matchId,$homeTeam,$awayTeam,$matchDate,$matchTime,$datetime))
     // // if(addPrediction($conn,$uid,$matchId))
     // {
     //      $_SESSION['messageType'] = 1;
     //      header('Location: ../adminMatchAll.php?type=1');
     //      // echo "SUCCESS";
     // }
     // else
     // {
     //      $_SESSION['messageType'] = 1;
     //      header('Location: ../adminMatchAll.php?type=2');
     //      // echo "ERROR";
     // }

     $fixtureRows = getMatches($conn," WHERE fixture_id = ? ",array("fixture_id"),array($matchId),"s");
     $existingFixtureId = $fixtureRows[0];

     if (!$existingFixtureId)
     {
          // if(addPrediction($conn,$uid,$matchId,$homeTeam,$awayTeam,$matchDate,$matchTime,$datetime,$country,$league))
          if(addPrediction($conn,$uid,$matchId,$homeTeam,$awayTeam,$matchDate,$matchTime,$datetime,$country,$league,$homeStrength,$awayStrength))
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../adminMatchAll.php?type=1');
               // echo "SUCCESS";
          }
          else
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../adminMatchAll.php?type=2');
               // echo "ERROR";
          }
     }
     else
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../adminMatchAll.php?type=3');
          // echo "Prediction Made, Pls Check";
     }
     
}
else 
{
     header('Location: ../index.php');
}
?>