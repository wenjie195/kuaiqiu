<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Prediction.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userUid = $_SESSION['uid'];

date_default_timezone_set('Asia/Kuala_Lumpur');
$monthYear = date('m-Y', time());

// function addPrediction($conn,$uid,$userUid,$fixtureId,$teamName,$teamNameCh,$winningTeam,$eatLet,$bigSmall,$correctScore,$remark)
// {
//      if(insertDynamicData($conn,"prediction",array("uid","user_uid","fixture_id","name_en","name_ch","winning_team","eat_let","big_small","score","remark"),
//           array($uid,$userUid,$fixtureId,$teamName,$teamNameCh,$winningTeam,$eatLet,$bigSmall,$correctScore,$remark),"ssssssssss") === null)
//      {
//           echo "gg";
//      }
//      else{    }
//      return true;
// }

// function addPrediction($conn,$uid,$userUid,$fixtureId,$teamName,$teamNameCh,$winningTeam,$eatLet,$bigSmall)
// {
//      if(insertDynamicData($conn,"prediction",array("uid","user_uid","fixture_id","name_en","name_ch","winning_team","eat_let","big_small"),
//           array($uid,$userUid,$fixtureId,$teamName,$teamNameCh,$winningTeam,$eatLet,$bigSmall),"ssssssss") === null)
//      {
//           echo "gg";
//      }
//      else{    }
//      return true;
// }

function addPrediction($conn,$uid,$userUid,$fixtureId,$teamName,$teamNameCh,$winningTeam,$eatLet,$bigSmall,$monthYear)
{
     if(insertDynamicData($conn,"prediction",array("uid","user_uid","fixture_id","name_en","name_ch","winning_team","eat_let","big_small","month_year"),
          array($uid,$userUid,$fixtureId,$teamName,$teamNameCh,$winningTeam,$eatLet,$bigSmall,$monthYear),"sssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());
     
     $fixtureId = rewrite($_POST['fixture_id']);
     $teamName = rewrite($_POST['team_name']);
     $teamNameCh = rewrite($_POST['team_name_ch']);
     $winningTeam = rewrite($_POST['support_team']);
     $eatLet = rewrite($_POST['eat_let']);
     $bigSmall = rewrite($_POST['big_small']);
     // $correctScore = rewrite($_POST['correct_score']);
     // $remark = rewrite($_POST['remark']);

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $nickname."<br>";
     // echo $username."<br>";

     $fixtureRows = getPrediction($conn," WHERE fixture_id = ? ",array("fixture_id"),array($fixtureId),"s");
     $existingFixtureId = $fixtureRows[0];

     // if (!$usernameDetails && !$userEmailDetails)
     if (!$existingFixtureId)
     {
          // if(addPrediction($conn,$uid,$userUid,$fixtureId,$teamName,$teamNameCh,$winningTeam,$eatLet,$bigSmall,$correctScore,$remark))
          // if(addPrediction($conn,$uid,$userUid,$fixtureId,$teamName,$teamNameCh,$winningTeam,$eatLet,$bigSmall))
          if(addPrediction($conn,$uid,$userUid,$fixtureId,$teamName,$teamNameCh,$winningTeam,$eatLet,$bigSmall,$monthYear))
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../adminMatchToPredict.php?type=1');
               // echo "SUCCESS";
          }
          else
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../adminMatchToPredict.php?type=2');
               // echo "ERROR 2";
          }
     }
     else
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../adminMatchToPredict.php?type=3');
          // echo "Prediction Made, Pls Check";
     }
     
}
else 
{
     header('Location: ../index.php');
}
?>