<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Prediction.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $itemUid = rewrite($_POST["item_uid"]);
    $matchResult = rewrite($_POST["match_result"]);
    $predicitonResult = rewrite($_POST["prediciton_result"]);

    $prediction = getPrediction($conn," WHERE uid = ? ",array("uid"),array($itemUid),"s");    

    if($prediction)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($matchResult)
        {
            array_push($tableName,"match_result");
            array_push($tableValue,$matchResult);
            $stringType .=  "s";
        }
        // if($predicitonResult)
        if($predicitonResult || !$predicitonResult)
        {
            array_push($tableName,"prediction_result");
            array_push($tableValue,$predicitonResult);
            $stringType .=  "s";
        }

        array_push($tableValue,$itemUid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"prediction"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../adminDashboard.php?type=1');
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../adminDashboard.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../adminDashboard.php?type=3');
    }
}
else 
{
    header('Location: ../index.php');
}
?>
