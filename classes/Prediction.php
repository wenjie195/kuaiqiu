<?php
class Prediction {
    /* Member variables */
    var $id,$uid,$userUid,$fixtureID,$nameEn,$nameCh,$winningTeam,$score,$bigSmall,$eatLet,$remark,$matchResult,$predictionResult,$editBy,$monthYear,$dateCreated,$dateUpdated;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getUserUid()
    {
        return $this->userUid;
    }

    /**
     * @param mixed $userUid
     */
    public function setUserUid($userUid)
    {
        $this->userUid = $userUid;
    }

    /**
     * @return mixed
     */
    public function getFixtureId()
    {
        return $this->fixtureID;
    }

    /**
     * @param mixed $fixtureID
     */
    public function setFixtureId($fixtureID)
    {
        $this->fixtureID = $fixtureID;
    }

    /**
     * @return mixed
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * @param mixed $nameEn
     */
    public function setNameEn($nameEn)
    {
        $this->nameEn = $nameEn;
    }

    /**
     * @return mixed
     */
    public function getNameCh()
    {
        return $this->nameCh;
    }

    /**
     * @param mixed $nameCh
     */
    public function setNameCh($nameCh)
    {
        $this->nameCh = $nameCh;
    }

    /**
     * @return mixed
     */
    public function getWinningTeam()
    {
        return $this->winningTeam;
    }

    /**
     * @param mixed $winningTeam
     */
    public function setWinningTeam($winningTeam)
    {
        $this->winningTeam = $winningTeam;
    }

    /**
     * @return mixed
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param mixed $score
     */
    public function setScore($score)
    {
        $this->score = $score;
    }

    /**
     * @return mixed
     */
    public function getHome()
    {
        return $this->home;
    }

    /**
     * @param mixed $home
     */
    public function setHome($home)
    {
        $this->home = $home;
    }

    /**
     * @return mixed
     */
    public function getAway()
    {
        return $this->away;
    }

    /**
     * @param mixed $away
     */
    public function setAway($away)
    {
        $this->away = $away;
    }

    /**
     * @return mixed
     */
    public function getBigSmall()
    {
        return $this->bigSmall;
    }

    /**
     * @param mixed $bigSmall
     */
    public function setBigSmall($bigSmall)
    {
        $this->bigSmall = $bigSmall;
    }

    /**
     * @return mixed
     */
    public function getEatLet()
    {
        return $this->eatLet;
    }

    /**
     * @param mixed $eatLet
     */
    public function setEatLet($eatLet)
    {
        $this->eatLet = $eatLet;
    }

    /**
     * @return mixed
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * @param mixed $remark
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;
    }

    /**
     * @return mixed
     */
    public function getMatchResult()
    {
        return $this->matchResult;
    }

    /**
     * @param mixed $result
     */
    public function setMatchResult($matchResult)
    {
        $this->matchResult = $matchResult;
    }    

    /**
     * @return mixed
     */
    public function getPredictionResult()
    {
        return $this->predictionResult;
    }

    /**
     * @param mixed $predictionResult
     */
    public function setPredictionResult($predictionResult)
    {
        $this->predictionResult = $predictionResult;
    }  

    /**
     * @return mixed
     */
    public function getEditBy()
    {
        return $this->editBy;
    }

    /**
     * @param mixed $editBy
     */
    public function setEditBy($editBy)
    {
        $this->editBy = $editBy;
    }

    /**
     * @return mixed
     */
    public function getMonthYear()
    {
        return $this->monthYear;
    }

    /**
     * @param mixed $monthYear
     */
    public function setMonthYear($monthYear)
    {
        $this->monthYear = $monthYear;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getPrediction($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","user_uid","fixture_id","name_en","name_ch","winning_team","score","big_small","eat_let","remark","match_result","prediction_result",
                            "edit_by","month_year","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"prediction");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$userUid,$fixtureID,$nameEn,$nameCh,$winningTeam,$score,$bigSmall,$eatLet,$remark,$matchResult,$predictionResult,$editBy,$monthYear,
                            $dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Prediction;
            $class->setId($id);
            $class->setUid($uid);
            $class->setUserUid($userUid);
            $class->setFixtureId($fixtureID);
            $class->setNameEn($nameEn);
            $class->setNameCh($nameCh);
            $class->setWinningTeam($winningTeam);
            $class->setScore($score);
            $class->setBigSmall($bigSmall);
            $class->setEatLet($eatLet);
            $class->setRemark($remark);
            $class->setMatchResult($matchResult);
            $class->setPredictionResult($predictionResult);
            $class->setEditBy($editBy);
            $class->setMonthYear($monthYear);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
