<?php
class Matches {
    /* Member variables */
    var $id,$uid,$fixtureID,$homeTeam,$awayTeam,$homeTeamCh,$awayTeamCh,$country,$league,$date,$time,$datetime,$remark,$matchResult,$editBy,$dateCreated,$dateUpdated;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getFixtureId()
    {
        return $this->fixtureID;
    }

    /**
     * @param mixed $fixtureID
     */
    public function setFixtureId($fixtureID)
    {
        $this->fixtureID = $fixtureID;
    }

    /**
     * @return mixed
     */
    public function getHomeTeam()
    {
        return $this->homeTeam;
    }

    /**
     * @param mixed $homeTeam
     */
    public function setHomeTeam($homeTeam)
    {
        $this->homeTeam = $homeTeam;
    }

    /**
     * @return mixed
     */
    public function getAwayTeam()
    {
        return $this->awayTeam;
    }

    /**
     * @param mixed $awayTeam
     */
    public function setAwayTeam($awayTeam)
    {
        $this->awayTeam = $awayTeam;
    }

    /**
     * @return mixed
     */
    public function getHomeTeamCh()
    {
        return $this->homeTeamCh;
    }

    /**
     * @param mixed $homeTeamCh
     */
    public function setHomeTeamCh($homeTeamCh)
    {
        $this->homeTeamCh = $homeTeamCh;
    }

    /**
     * @return mixed
     */
    public function getAwayTeamCh()
    {
        return $this->awayTeamCh;
    }

    /**
     * @param mixed $awayTeamCh
     */
    public function setAwayTeamCh($awayTeamCh)
    {
        $this->awayTeamCh = $awayTeamCh;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getLeague()
    {
        return $this->league;
    }

    /**
     * @param mixed $league
     */
    public function setLeague($league)
    {
        $this->league = $league;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return mixed
     */
    public function getDateTime()
    {
        return $this->datetime;
    }

    /**
     * @param mixed $datetime
     */
    public function setDateTime($datetime)
    {
        $this->datetime = $datetime;
    }

    /**
     * @return mixed
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * @param mixed $remark
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;
    }

    /**
     * @return mixed
     */
    public function getMatchResult()
    {
        return $this->matchResult;
    }

    /**
     * @param mixed $result
     */
    public function setMatchResult($matchResult)
    {
        $this->matchResult = $matchResult;
    }    

    /**
     * @return mixed
     */
    public function getEditBy()
    {
        return $this->editBy;
    }

    /**
     * @param mixed $editBy
     */
    public function setEditBy($editBy)
    {
        $this->editBy = $editBy;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getMatches($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","fixture_id","home_team","away_team","home_team_ch","away_team_ch","country","league","date","time","date_time","remark","match_result",
                            "edit_by","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"matches");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$fixtureID,$homeTeam,$awayTeam,$homeTeamCh,$awayTeamCh,$country,$league,$date,$time,$datetime,$remark,$matchResult,$editBy,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Matches;
            $class->setId($id);
            $class->setUid($uid);
            $class->setFixtureId($fixtureID);
            $class->setHomeTeam($homeTeam);
            $class->setAwayTeam($awayTeam);
            $class->setHomeTeamCh($homeTeamCh);
            $class->setAwayTeamCh($awayTeamCh);

            $class->setCountry($country);
            $class->setLeague($league);

            $class->setDate($date);
            $class->setTime($time);
            $class->setDateTime($datetime);
            $class->setRemark($remark);
            $class->setMatchResult($matchResult);
            $class->setEditBy($editBy);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
