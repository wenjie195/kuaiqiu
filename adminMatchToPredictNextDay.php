<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Matches.php';
require_once dirname(__FILE__) . '/classes/Prediction.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

date_default_timezone_set('Asia/Kuala_Lumpur');
// $date = date('Y-m-d H:i', time());
// $dateSQL = '2021-12-08';
// $dateSQL = '2021-12-09';
// $dateSQL = '2021-12-19';
// $dateSQL = '2021-12-13';
$dateSQL = date('Y-m-d', time());
$dateDisplay = date('d/m/Y', time());

$secondDay = date('Y-m-d',strtotime('+1 day ',strtotime($dateSQL)));

$twoWeeks = date('Y-m-d',strtotime('-14 days ',strtotime($dateSQL)));
$oneMonth = date('Y-m-d',strtotime('-1 month ',strtotime($dateSQL)));

// $allMatches = getMatches($conn, " ORDER BY date_created DESC ");

// $epl = getMatches($conn, " WHERE country = 'England' AND league = 'Premier League' AND date = '$dateSQL' ");
// $laLiga = getMatches($conn, " WHERE country = 'Spain' AND league = 'Primera Division' AND date = '$dateSQL' ");
// $serieA = getMatches($conn, " WHERE country = 'Italy' AND league = 'Serie A' AND date = '$dateSQL' ");
// $bundesliga = getMatches($conn, " WHERE country = 'Germany' AND league = 'Bundesliga' AND date = '$dateSQL' ");
// $ligueOne = getMatches($conn, " WHERE country = 'France' AND league = 'Ligue 1' AND date = '$dateSQL' ");
// $ucl = getMatches($conn, " WHERE country = 'Champions League' AND date = '$dateSQL' ");
// $uel = getMatches($conn, " WHERE country = 'Europa League' AND date = '$dateSQL' ");
// $championship = getMatches($conn, " WHERE country = 'England' AND league = 'Championship' AND date = '$dateSQL' ");

$epl = getMatches($conn, " WHERE country = 'England' AND league = 'Premier League' AND date = '$secondDay' ORDER BY time ASC ");
$laLiga = getMatches($conn, " WHERE country = 'Spain' AND league = 'Primera Division' AND date = '$secondDay' ORDER BY time ASC ");
$serieA = getMatches($conn, " WHERE country = 'Italy' AND league = 'Serie A' AND date = '$secondDay' ORDER BY time ASC ");
$bundesliga = getMatches($conn, " WHERE country = 'Germany' AND league = 'Bundesliga' AND date = '$secondDay' ORDER BY time ASC ");
$ligueOne = getMatches($conn, " WHERE country = 'France' AND league = 'Ligue 1' AND date = '$secondDay' ORDER BY time ASC ");
$ucl = getMatches($conn, " WHERE country = 'Champions League' AND date = '$secondDay' ORDER BY time ASC ");
$uel = getMatches($conn, " WHERE country = 'Europa League' AND date = '$secondDay' ORDER BY time ASC ");
$championship = getMatches($conn, " WHERE country = 'England' AND league = 'Championship' AND date = '$secondDay' ORDER BY time ASC ");

// $allPrediction = getPrediction($conn, " WHERE eat_let != '' ");
// $predictionWin = getPrediction($conn, " WHERE prediction_result > 0 ");
// $twoWeeksPrediction = getPrediction($conn, "WHERE eat_let != '' AND date_created >= '$twoWeeks' AND date_created <= '$dateSQL' ");
// $twoWeeksPercentage = getPrediction($conn, "WHERE prediction_result = '+1' AND date_created >= '$twoWeeks' AND date_created <= '$dateSQL' ");
// $oneMonthPrediction = getPrediction($conn, "WHERE eat_let != '' AND date_created >= '$oneMonth' AND date_created <= '$dateSQL' ");
// $oneMonthPercentage = getPrediction($conn, "WHERE prediction_result = '+1' AND date_created >= '$oneMonth' AND date_created <= '$dateSQL' ");

$allPrediction = getPrediction($conn, " WHERE prediction_result != '' ");
$predictionWin = getPrediction($conn, " WHERE prediction_result > 0 ");
$twoWeeksPrediction = getPrediction($conn, "WHERE prediction_result != '' AND date_created >= '$twoWeeks' AND date_created <= '$dateSQL' ");
$twoWeeksPercentage = getPrediction($conn, "WHERE prediction_result = '+1' AND date_created >= '$twoWeeks' AND date_created <= '$dateSQL' ");
$oneMonthPrediction = getPrediction($conn, "WHERE prediction_result != '' AND date_created >= '$oneMonth' AND date_created <= '$dateSQL' ");
$oneMonthPercentage = getPrediction($conn, "WHERE prediction_result = '+1' AND date_created >= '$oneMonth' AND date_created <= '$dateSQL' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://kuaiqiu.tech/matches.php" />
<link rel="canonical" href="https://kuaiqiu.tech/matches.php" />
<meta property="og:title" content="赛事 | 超级智能预判赛果" />
<title>赛事 | 超级智能预判赛果</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding min-height grey-bg menu-distance overflow ow-same-padding">

    <?php
    if($allPrediction)
    {   
        $totalPrediction = count($allPrediction);
    }
    else
    {   $totalPrediction = 0;   }

    if($predictionWin)
    {   
        $totalWin = count($predictionWin);
    }
    else
    {   $totalWin = 0;   }
    $winPercentage = ($totalWin / $totalPrediction) * 100;



    if($twoWeeksPrediction)
    {   
        $totalPrediction2Weeks = count($twoWeeksPrediction);
    }
    else
    {   $totalPrediction2Weeks = 0;   }

    if($twoWeeksPercentage)
    {   
        $totalWin2Weeks = count($twoWeeksPercentage);
    }
    else
    {   $totalWin2Weeks = 0;   }
    $winPercentage2Weeks = (($totalWin2Weeks / $totalPrediction2Weeks) * 100);



    if($oneMonthPrediction)
    {   
        $totalPrediction1Month = count($oneMonthPrediction);
    }
    else
    {   $totalPrediction1Month = 0;   }

    if($oneMonthPercentage)
    {   
        $totalWinOneMonth = count($oneMonthPercentage);
    }
    else
    {   $totalWinOneMonth = 0;   }
    $winPercentageOneMonth = (($totalWinOneMonth / $totalPrediction1Month) * 100);
    ?>

    <table class="odds-table2 odds-table3 resize-table">
        <tbody>
            <tr>
                <td class="font-weight900 text-center td-title" >赢率（累计）</td>
                <td class="font-weight900 text-center td-title" >赢率（近一个月）</td>
                <td class="font-weight900 text-center td-title" >赢率（近两周）</td>
            </tr>
            <tr>
 

                <td class="text-center bold-td"><?php echo number_format((float)$winPercentage, 2, '.', '');?>%</td>
                <td class="text-center bold-td"><?php echo number_format((float)$winPercentageOneMonth, 2, '.', '');?>%</td>
                <td class="text-center bold-td"><?php echo number_format((float)$winPercentage2Weeks, 2, '.', '');?>%</td>
            </tr>
        </tbody>	
    </table>

    <!-- <h1 class="black-text stadium-title text-center resize-title"><?php //echo $dateSQL;?> | <a href="editProfile.php">更改个人资料</a></h1> -->
    <h1 class="black-text stadium-title text-center resize-title"><a href="adminMatchToPredict.php"><?php echo $dateSQL;?></a> | <?php echo $secondDay;?></h1>

            <div class="width100 overflow-auto margin-top30">
                <table class="odds-table2 td-center no-break-text resize-table"  border="1" cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td class="font-weight900 text-center td-title" >No.</td>
                        <td class="font-weight900 text-center td-title" >赛事名称</td>
                        <td class="font-weight900 text-center td-title" >主队vs客队</td>
                        <td class="font-weight900 text-center td-title" >日期</td>
                        <td class="font-weight900 text-center td-title" >推荐</td>
                    </tr>

                    <?php
                        if($epl)
                        {
                            for($cnt = 0;$cnt < count($epl) ;$cnt++)
                            {
                            ?>
                                <tr>                            
                                    <td>英超 <?php echo ($cnt+1)?></td>
                                    <td><?php echo $epl[$cnt]->getCountry();?> <?php echo $epl[$cnt]->getLeague();?></td> 
                                    <td><?php echo $epl[$cnt]->getHomeTeam();?> vs <?php echo $epl[$cnt]->getAwayTeam();?></td>
                                    <td><?php echo $epl[$cnt]->getTime();?></td> 
                                    <td>
                                        <?php 
                                            $fixutreIdEpl = $epl[$cnt]->getFixtureId();
                                            $fixtureDateEpl = $epl[$cnt]->getDate();

                                            $conn = connDB();
                                            $predictionEpl = getPrediction($conn," WHERE fixture_id = ? ",array("fixture_id"),array($fixutreIdEpl),"s"); 
                                            if($dateSQL > $fixtureDateEpl)
                                            {
                                                if($predictionEpl)
                                                {
                                                ?>
                                                    <a href='matchPrediction.php?id=<?php echo $epl[$cnt]->getFixtureId();?>'>
                                                        <button class="clean rec-button">查看推荐</button>
                                                    </a>
                                                <?php
                                                }
                                                else
                                                {   echo "无推荐";  }   
                                            }
                                            else
                                            {
                                                if($predictionEpl)
                                                {
                                                ?>
                                                    <a href='matchPrediction.php?id=<?php echo $epl[$cnt]->getFixtureId();?>'>
                                                        <button class="clean rec-button">查看推荐</button>
                                                    </a>
                                                <?php
                                                }  
                                                else
                                                {
                                                ?>
                                                    <form method="POST" action="adminMatchAddPrediction.php">
                                                        <button class="clean rec-button" name="match_id" value="<?php echo $epl[$cnt]->getFixtureId();?>">
                                                            发布推荐
                                                        </button>
                                                    </form>
                                                <?php
                                                }  
                                            }                                     
                                        ?>
                                    </td> 
                                </tr>
                            <?php
                            }
                        }
                    ?> 

                    <?php
                        if($laLiga)
                        {
                            for($cnt = 0;$cnt < count($laLiga) ;$cnt++)
                            {
                            ?>
                                <tr>                            
                                    <td>西甲 <?php echo ($cnt+1)?></td>
                                    <td><?php echo $laLiga[$cnt]->getCountry();?> <?php echo $laLiga[$cnt]->getLeague();?></td> 
                                    <td><?php echo $laLiga[$cnt]->getHomeTeam();?> vs <?php echo $laLiga[$cnt]->getAwayTeam();?></td>
                                    <td><?php echo $laLiga[$cnt]->getTime();?></td> 
                                    <td>
                                        <?php 
                                            $fixutreIdLaLiga = $laLiga[$cnt]->getFixtureId();
                                            $fixtureDateLaLiga = $laLiga[$cnt]->getDate();

                                            $conn = connDB();
                                            $predictionLaLiga = getPrediction($conn," WHERE fixture_id = ? ",array("fixture_id"),array($fixutreIdLaLiga),"s"); 
                                            if($dateSQL > $fixtureDateLaLiga)
                                            {
                                                if($predictionLaLiga)
                                                {
                                                ?>
                                                    <a href='matchPrediction.php?id=<?php echo $laLiga[$cnt]->getFixtureId();?>'>
                                                        <button class="clean rec-button">查看推荐</button>
                                                    </a>
                                                <?php
                                                }
                                                else
                                                {   echo "无推荐";  }   
                                            }
                                            else
                                            {
                                                if($predictionLaLiga)
                                                {
                                                ?>
                                                    <a href='matchPrediction.php?id=<?php echo $laLiga[$cnt]->getFixtureId();?>'>
                                                        <button class="clean rec-button">查看推荐</button>
                                                    </a>
                                                <?php
                                                }  
                                                else
                                                {
                                                ?>
                                                    <form method="POST" action="adminMatchAddPrediction.php">
                                                        <button class="clean rec-button" name="match_id" value="<?php echo $laLiga[$cnt]->getFixtureId();?>">
                                                            发布推荐
                                                        </button>
                                                    </form>
                                                <?php
                                                }  
                                            }                                     
                                        ?>
                                    </td> 
                                </tr>
                            <?php
                            }
                        }
                    ?> 

                    <?php
                        if($serieA)
                        {
                            for($cnt = 0;$cnt < count($serieA) ;$cnt++)
                            {
                            ?>
                                <tr>                            
                                    <td>意甲 <?php echo ($cnt+1)?></td>
                                    <td><?php echo $serieA[$cnt]->getCountry();?> <?php echo $serieA[$cnt]->getLeague();?></td> 
                                    <td><?php echo $serieA[$cnt]->getHomeTeam();?> vs <?php echo $serieA[$cnt]->getAwayTeam();?></td>
                                    <td><?php echo $serieA[$cnt]->getTime();?></td> 
                                    <td>
                                        <?php 
                                            $fixutreIdSerieA = $serieA[$cnt]->getFixtureId();
                                            $fixtureDateSerieA = $serieA[$cnt]->getDate();

                                            $conn = connDB();
                                            $predictionSerieA = getPrediction($conn," WHERE fixture_id = ? ",array("fixture_id"),array($fixutreIdSerieA),"s"); 
                                            if($dateSQL > $fixtureDateSerieA)
                                            {
                                                if($predictionSerieA)
                                                {
                                                ?>
                                                    <a href='matchPrediction.php?id=<?php echo $serieA[$cnt]->getFixtureId();?>'>
                                                        <button class="clean rec-button">查看推荐</button>
                                                    </a>
                                                <?php
                                                }
                                                else
                                                {   echo "无推荐";  }   
                                            }
                                            else
                                            {
                                                if($predictionSerieA)
                                                {
                                                ?>
                                                    <a href='matchPrediction.php?id=<?php echo $serieA[$cnt]->getFixtureId();?>'>
                                                        <button class="clean rec-button">查看推荐</button>
                                                    </a>
                                                <?php
                                                }  
                                                else
                                                {
                                                ?>
                                                    <form method="POST" action="adminMatchAddPrediction.php">
                                                        <button class="clean rec-button" name="match_id" value="<?php echo $serieA[$cnt]->getFixtureId();?>">
                                                            发布推荐
                                                        </button>
                                                    </form>
                                                <?php
                                                }  
                                            }                                     
                                        ?>
                                    </td> 
                                </tr>
                            <?php
                            }
                        }
                    ?> 

                    <?php
                        if($bundesliga)
                        {
                            for($cnt = 0;$cnt < count($bundesliga) ;$cnt++)
                            {
                            ?>
                                <tr>                            
                                    <td>德甲 <?php echo ($cnt+1)?></td>
                                    <td><?php echo $bundesliga[$cnt]->getCountry();?> <?php echo $bundesliga[$cnt]->getLeague();?></td> 
                                    <td><?php echo $bundesliga[$cnt]->getHomeTeam();?> vs <?php echo $bundesliga[$cnt]->getAwayTeam();?></td>
                                    <td><?php echo $bundesliga[$cnt]->getTime();?></td> 
                                    <td>
                                        <?php 
                                            $fixutreIdBundesliga = $bundesliga[$cnt]->getFixtureId();
                                            $fixtureDateBundesliga = $bundesliga[$cnt]->getDate();

                                            $conn = connDB();
                                            $predictionBundesliga = getPrediction($conn," WHERE fixture_id = ? ",array("fixture_id"),array($fixutreIdBundesliga),"s"); 
                                            if($dateSQL > $fixtureDateBundesliga)
                                            {
                                                if($predictionBundesliga)
                                                {
                                                ?>
                                                    <a href='matchPrediction.php?id=<?php echo $bundesliga[$cnt]->getFixtureId();?>'>
                                                        <button class="clean rec-button">查看推荐</button>
                                                    </a>
                                                <?php
                                                }
                                                else
                                                {   echo "无推荐";  }   
                                            }
                                            else
                                            {
                                                if($predictionBundesliga)
                                                {
                                                ?>
                                                    <a href='matchPrediction.php?id=<?php echo $bundesliga[$cnt]->getFixtureId();?>'>
                                                        <button class="clean rec-button">查看推荐</button>
                                                    </a>
                                                <?php
                                                }  
                                                else
                                                {
                                                ?>
                                                    <form method="POST" action="adminMatchAddPrediction.php">
                                                        <button class="clean rec-button" name="match_id" value="<?php echo $bundesliga[$cnt]->getFixtureId();?>">
                                                            发布推荐
                                                        </button>
                                                    </form>
                                                <?php
                                                }  
                                            }                                     
                                        ?>
                                    </td> 
                                </tr>
                            <?php
                            }
                        }
                    ?> 

                    <?php
                        if($ligueOne)
                        {
                            for($cnt = 0;$cnt < count($ligueOne) ;$cnt++)
                            {
                            ?>
                                <tr>                            
                                    <td>法甲 <?php echo ($cnt+1)?></td>
                                    <td><?php echo $ligueOne[$cnt]->getCountry();?> <?php echo $ligueOne[$cnt]->getLeague();?></td> 
                                    <td><?php echo $ligueOne[$cnt]->getHomeTeam();?> vs <?php echo $ligueOne[$cnt]->getAwayTeam();?></td>
                                    <td><?php echo $ligueOne[$cnt]->getTime();?></td> 
                                    <td>
                                        <?php 
                                            $fixutreIdLigueOne = $ligueOne[$cnt]->getFixtureId();
                                            $fixtureDateLigueOne = $ligueOne[$cnt]->getDate();

                                            $conn = connDB();
                                            $predictionLigueOne = getPrediction($conn," WHERE fixture_id = ? ",array("fixture_id"),array($fixutreIdLigueOne),"s"); 
                                            if($dateSQL > $fixtureDateLigueOne)
                                            {
                                                if($predictionLigueOne)
                                                {
                                                ?>
                                                    <a href='matchPrediction.php?id=<?php echo $ligueOne[$cnt]->getFixtureId();?>'>
                                                        <button class="clean rec-button">查看推荐</button>
                                                    </a>
                                                <?php
                                                }
                                                else
                                                {   echo "无推荐";  }   
                                            }
                                            else
                                            {
                                                if($predictionLigueOne)
                                                {
                                                ?>
                                                    <a href='matchPrediction.php?id=<?php echo $ligueOne[$cnt]->getFixtureId();?>'>
                                                        <button class="clean rec-button">查看推荐</button>
                                                    </a>
                                                <?php
                                                }  
                                                else
                                                {
                                                ?>
                                                    <form method="POST" action="adminMatchAddPrediction.php">
                                                        <button class="clean rec-button" name="match_id" value="<?php echo $ligueOne[$cnt]->getFixtureId();?>">
                                                            发布推荐
                                                        </button>
                                                    </form>
                                                <?php
                                                }  
                                            }                                     
                                        ?>
                                    </td> 
                                </tr>
                            <?php
                            }
                        }
                    ?> 

                    <?php
                        if($ucl)
                        {
                            for($cnt = 0;$cnt < count($ucl) ;$cnt++)
                            {
                            ?>
                                <tr>                            
                                    <td>欧冠 <?php echo ($cnt+1)?></td>
                                    <td><?php echo $ucl[$cnt]->getCountry();?> <?php echo $ucl[$cnt]->getLeague();?></td> 
                                    <td><?php echo $ucl[$cnt]->getHomeTeam();?> vs <?php echo $ucl[$cnt]->getAwayTeam();?></td>
                                    <td><?php echo $ucl[$cnt]->getTime();?></td> 
                                    <td>
                                        <?php 
                                            $fixutreIdUcl = $ucl[$cnt]->getFixtureId();
                                            $fixtureDateUcl = $ucl[$cnt]->getDate();

                                            $conn = connDB();
                                            $predictionUcl = getPrediction($conn," WHERE fixture_id = ? ",array("fixture_id"),array($fixutreIdUcl),"s"); 
                                            if($dateSQL > $fixtureDateUcl)
                                            {
                                                if($predictionUcl)
                                                {
                                                ?>
                                                    <a href='matchPrediction.php?id=<?php echo $ucl[$cnt]->getFixtureId();?>'>
                                                        <button class="clean rec-button">查看推荐</button>
                                                    </a>
                                                <?php
                                                }
                                                else
                                                {   echo "无推荐";  }   
                                            }
                                            else
                                            {
                                                if($predictionUcl)
                                                {
                                                ?>
                                                    <a href='matchPrediction.php?id=<?php echo $ucl[$cnt]->getFixtureId();?>'>
                                                        <button class="clean rec-button">查看推荐</button>
                                                    </a>
                                                <?php
                                                }  
                                                else
                                                {
                                                ?>
                                                    <form method="POST" action="adminMatchAddPrediction.php">
                                                        <button class="clean rec-button" name="match_id" value="<?php echo $ucl[$cnt]->getFixtureId();?>">
                                                            发布推荐
                                                        </button>
                                                    </form>
                                                <?php
                                                }  
                                            }                                     
                                        ?>
                                    </td> 
                                </tr>
                            <?php
                            }
                        }
                    ?> 

                    <?php
                        if($uel)
                        {
                            for($cnt = 0;$cnt < count($uel) ;$cnt++)
                            {
                            ?>
                                <tr>                            
                                    <td>欧联 <?php echo ($cnt+1)?></td>
                                    <td><?php echo $uel[$cnt]->getCountry();?> <?php echo $uel[$cnt]->getLeague();?></td> 
                                    <td><?php echo $uel[$cnt]->getHomeTeam();?> vs <?php echo $uel[$cnt]->getAwayTeam();?></td>
                                    <td><?php echo $uel[$cnt]->getTime();?></td> 
                                    <td>
                                        <?php 
                                            $fixutreIdUel = $uel[$cnt]->getFixtureId();
                                            $fixtureDateUel = $uel[$cnt]->getDate();

                                            $conn = connDB();
                                            $predictionUel = getPrediction($conn," WHERE fixture_id = ? ",array("fixture_id"),array($fixutreIdUel),"s"); 
                                            if($dateSQL > $fixtureDateUel)
                                            {
                                                if($predictionUel)
                                                {
                                                ?>
                                                    <a href='matchPrediction.php?id=<?php echo $uel[$cnt]->getFixtureId();?>'>
                                                        <button class="clean rec-button">查看推荐</button>
                                                    </a>
                                                <?php
                                                }
                                                else
                                                {   echo "无推荐";  }   
                                            }
                                            else
                                            {
                                                if($predictionUel)
                                                {
                                                ?>
                                                    <a href='matchPrediction.php?id=<?php echo $uel[$cnt]->getFixtureId();?>'>
                                                        <button class="clean rec-button">查看推荐</button>
                                                    </a>
                                                <?php
                                                }  
                                                else
                                                {
                                                ?>
                                                    <form method="POST" action="adminMatchAddPrediction.php">
                                                        <button class="clean rec-button" name="match_id" value="<?php echo $uel[$cnt]->getFixtureId();?>">
                                                            发布推荐
                                                        </button>
                                                    </form>
                                                <?php
                                                }  
                                            }                                     
                                        ?>
                                    </td> 
                                </tr>
                            <?php
                            }
                        }
                    ?> 

                    <?php
                        if($championship)
                        {
                            for($cnt = 0;$cnt < count($championship) ;$cnt++)
                            {
                            ?>
                                <tr>                            
                                    <td>英冠 <?php echo ($cnt+1)?></td>
                                    <td><?php echo $championship[$cnt]->getCountry();?> <?php echo $championship[$cnt]->getLeague();?></td> 
                                    <td><?php echo $championship[$cnt]->getHomeTeam();?> vs <?php echo $championship[$cnt]->getAwayTeam();?></td>
                                    <td><?php echo $championship[$cnt]->getTime();?></td> 
                                    <td>
                                        <?php 
                                            $fixutreIdChampionship = $championship[$cnt]->getFixtureId();
                                            $fixtureDateChampionship = $championship[$cnt]->getDate();

                                            $conn = connDB();
                                            $predictionChampionship = getPrediction($conn," WHERE fixture_id = ? ",array("fixture_id"),array($fixutreIdChampionship),"s"); 
                                            if($dateSQL > $fixtureDateChampionship)
                                            {
                                                if($predictionChampionship)
                                                {
                                                ?>
                                                    <a href='matchPrediction.php?id=<?php echo $championship[$cnt]->getFixtureId();?>'>
                                                        <button class="clean rec-button">查看推荐</button>
                                                    </a>
                                                <?php
                                                }
                                                else
                                                {   echo "无推荐";  }   
                                            }
                                            else
                                            {
                                                if($predictionChampionship)
                                                {
                                                ?>
                                                    <a href='matchPrediction.php?id=<?php echo $championship[$cnt]->getFixtureId();?>'>
                                                        <button class="clean rec-button">查看推荐</button>
                                                    </a>
                                                <?php
                                                }  
                                                else
                                                {
                                                ?>
                                                    <form method="POST" action="adminMatchAddPrediction.php">
                                                        <button class="clean rec-button" name="match_id" value="<?php echo $championship[$cnt]->getFixtureId();?>">
                                                            发布推荐
                                                        </button>
                                                    </form>
                                                <?php
                                                }  
                                            }                                     
                                        ?>
                                    </td> 
                                </tr>
                            <?php
                            }
                        }
                    ?> 

                    </tbody>	
                </table>
            </div>


</div>

<?php include 'js.php'; ?>

<?php unset($_SESSION['match_id']);?>
<?php unset($_SESSION['date']);?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Prediction Added ! "; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Fail to submit Prediction !"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "The match already predicted, Pls Check !"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<style>
::-webkit-input-placeholder { /* Edge */
  color: #eaeaea;
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: #eaeaea;
}

::placeholder {
  color: #eaeaea;
}
</style>

</body>
</html>