<?php
if(isset($_SESSION['uid']))
{
?>
    <?php
    if($_SESSION['usertype'] == 0)
    //admin
    {
    ?>

        <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
            <div class="big-container-size hidden-padding" id="top-menu">
                <div class="float-left left-logo-div">
                    <a href="index.php"><img src="img/logo-white.png" class="logo-img" alt="Kuai Qiu" title="Kuai Qiu"></a>
                </div>

                <div class="right-menu-div float-right before-header-div">

                    <a href="editProfile.php" class="black-text menu-margin-right menu-item opacity-hover">
                    更改个人资料
                    </a>
                    <a href="adminDashboard.php" class="black-text menu-margin-right menu-item opacity-hover">
                        Dashboard
                    </a>
                    <a href="adminMatchToPredict.php" class="black-text menu-margin-right menu-item opacity-hover">
                        Prediction
                    </a>
                    <a href="adminMatchAllToPredict.php" class="black-text menu-margin-right menu-item opacity-hover">
                        Prediction (Whole UEFA)
                    </a>
                    <!-- <a href="" class="white-text menu-margin-right menu-item opacity-hover">
                        Tab 2
                    </a>   -->

                    <a href="logout.php"  class="black-text menu-margin-right menu-item opacity-hover">
                        登出
                    </a>   
            
                    <div id="dl-menu" class="dl-menuwrapper before-dl">
                        <button class="dl-trigger">Open Menu</button>
                        <ul class="dl-menu">
                        <li><a href="editProfile.php" class="black-text opacity-hover">更改个人资料</a></li>
                        <li><a href="adminDashboard.php" class="black-text opacity-hover">Dashboard</a></li>
                        <li><a href="adminMatchToPredict.php" class="black-text opacity-hover">Prediciton</a></li>
                        <li><a href="adminMatchAllToPredict.php" class="black-text opacity-hover">Prediciton (Whole UEFA)</a></li>
                        <li><a  href="logout.php" class="black-text opacity-hover">登出</a></li>
                        </ul>
                    </div>
                                            
                </div>
            </div>
        </header>

    <?php
    }
    elseif($_SESSION['usertype'] == 1)
    //user
    {
    ?>

        <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
                    <div class="float-left left-logo-div">
                        <a href="index.php"><img src="img/logo-white.png" class="logo-img" alt="Kuai Qiu" title="Kuai Qiu"></a>
                    </div>

                    <div class="right-menu-div float-right before-header-div">

                        <a href="editProfile.php" class="black-text menu-margin-right menu-item opacity-hover">
                        更改个人资料
                        </a>
                        
                        <!-- <a href="" class="white-text menu-margin-right menu-item opacity-hover">
                            Tab 2
                        </a>   -->

                        <a href="logout.php"  class="black-text menu-margin-right menu-item opacity-hover">
                            登出
                        </a>   
                
                        <div id="dl-menu" class="dl-menuwrapper before-dl">
                            <button class="dl-trigger">Open Menu</button>
                            <ul class="dl-menu">
                            <li><a href="editProfile.php" class="black-text opacity-hover">更改个人资料</a></li>
                            <li><a  href="logout.php" class="black-text opacity-hover">登出</a></li>
                            </ul>
                        </div>
                                                
                    </div>
                </div>
        </header>

    <?php
    }
    ?>
<?php
}
else
//no login
{
?>

    <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">

        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <a href="index.php"><img src="img/logo-white.png" class="logo-img opacity-hover" alt="Kuai Qiu" title="Kuai Qiu">
            </div>

            <div class="right-menu-div float-right before-header-div no-mobile">

                <a href="matches.php" class="black-text menu-margin-right menu-item opacity-hover">
                赛事
                </a>
                <a href="predictionPast.php" class="black-text menu-margin-right menu-item opacity-hover">
                过往推荐
                </a>                        
        
                <div id="dl-menu" class="dl-menuwrapper before-dl">
                    <button class="dl-trigger">Open Menu</button>
                    <ul class="dl-menu">
                    <li><a href="matches.php" class="black-text">赛事</a></li>
                    <li><a href="predictionPast.php" class="black-text">过往推荐</a></li>
                    <li><a  href="logout.php" class="black-text">Logout</a></li>
                    </ul>
                </div>
                                        
            </div>
        </div>

    </header>                        
                        
<?php
}
?>