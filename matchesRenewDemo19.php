<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Matches.php';
require_once dirname(__FILE__) . '/classes/Prediction.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

date_default_timezone_set('Asia/Kuala_Lumpur');
// $date = date('Y-m-d H:i', time());
// $dateSQL = '2021-12-08';
// $dateSQL = '2021-12-09';
$dateSQL = '2021-12-19';
// $dateSQL = '2021-12-22';
// $dateSQL = date('Y-m-d', time());
$dateDisplay = date('d/m/Y', time());

$twoWeeks = date('Y-m-d',strtotime('-14 days ',strtotime($dateSQL)));
$oneMonth = date('Y-m-d',strtotime('-1 month ',strtotime($dateSQL)));

// echo $twoWeeks = date('Y-m-d',strtotime('-14 days ',strtotime($dateSQL)));
// echo "<br>";
// echo $oneMonth = date('Y-m-d',strtotime('-1 month ',strtotime($dateSQL)));

$allMatches = getMatches($conn, " ORDER BY date_created DESC ");

// $epl = getMatches($conn, " WHERE country = 'England' AND league = 'Premier League' ");
// $laLiga = getMatches($conn, " WHERE country = 'Spain' AND league = 'Primera Division' ");
// $serieA = getMatches($conn, " WHERE country = 'Italy' AND league = 'Serie A' ");
// $bundesliga = getMatches($conn, " WHERE country = 'Germany' AND league = 'Bundesliga' ");
// $ligueOne = getMatches($conn, " WHERE country = 'France' AND league = 'Ligue 1' ");
// $championship = getMatches($conn, " WHERE country = 'England' AND league = 'Championship' ");

$epl = getMatches($conn, " WHERE country = 'England' AND league = 'Premier League' AND date = '$dateSQL' ");
$laLiga = getMatches($conn, " WHERE country = 'Spain' AND league = 'Primera Division' AND date = '$dateSQL' ");
$serieA = getMatches($conn, " WHERE country = 'Italy' AND league = 'Serie A' AND date = '$dateSQL' ");
$bundesliga = getMatches($conn, " WHERE country = 'Germany' AND league = 'Bundesliga' AND date = '$dateSQL' ");
$ligueOne = getMatches($conn, " WHERE country = 'France' AND league = 'Ligue 1' AND date = '$dateSQL' ");
$ucl = getMatches($conn, " WHERE country = 'Champions League' AND date = '$dateSQL' ");
$uel = getMatches($conn, " WHERE country = 'Europa League' AND date = '$dateSQL' ");
$championship = getMatches($conn, " WHERE country = 'England' AND league = 'Championship' AND date = '$dateSQL' ");

$allPrediction = getPrediction($conn, " WHERE eat_let != '' ");
$predictionWin = getPrediction($conn, " WHERE prediction_result > 0 ");

// $twoWeeksPrediction = getPrediction($conn, " WHERE eat_let != '' AND date_created <= $twoWeeks ");
// // $twoWeeksPercentage = getPrediction($conn, " WHERE prediction_result > 0 AND date_created >= $twoWeeks ");
// $twoWeeksPercentage = getPrediction($conn, "WHERE prediction_result > 0 AND date_created >= '$twoWeeks' AND date_created <= '$dateSQL'  ");

$twoWeeksPrediction = getPrediction($conn, "WHERE eat_let != '' AND date_created >= '$twoWeeks' AND date_created <= '$dateSQL' ");
$twoWeeksPercentage = getPrediction($conn, "WHERE prediction_result = '+1' AND date_created >= '$twoWeeks' AND date_created <= '$dateSQL' ");

// $oneMonthPrediction = getPrediction($conn, " WHERE eat_let != '' AND date_created <= $oneMonth ");
// // $oneMonthPercentage = getPrediction($conn, " WHERE prediction_result > 0 AND date_created >= $oneMonth ");
// $oneMonthPercentage = getPrediction($conn, "WHERE prediction_result > 0 AND date_created >= '$oneMonth' AND date_created <= '$dateSQL'  ");

$oneMonthPrediction = getPrediction($conn, "WHERE eat_let != '' AND date_created >= '$oneMonth' AND date_created <= '$dateSQL' ");
$oneMonthPercentage = getPrediction($conn, "WHERE prediction_result = '+1' AND date_created >= '$oneMonth' AND date_created <= '$dateSQL' ");

// $predictionDraw = getPrediction($conn, " WHERE prediction_result  = 0 ");
// $predictionLose = getPrediction($conn, " WHERE prediction_result < 0 ");

unset($_SESSION['match_id']);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://kuaiqiu.tech/matches.php" />
<link rel="canonical" href="https://kuaiqiu.tech/matches.php" />
<meta property="og:title" content="赛事 | 超级智能预判赛果" />
<title>赛事 | 超级智能预判赛果</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding min-height grey-bg menu-distance overflow ow-same-padding">

    <?php
    if($allPrediction)
    {   
        $totalPrediction = count($allPrediction);
    }
    else
    {   $totalPrediction = 0;   }

    if($predictionWin)
    {   
        $totalWin = count($predictionWin);
    }
    else
    {   $totalWin = 0;   }
    $winPercentage = ($totalWin / $totalPrediction) * 100;



    if($twoWeeksPrediction)
    {   
        $totalPrediction2Weeks = count($twoWeeksPrediction);
    }
    else
    {   $totalPrediction2Weeks = 0;   }

    if($twoWeeksPercentage)
    {   
        $totalWin2Weeks = count($twoWeeksPercentage);
    }
    else
    {   $totalWin2Weeks = 0;   }
    $winPercentage2Weeks = (($totalWin2Weeks / $totalPrediction2Weeks) * 100);



    if($oneMonthPrediction)
    {   
        $totalPrediction1Month = count($oneMonthPrediction);
    }
    else
    {   $totalPrediction1Month = 0;   }

    if($oneMonthPercentage)
    {   
        $totalWinOneMonth = count($oneMonthPercentage);
    }
    else
    {   $totalWinOneMonth = 0;   }
    $winPercentageOneMonth = (($totalWinOneMonth / $totalPrediction1Month) * 100);
    ?>

    <table class="odds-table2 odds-table3 resize-table">
        <tbody>
            <tr>
                <td class="font-weight900 text-center td-title" >赢率（累计）</td>
                <td class="font-weight900 text-center td-title" >赢率（近一个月）</td>
                <td class="font-weight900 text-center td-title" >赢率（近两周）</td>
            </tr>
            <tr>
 

                <td class="text-center bold-td"><?php echo number_format((float)$winPercentage, 2, '.', '');?>%</td>
                <td class="text-center bold-td"><?php echo number_format((float)$winPercentageOneMonth, 2, '.', '');?>%</td>
                <td class="text-center bold-td"><?php echo number_format((float)$winPercentage2Weeks, 2, '.', '');?>%</td>
            </tr>
        </tbody>	
    </table>


    <!-- <h1 class="black-text stadium-title text-center resize-title">今日比赛： 22/12/2021</h1> -->
    <h1 class="black-text stadium-title text-center resize-title">今日比赛： <?php echo $dateSQL;?></h1>

    <?php
        if($epl)
        {
        ?>
            <div class="width100 overflow-auto margin-top30">
                <table class="odds-table2 td-center no-break-text resize-table"  border="1" cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td class="font-weight900 text-center td-title td1z" >英超</td>
                        <td class="font-weight900 text-center td-title td2z" >主队vs客队</td>
                        <!-- <td class="font-weight900 text-center td-title" >关注一方</td>
                        <td class="font-weight900 text-center td-title" >吃/放</td> -->
                        <td class="font-weight900 text-center td-title td3z" >时间</td>
                      
                        <td class="font-weight900 text-center td-title td4z" >数据 / 推荐</td>
                    </tr>
                    <?php
                        for($cnt = 0;$cnt < count($epl) ;$cnt++)
                        {
                        ?>    
                            <tr>                            
                                <td>比赛 <?php echo ($cnt+1)?></td>
                                <td><?php echo $epl[$cnt]->getHomeTeam();?> vs <?php echo $epl[$cnt]->getAwayTeam();?></td>
                                <!-- <td>&nbsp;</td>
                                <td>&nbsp;</td> -->
                                <td><?php echo $epl[$cnt]->getTime();?></td> 

                                <td>
                                    <?php 
                                        $fixutreIdEpl = $epl[$cnt]->getFixtureId();
                                        $conn = connDB();
                                        $predictionEpl = getPrediction($conn," WHERE fixture_id = ? ",array("fixture_id"),array($fixutreIdEpl),"s"); 
                                        if($predictionEpl)
                                        {
                                            // echo "查看推荐 / 点击查看";
                                        ?>
                                            <a href='matchPrediction.php?id=<?php echo $epl[$cnt]->getFixtureId();?>'>
                                                <button class="clean rec-button">
                                                查看推荐
                                                </button>
                                            </a>
                                        <?php
                                        }  
                                        else
                                        {
                                        ?>
                                            <a href='matchStatistics.php?id=<?php echo $epl[$cnt]->getFixtureId();?>'>
                                                <button class="clean rec-button">
                                                    数据
                                                </button>
                                            </a>
                                        <?php
                                        }  
                                    ?>
                                </td> 
                                
                            </tr>
                        <?php
                        }
                    ?>
                    </tbody>	
                </table>
            </div>
        <?php
        }
        else
        {   }
    ?>  

    <?php
        if($laLiga)
        {
        ?>
            <div class="width100 overflow-auto margin-top30">
                <table class="odds-table2 td-center no-break-text resize-table" border="1" cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td class="font-weight900 text-center td-title td1z" >西甲</td>
                        <td class="font-weight900 text-center td-title td2z" >主队vs客队</td>
                        <!-- <td class="font-weight900 text-center td-title" >关注一方</td>
                        <td class="font-weight900 text-center td-title" >吃/放</td> -->
                        <td class="font-weight900 text-center td-title td3z" >时间</td>
                        <td class="font-weight900 text-center td-title td4z" >数据 / 推荐</td>
                    </tr>
                    <?php
                        for($cnt = 0;$cnt < count($laLiga) ;$cnt++)
                        {
                        ?>    
                            <tr>                            
                                <td>比赛 <?php echo ($cnt+1)?></td>
                                <td><?php echo $laLiga[$cnt]->getHomeTeam();?> vs <?php echo $laLiga[$cnt]->getAwayTeam();?></td>
                                <!-- <td>    </td>
                                <td>    </td> -->
                                <td><?php echo $laLiga[$cnt]->getTime();?></td> 
                               
                                <td>
                                    <?php 
                                        $fixutreIdLaLiga = $laLiga[$cnt]->getFixtureId();
                                        $conn = connDB();
                                        $predictionLaLiga = getPrediction($conn," WHERE fixture_id = ? ",array("fixture_id"),array($fixutreIdLaLiga),"s"); 
                                        if($predictionLaLiga)
                                        {
                                            // echo "查看推荐 / 点击查看";
                                        ?>
                                            <a href='matchPrediction.php?id=<?php echo $laLiga[$cnt]->getFixtureId();?>'>
                                                <button class="clean rec-button">
                                                查看推荐
                                                </button>
                                            </a>
                                        <?php
                                        }  
                                        else
                                        {
                                        ?>
                                            <a href='matchStatistics.php?id=<?php echo $laLiga[$cnt]->getFixtureId();?>'>
                                                <button class="clean rec-button">
                                                    数据
                                                </button>
                                            </a>
                                        <?php
                                        }  
                                    ?>
                                </td> 
                            </tr>
                        <?php
                        }
                    ?>
                    </tbody>	
                </table>
            </div>
        <?php
        }
    ?>  

    <?php
        if($serieA)
        {
        ?>
            <div class="width100 overflow-auto margin-top30">
                <table class="odds-table2 td-center no-break-text resize-table">
                    <tbody>
                    <tr>
                        <td class="font-weight900 text-center td-title td1z" >意甲</td>
                        <td class="font-weight900 text-center td-title td2z" >主队vs客队</td>
                        <!-- <td class="font-weight900 text-center td-title" >关注一方</td>
                        <td class="font-weight900 text-center td-title" >吃/放</td> -->
                        <td class="font-weight900 text-center td-title td3z" >时间</td>
                        <td class="font-weight900 text-center td-title td4z" >数据 / 推荐</td>
                    </tr>
                    <?php
                        for($cnt = 0;$cnt < count($serieA) ;$cnt++)
                        {
                        ?>    
                            <tr>                            
                                <td>比赛 <?php echo ($cnt+1)?></td>
                                <td><?php echo $serieA[$cnt]->getHomeTeam();?> vs <?php echo $serieA[$cnt]->getAwayTeam();?></td>
                                <!-- <td>    </td>
                                <td>    </td> -->
                                <td><?php echo $serieA[$cnt]->getTime();?></td> 
                                <!-- <td>    </td>    -->
                                <td>
                                    <?php 
                                        $fixutreIdSerieA = $serieA[$cnt]->getFixtureId();
                                        $conn = connDB();
                                        $predictionSerieA = getPrediction($conn," WHERE fixture_id = ? ",array("fixture_id"),array($fixutreIdSerieA),"s"); 
                                        if($predictionSerieA)
                                        {
                                        ?>
                                            <a href='matchPrediction.php?id=<?php echo $serieA[$cnt]->getFixtureId();?>'>
                                                <button class="clean rec-button">
                                                查看推荐
                                                </button>
                                            </a>
                                        <?php
                                        }  
                                        else
                                        {
                                        ?>
                                            <a href='matchStatistics.php?id=<?php echo $serieA[$cnt]->getFixtureId();?>'>
                                                <button class="clean rec-button">
                                                    数据
                                                </button>
                                            </a>
                                        <?php
                                        }  
                                    ?>
                                </td> 
                            </tr>
                        <?php
                        }
                    ?>
                    </tbody>	
                </table>
            </div>
        <?php
        }
    ?> 

    <?php
        if($bundesliga)
        {
        ?>
            <div class="width100 overflow-auto margin-top30">
                <table class="odds-table2 td-center no-break-text resize-table">
                    <tbody>
                    <tr>
                        <td class="font-weight900 text-center td-title td1z" >德甲</td>
                        <td class="font-weight900 text-center td-title td2z" >主队vs客队</td>
                        <!-- <td class="font-weight900 text-center td-title" >关注一方</td>
                        <td class="font-weight900 text-center td-title" >吃/放</td> -->
                        <td class="font-weight900 text-center td-title td3z" >时间</td>
                        <td class="font-weight900 text-center td-title td4z" >数据 / 推荐</td>
                    </tr>
                    <?php
                        for($cnt = 0;$cnt < count($bundesliga) ;$cnt++)
                        {
                        ?>    
                            <tr>                            
                                <td>比赛 <?php echo ($cnt+1)?></td>
                                <td><?php echo $bundesliga[$cnt]->getHomeTeam();?> vs <?php echo $bundesliga[$cnt]->getAwayTeam();?></td>
                                <!-- <td>    </td>
                                <td>    </td> -->
                                <td><?php echo $bundesliga[$cnt]->getTime();?></td> 
                                <!-- <td>    </td>    -->
                                <td>
                                    <?php 
                                        $fixutreIdBundesliga = $bundesliga[$cnt]->getFixtureId();
                                        $conn = connDB();
                                        $predictionBundesliga = getPrediction($conn," WHERE fixture_id = ? ",array("fixture_id"),array($fixutreIdBundesliga),"s"); 
                                        if($predictionBundesliga)
                                        {
                                        ?>
                                            <a href='matchPrediction.php?id=<?php echo $bundesliga[$cnt]->getFixtureId();?>'>
                                                <button class="clean rec-button">
                                                查看推荐
                                                </button>
                                            </a>
                                        <?php
                                        }  
                                        else
                                        {
                                        ?>
                                            <a href='matchStatistics.php?id=<?php echo $bundesliga[$cnt]->getFixtureId();?>'>
                                                <button class="clean rec-button">
                                                    数据
                                                </button>
                                            </a>
                                        <?php
                                        }  
                                    ?>
                                </td> 
                            </tr>
                        <?php
                        }
                    ?>
                    </tbody>	
                </table>
            </div>
        <?php
        }
    ?> 

    <?php
        if($ligueOne)
        {
        ?>
            <div class="width100 overflow-auto margin-top30">
                <table class="odds-table2 td-center no-break-text resize-table">
                    <tbody>
                    <tr>
                        <td class="font-weight900 text-center td-title td1z" >法甲</td>
                        <td class="font-weight900 text-center td-title td2z" >主队vs客队</td>
                        <!-- <td class="font-weight900 text-center td-title" >关注一方</td>
                        <td class="font-weight900 text-center td-title" >吃/放</td> -->
                        <td class="font-weight900 text-center td-title td3z" >时间</td>
                        <td class="font-weight900 text-center td-title td4z" >数据 / 推荐</td>
                    </tr>
                    <?php
                        for($cnt = 0;$cnt < count($ligueOne) ;$cnt++)
                        {
                        ?>    
                            <tr>                            
                                <td>比赛 <?php echo ($cnt+1)?></td>
                                <td><?php echo $ligueOne[$cnt]->getHomeTeam();?> vs <?php echo $ligueOne[$cnt]->getAwayTeam();?></td>
                                <!-- <td>    </td>
                                <td>    </td> -->
                                <td><?php echo $ligueOne[$cnt]->getTime();?></td> 
                                <!-- <td>    </td>    -->
                                <td>
                                    <?php 
                                        $fixutreIdLigueOne = $ligueOne[$cnt]->getFixtureId();
                                        $conn = connDB();
                                        $predictionLigueOne = getPrediction($conn," WHERE fixture_id = ? ",array("fixture_id"),array($fixutreIdLigueOne),"s"); 
                                        if($predictionLigueOne)
                                        {
                                        ?>
                                            <a href='matchPrediction.php?id=<?php echo $ligueOne[$cnt]->getFixtureId();?>'>
                                                <button class="clean rec-button">
                                                查看推荐
                                                </button>
                                            </a>
                                        <?php
                                        }  
                                        else
                                        {
                                        ?>
                                            <a href='matchStatistics.php?id=<?php echo $ligueOne[$cnt]->getFixtureId();?>'>
                                                <button class="clean rec-button">
                                                    数据
                                                </button>
                                            </a>
                                        <?php
                                        }  
                                    ?>
                                </td> 
                            </tr>
                        <?php
                        }
                    ?>
                    </tbody>	
                </table>
            </div>
        <?php
        }
    ?> 

    <?php
        if($ucl)
        {
        ?>
            <div class="width100 overflow-auto margin-top30">
                <table class="odds-table2 td-center no-break-text resize-table">
                    <tbody>
                    <tr>
                        <td class="font-weight900 text-center td-title td1z" >欧冠</td>
                        <td class="font-weight900 text-center td-title td2z" >主队vs客队</td>
                        <!-- <td class="font-weight900 text-center td-title" >关注一方</td>
                        <td class="font-weight900 text-center td-title" >吃/放</td> -->
                        <td class="font-weight900 text-center td-title td3z" >时间</td>
                        <td class="font-weight900 text-center td-title td4z" >数据 / 推荐</td>
                    </tr>
                    <?php
                        for($cnt = 0;$cnt < count($ucl) ;$cnt++)
                        {
                        ?>    
                            <tr>                            
                                <td>比赛 <?php echo ($cnt+1)?></td>
                                <td><?php echo $ucl[$cnt]->getHomeTeam();?> vs <?php echo $ucl[$cnt]->getAwayTeam();?></td>
                                <!-- <td>    </td>
                                <td>    </td> -->
                                <td><?php echo $ucl[$cnt]->getTime();?></td> 
                                <!-- <td>    </td>    -->
                                <td>
                                    <?php 
                                        $fixutreIdUcl = $ucl[$cnt]->getFixtureId();
                                        $conn = connDB();
                                        $predictionUcl = getPrediction($conn," WHERE fixture_id = ? ",array("fixture_id"),array($fixutreIdUcl),"s"); 
                                        if($predictionUcl)
                                        {
                                        ?>
                                            <a href='matchPrediction.php?id=<?php echo $ucl[$cnt]->getFixtureId();?>'>
                                                <button class="clean rec-button">
                                                查看推荐
                                                </button>
                                            </a>
                                        <?php
                                        }  
                                        else
                                        {
                                        ?>
                                            <a href='matchStatistics.php?id=<?php echo $ucl[$cnt]->getFixtureId();?>'>
                                                <button class="clean rec-button">
                                                    数据
                                                </button>
                                            </a>
                                        <?php
                                        }  
                                    ?>
                                </td> 
                            </tr>
                        <?php
                        }
                    ?>
                    </tbody>	
                </table>
            </div>
        <?php
        }
    ?> 

    <?php
        if($uel)
        {
        ?>
            <div class="width100 overflow-auto margin-top30">
                <table class="odds-table2 td-center no-break-text resize-table">
                    <tbody>
                    <tr>
                        <td class="font-weight900 text-center td-title td1z" >欧联</td>
                        <td class="font-weight900 text-center td-title td2z" >主队vs客队</td>
                        <!-- <td class="font-weight900 text-center td-title" >关注一方</td>
                        <td class="font-weight900 text-center td-title" >吃/放</td> -->
                        <td class="font-weight900 text-center td-title td3z" >时间</td>
                        <td class="font-weight900 text-center td-title td4z" >数据 / 推荐</td>
                    </tr>
                    <?php
                        for($cnt = 0;$cnt < count($uel) ;$cnt++)
                        {
                        ?>    
                            <tr>                            
                                <td>比赛 <?php echo ($cnt+1)?></td>
                                <td><?php echo $uel[$cnt]->getHomeTeam();?> vs <?php echo $uel[$cnt]->getAwayTeam();?></td>
                                <!-- <td>    </td>
                                <td>    </td> -->
                                <td><?php echo $uel[$cnt]->getTime();?></td> 
                                <!-- <td>    </td>    -->
                                <td>
                                    <?php 
                                        $fixutreIdUel = $uel[$cnt]->getFixtureId();
                                        $conn = connDB();
                                        $predictionUel = getPrediction($conn," WHERE fixture_id = ? ",array("fixture_id"),array($fixutreIdUel),"s"); 
                                        if($predictionUel)
                                        {
                                        ?>
                                            <a href='matchPrediction.php?id=<?php echo $uel[$cnt]->getFixtureId();?>'>
                                                <button class="clean rec-button">
                                                查看推荐
                                                </button>
                                            </a>
                                        <?php
                                        }  
                                        else
                                        {
                                        ?>
                                            <a href='matchStatistics.php?id=<?php echo $uel[$cnt]->getFixtureId();?>'>
                                                <button class="clean rec-button">
                                                    数据
                                                </button>
                                            </a>
                                        <?php
                                        }  
                                    ?>
                                </td> 
                            </tr>
                        <?php
                        }
                    ?>
                    </tbody>	
                </table>
            </div>
        <?php
        }
    ?> 

    <?php
        if($championship)
        {
        ?>
            <div class="width100 overflow-auto margin-top30">
                <table class="odds-table2 td-center no-break-text resize-table">
                    <tbody>
                    <tr>
                        <td class="font-weight900 text-center td-title td1z" >英冠</td>
                        <td class="font-weight900 text-center td-title td2z" >主队vs客队</td>
                        <!-- <td class="font-weight900 text-center td-title" >关注一方</td>
                        <td class="font-weight900 text-center td-title" >吃/放</td> -->
                        <td class="font-weight900 text-center td-title td3z" >时间</td>
                        <td class="font-weight900 text-center td-title td4z" >数据 / 推荐</td>
                    </tr>
                    <?php
                        for($cnt = 0;$cnt < count($championship) ;$cnt++)
                        {
                        ?>    
                            <tr>                            
                                <td>比赛 <?php echo ($cnt+1)?></td>
                                <td><?php echo $championship[$cnt]->getHomeTeam();?> vs <?php echo $championship[$cnt]->getAwayTeam();?></td>
                                <!-- <td>    </td>
                                <td>    </td> -->
                                <td><?php echo $championship[$cnt]->getTime();?></td> 
                                <!-- <td>    </td>    -->
                                <td>
                                    <?php 
                                        $fixutreIdChampionship = $championship[$cnt]->getFixtureId();
                                        $conn = connDB();
                                        $predictionChampionship = getPrediction($conn," WHERE fixture_id = ? ",array("fixture_id"),array($fixutreIdChampionship),"s"); 
                                        if($predictionChampionship)
                                        {
                                        ?>
                                            <a href='matchPrediction.php?id=<?php echo $championship[$cnt]->getFixtureId();?>'>
                                                <button class="clean rec-button">
                                                查看推荐
                                                </button>
                                            </a>
                                        <?php
                                        }  
                                        else
                                        {
                                        ?>
                                            <a href='matchStatistics.php?id=<?php echo $championship[$cnt]->getFixtureId();?>'>
                                                <button class="clean rec-button">
                                                    数据
                                                </button>
                                            </a>
                                        <?php
                                        }  
                                    ?>
                                </td> 
                            </tr>
                        <?php
                        }
                    ?>
                    </tbody>	
                </table>
            </div>
        <?php
        }
    ?> 

</div>

<?php include 'js.php'; ?>
<?php unset($_SESSION['match_id']);?>

<style>
::-webkit-input-placeholder { /* Edge */
  color: #eaeaea;
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: #eaeaea;
}

::placeholder {
  color: #eaeaea;
}
</style>

</body>
</html>