<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://kuaiqiu.tech/editProfile.php" />
<link rel="canonical" href="https://kuaiqiu.tech/editProfile.php" />
<meta property="og:title" content="Edit Profile | Kuai Qiu" />
<title>Edit Profile | Kuai Qiu</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding min-height grey-bg menu-distance overflow">
	
        
       
        <h1 class="green-text h1-title margin-bottom0">Edit Profile</h1>
         <div class="green-border margin-bottom30"></div>
        <form action="utilities/editProfileFunction.php" method="POST" >
        <div class="dual-input">
          <p class="input-top-p ow-grey-text">Email</p>
            <input class="input-name clean grey-input" type="email" placeholder="Email" value="<?php echo $userData->getEmail();?>" id="update_email" name="update_email" required>
        </div>
        <div class="dual-input second-dual-input">
          <p class="input-top-p ow-grey-text">Contact Number</p>
            <input class="input-name clean grey-input" type="text" placeholder="Contact Number" value="<?php echo $userData->getPhone();?>" id="update_phone" name="update_phone" required> 
        </div>
        <div class="width100 text-center">    
                      <button class="pill-button green-bg white-text clean fix-width-button opacity-hover"  name="Submit">Confirm</button> 
       </div>
          </form>      
       
        	<h1 class="green-text h1-title margin-bottom0 margin-top50">Edit Password</h1>
         <div class="green-border margin-bottom30"></div>  
         <!-- <form action="" method="POST" >  -->
         <form action="utilities/editPasswordFunction.php" method="POST">
         <div class="dual-input">       
          <p class="input-top-p ow-grey-text">Current Password</p>
          
              <div class="password-input-div grey-password-input">
                <input class="input-name clean password-input" type="password" placeholder="Current Password" id="register_password" name="register_password" required> 
                <img src="img/eye-grey.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">                 
              </div>
          </div>
          <div class="dual-input second-dual-input">
                <p class="input-top-p ow-grey-text">New Password</p>
              <div class="password-input-div grey-password-input">
                <input class="input-name clean password-input" type="password" placeholder="New Password" id="new_password" name="new_password" required> 
                <img src="img/eye-grey.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionE()" alt="View Password" title="View Password">                 
              </div>  
          </div> 
          <div class="clear"></div>    
          <div class="dual-input">     
                <p class="input-top-p ow-grey-text">Retype New Password</p>
              <div class="password-input-div grey-password-input">
                <input class="input-name clean password-input" type="password" placeholder="Retype New Password" id="retype_new_password" name="retype_new_password" required> 
                <img src="img/eye-grey.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionC()" alt="View Password" title="View Password">                 
              </div>  
          </div>  
          <div class="clear"></div> 
          <div class="width100 text-center">          
            <button class="pill-button green-bg white-text clean fix-width-button opacity-hover"  name="Submit">Confirm</button> 
          </div>
          </form>           
          <div class="clear"></div>
          
          
    
</div>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
      if($_GET['type'] == 1)
      {
          $messageType = "Profile Updated ! "; 
      }
      elseif($_GET['type'] == 2)
      {
          $messageType = "Fail to update profile !"; 
      }
      elseif($_GET['type'] == 3)
      {
          $messageType = "ERROR !"; 
      }
      echo '
      <script>
          putNoticeJavascript("Notice !! ","'.$messageType.'");  
      </script>
      ';   
      $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
      if($_GET['type'] == 1)
      {
          $messageType = "Password Changed ! "; 
      }
      elseif($_GET['type'] == 2)
      {
          $messageType = "Fail to update password !"; 
      }
      elseif($_GET['type'] == 3)
      {
          $messageType = "new password must be same with retype new passsword !"; 
      }
      elseif($_GET['type'] == 4)
      {
          $messageType = "password length must be more than 5 !"; 
      }
      // elseif($_GET['type'] == 5)
      // {
      //     $messageType = "current password is not the same as previous !"; 
      // }
      echo '
      <script>
          putNoticeJavascript("Notice !! ","'.$messageType.'");  
      </script>
      ';   
      $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>