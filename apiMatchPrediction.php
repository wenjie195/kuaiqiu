<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $matchID = "150712";
// $originalUri = "https://football-prediction-api.p.rapidapi.com/api/v2/predictions/".$matchID."";

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $matchID = rewrite($_POST["match_id"]);
	// $_SESSION['match_id'] = $matchID;

    // echo $matchID = rewrite($_POST["match_id"]);
	// echo "<br>";

	$originalUri = "https://football-prediction-api.p.rapidapi.com/api/v2/predictions/".$matchID."";
}
else 
{
    header('Location: ../index.php');
}

$No = 0;
$curl = curl_init();

curl_setopt_array($curl, [
	CURLOPT_URL => $originalUri,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => [
		"x-rapidapi-host: football-prediction-api.p.rapidapi.com",
		"x-rapidapi-key: 16c81199b8msh057448939d0cc57p135fd8jsn8c6fb196197a"
	],
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	echo $response;
	$exchangeRates = json_decode($response, true);
}
?>

<?php include 'css.php'; ?>

<div class="width100 same-padding min-height grey-bg menu-distance overflow ow-same-padding">
<?php
if ($exchangeRates)
{
	for ($cnt=0; $cnt <count($exchangeRates['data']) ; $cnt++)
	{
	?>

				<p class="result-p ow-black-text text-center font-weight900">
				<?php if (isset($exchangeRates['data'][$cnt]['available_markets']))
				{
					echo "Available Markets : ";
					foreach ($exchangeRates['data'][$cnt]['available_markets'] as $key => $market)
					{
						$asd = $market;
						$stringOne = $asd;
						$updatedCompName = str_replace('_', ' ', trim($stringOne));
						// echo "Available Markets :";
						echo $updatedCompName . ', ';
					}
				}
				?>
				</p>

				<div class="overflow-div width100 margin-top50">
					
						<?php if (isset($exchangeRates['data'][$cnt]['prediction_per_market']))
						{
							foreach ($exchangeRates['data'][$cnt]['prediction_per_market'] as $market => $prediction)
							{
								// echo '<tr><td class="first-td">'.$market . ' <td class="second-td"> ';
								// echo '<!--<tr><td>'.$market . ' </td></tr> -->';
								echo '<div class="overflow-div width100"><table class="odds-table2 odds-table3"><tr><td class="font-weight900 text-center td-title" colspan="100%">'.$market . ' </td></tr>';
								if (isset($exchangeRates['data'][$cnt]['prediction_per_market'][$market]))
								{
									foreach ($exchangeRates['data'][$cnt]['prediction_per_market'][$market] as $status => $prediction)
									{
										if ($status == 'probabilities' || $status == 'odds')
										{
											foreach ($exchangeRates['data'][$cnt]['prediction_per_market'][$market][$status] as $stat => $probability)
											{
												// echo $stat . ' : ';
												// echo $probability . ', ';
												// echo $probability;
												// echo "<br>";
												// echo '<tr><td>'.$market . ' </td></tr> ';
												
												// echo '<tr>';
												// echo '<td class="font-weight900">'.$stat . ' </td><td>'.$probability . ' </td>';

												echo '<td><p class="td-top">'.$stat . '</p><p class="td-bottom">'.$probability . '</p></td>';
												// echo '<td>'.$probability . ' </td>';

												// echo '<tr><td>'.$stat . ' </td></tr>';
												// echo '<tr><td>'.$probability . ' </td></tr>';
												// // echo '<td>'.$probability . ' </td>';
												// echo '</tr> ';

												// echo '<tr><td>'.$stat . ' </td></tr>';
												// echo '<tr><td>'.$probability . ' </td></tr>';

												// echo '<tr><td>'.$stat . ' </td>';
												// echo '<td>'.$probability . ' </td></tr>';

												// echo '<tr>';
												// echo '<td class="font-weight900">'.$stat . ' </td>';
												// echo '<td>'.$probability . ' </td>';
												// echo '<tr></tr>';
												// echo '</tr> ';

												// echo '<tr><td>'.$probability . ' </td></tr> ';
												// echo "";
												// echo "<br>";
												// echo '<div class="clear"></div>';
												// echo '<tr><td class="first-td">'.$stat . ' <td class="second-td"> ';
												// echo '<tr><td class="first-td">'.$probability . ' <td class="second-td"> ';
												// echo '<tr><td class="first-td">'.$market . ' <td class="second-td"> ';
											}
										}
										// else
										// {
										// 	echo $status . ' : ';
										// 	echo $prediction . ', ';
										// }
									}
								}
							}
						}
						?>
					</table>
				</div>

	<?php
	}
}
?>
</div>