<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $matchID = "150712";
// $originalUri = "https://football-prediction-api.p.rapidapi.com/api/v2/predictions/".$matchID."";

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $matchID = rewrite($_POST["match_id"]);
	// $_SESSION['match_id'] = $matchID;

    // echo $matchID = rewrite($_POST["match_id"]);
	// echo "<br>";

	// $originalUri = "https://football-prediction-api.p.rapidapi.com/api/v2/head-to-head/".$matchID."?limit=10";
	$originalUri = "https://football-prediction-api.p.rapidapi.com/api/v2/away-last-10/".$matchID."";
	// CURLOPT_URL => "https://football-prediction-api.p.rapidapi.com/api/v2/home-last-10/178353",
}
else 
{
    header('Location: ../index.php');
}

$No = 0;
$curl = curl_init();

curl_setopt_array($curl, [
	// CURLOPT_URL => "https://football-prediction-api.p.rapidapi.com/api/v2/predictions?market=classic&iso_date=".$dateOnly."&federation=UEFA",
	// CURLOPT_URL => "https://football-prediction-api.p.rapidapi.com/api/v2/predictions?market=classic&iso_date=2021-12-19&federation=UEFA",
	// CURLOPT_URL => "https://football-prediction-api.p.rapidapi.com/api/v2/away-last-10/178353",
	CURLOPT_URL => $originalUri,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => [
		"x-rapidapi-host: football-prediction-api.p.rapidapi.com",
		"x-rapidapi-key: 16c81199b8msh057448939d0cc57p135fd8jsn8c6fb196197a"
	],
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	echo $response;
	$exchangeRates = json_decode($response, true);
}
?>

<?php include 'css.php'; ?>

<div class="width100 same-padding min-height grey-bg menu-distance overflow ow-same-padding">

<!-- <div class="overflow-div width100">	 -->
	<table class="odds-table2 odds-table3 center-text-padding"><tr><td class="font-weight900 text-center td-title" colspan="100%">客队近10场战绩</td></tr>
			<tr>
			<td><p class="font-weight900">日期</p></td>
			<td><p class="font-weight900">和局赔率</p></td>
			<td><p class="font-weight900">支持球队</p></td>
			<td><p class="font-weight900">全场比分</p></td>
			<td><p class="font-weight900">对手</p></td>
			<td><p class="font-weight900">No.</p></td>
			<td><p class="font-weight900">对手赔率</p></td>
			<td><p class="font-weight900">成绩</p></td>
			<td><p class="font-weight900">上半场入球</p></td>
			<td><p class="font-weight900">客场</p></td>
            </tr>
    
    	<?php
			foreach ($exchangeRates['data']['encounters'] as $key5 => $value5)
			{
				echo '<tr></tr>';
				foreach ($value5 as $key6 => $value6)
				{
					$asd6 = $key6;
					$stringSix = $asd6;
					$updatedKey6 = str_replace('_', ' ', trim($stringSix));
					echo '<td><p class="td-bottom">'.$value6 . '</p></td>';
				}
			}
		?>
	</table>
</div>