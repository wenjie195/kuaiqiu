<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// if($_SERVER['REQUEST_METHOD'] == 'POST')
// {
//     $conn = connDB();
// }
// else 
// {
//     // header('Location: ../index.php');
// }
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://kuaiqiu.tech/matchEuro.php" />
<link rel="canonical" href="https://kuaiqiu.tech/matchEuro.php" />
<meta property="og:title" content="Match Euro | Kuai Qiu" />
<title>Match Euro | Kuai Qiu</title>
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding min-height grey-bg menu-distance overflow ow-same-padding">

			<div class="width100 text-center">
            	<img src="img/euro2020.png" class="euro-logo" alt="EURO 2020" title="EURO 2020">
            </div>

			<div class="text-center width100"></div>

			<div class="overflow-div width100">
            	<h1 class="green-text stadium-title text-center">Group Stage Winning</h1>
				<div class="overflow-div width100 win-table-div">
					<table class="odds-table2 win-table">	
						<tr  class="top-tr">
							<td  class="font-weight900 top-td" style="text-align:left !important;">Team Statistics</td>
							
							<td  class="font-weight900 top-td">Final Win 100%</td>
							<td  class="font-weight900 top-td">Round 16</td>
							<td  class="font-weight900 top-td">Quarter Final</td>
							<td  class="font-weight900 top-td">Semi Final</td>
							<td  class="font-weight900 top-td">Final</td>
							<td  class="font-weight900 top-td">Win</td>
						</tr>
						<tr class="tr2">
							<td  class="font-weight900 left-td1">France</td>
							<td >14.8</td>
							<td >89.7</td>
							<td >59.5</td>
							<td >39.7</td>
							<td >25</td>
							<td >14.8</td>
						</tr>  
						<tr class="tr1">
							<td  class="font-weight900 left-td1">England</td>                     
							<td >13.5</td>
							<td >94.6</td>
							<td >56.3</td>
							<td >35.3</td>
							<td >22.9</td>
							<td >13.5</td>
						</tr>  
						<tr class="tr2">
							<td  class="font-weight900 left-td1">Spain</td>                     
							<td >12.3</td>
							<td >94</td>
							<td >66.8</td>
							<td >35.4</td>
							<td >21.9</td>
							<td >12.3</td>
						</tr>
						<tr class="tr1">
							<td  class="font-weight900 left-td1">Portugal</td>                     
							<td >10.1</td>
							<td >85.3</td>
							<td >52.3</td>
							<td >31.7</td>
							<td >18.6</td>
							<td >10.1</td>
						</tr>
						<tr class="tr2">
							<td  class="font-weight900 left-td1">Germany</td>                     
							<td >10.1</td>
							<td >85.3</td>
							<td >52.3</td>
							<td >32.5</td>
							<td >18.8</td>
							<td >10.1</td>
						</tr>        
						<tr class="tr1">
							<td  class="font-weight900 left-td1">Belgium</td>                     
							<td >8.3</td>
							<td >91.5</td>
							<td >54.6</td>
							<td >32.2</td>
							<td >16.2</td>
							<td >8.3</td>
						</tr>                
						<tr class="tr2">
							<td  class="font-weight900 left-td1">Italy</td>                     
							<td >7.9</td>
							<td >88.8</td>
							<td >56.6</td>
							<td >32.2</td>
							<td >15.9</td>
							<td >7.9</td>
						</tr>   
						<tr class="tr1">
							<td  class="font-weight900 left-td1">Netherlands</td>    
							<td >6.1</td>
							<td >93.4</td>
							<td >52.1</td>
							<td >28.3</td>
							<td >13</td>
							<td >6.1</td>             
						</tr>        
						<tr class="tr2">
							<td  class="font-weight900 left-td1">Denmark</td>        
							<td >4.6</td>
							<td >84.5</td>
							<td >44.4</td>
							<td >23.3</td>
							<td >10.2</td>
							<td >4.6</td>       
						</tr>  
						<tr class="tr1">
							<td  class="font-weight900 left-td1">Croatia</td>        
							<td >3.1</td>
							<td >78</td>
							<td >36.8</td>
							<td >16.3</td>
							<td >7.4</td>
							<td >3.1</td>         
						</tr>  
						<tr class="tr2">
							<td  class="font-weight900 left-td1">Switzerland</td>      
							<td >2.2</td>
							<td >72.3</td>
							<td >34.7</td>
							<td >15.3</td>
							<td >5.7</td>
							<td >2.2</td>
						</tr>  
						<tr class="tr1">
							<td  class="font-weight900 left-td1">Austria</td>       
							<td >1.5</td>
							<td >80.9</td>
							<td >33.2</td>
							<td >13.5</td>
							<td >4.6</td>
							<td >1.5</td>             
						</tr>      
						<tr class="tr2">
							<td  class="font-weight900 left-td1">Poland</td>            
							<td >1.2</td>
							<td >66.2</td>
							<td >29.8</td>
							<td >10.2</td>
							<td >3.9</td>
							<td >1.2</td>        
						</tr> 
						<tr class="tr1">
							<td  class="font-weight900 left-td1">Sweden</td>   
							<td >1</td>
							<td >59.8</td>
							<td >25.6</td>
							<td >8.7</td>
							<td >3.2</td>
							<td >1</td>             
						</tr>      
						<tr class="tr2">
							<td  class="font-weight900 left-td1">Turkey</td>          
							<td >0.7</td>
							<td >53.3</td>
							<td >20.8</td>
							<td >7.8</td>
							<td >2.4</td>
							<td >0.7</td>         
						</tr> 
						<tr class="tr1">
							<td  class="font-weight900 left-td1">Wales</td>        
							<td >0.6</td>
							<td >53.7</td>
							<td >20.6</td>
							<td >7.4</td>
							<td >2.1</td>
							<td >0.6</td>         
						</tr> 
						<tr class="tr2">
							<td  class="font-weight900 left-td1">Scotland</td>     
							<td >0.6</td>
							<td >49.8</td>
							<td >19.3</td>
							<td >6.3</td>
							<td >2</td>
							<td >0.6</td>              
						</tr> 
						<tr class="tr1">
							<td  class="font-weight900 left-td1">Russia</td>         
							<td >0.4</td>
							<td >52</td>
							<td >16.8</td>
							<td >5.3</td>
							<td >1.4</td>
							<td >0.4</td>                           
						</tr> 
						<tr class="tr2">
							<td  class="font-weight900 left-td1">Czech Republic</td>      
							<td >0.3</td>
							<td >40.8</td>
							<td >14.4</td>
							<td >4.4</td>
							<td >1.3</td>
							<td >0.3</td>                           
						</tr> 
						<tr class="tr1">
							<td  class="font-weight900 left-td1">Ukraine</td>        
							<td >0.3</td>
							<td >57.4</td>
							<td >16.9</td>
							<td >5.1</td>
							<td >1.3</td>
							<td >0.3</td>                         
						</tr> 
						<tr class="tr2">
							<td  class="font-weight900 left-td1">Slovakia</td>     
							<td >0.3</td>
							<td >44.9</td>
							<td >16.5</td>
							<td >4.6</td>
							<td >1.3</td>
							<td >0.3</td>       
						</tr> 
						<tr class="tr1">
							<td  class="font-weight900 left-td1">Finland</td>       
							<td >0.1</td>
							<td >37.1</td>
							<td >9.3</td>
							<td >2.3</td>
							<td >0.5</td>
							<td >0.1</td>  
						</tr> 
						<tr class="tr2">
							<td  class="font-weight900 left-td1">North Macedonia</td>        
							<td >0</td>
							<td >32.9</td>
							<td >7.1</td>
							<td >1.6</td>
							<td >0.3</td>
							<td >0</td>           
						</tr> 
						<tr class="tr1">
							<td  class="font-weight900 left-td1">Hungary</td>         
							<td >0</td>
							<td >13.9</td>
							<td >3.4</td>
							<td >1</td>
							<td >0.2</td>
							<td >0</td>         
						</tr> 
						<tr class="tr2">
							<td  class="left-td1" ></td>         
							<td  class="font-weight900">100</td>
							<td ></td>
							<td ></td>
							<td ></td>
							<td ></td>
							<td ></td>         
						</tr> 
						<!-- <tr>
							<td  class="font-weight900"></td>     
							<td  class="font-weight900">Hungary</td>
							<td  class="font-weight900">North Macedonia</td>
							<td  class="font-weight900">Finland</td>
							<td  class="font-weight900">Slovakia</td>
							<td  class="font-weight900">Ukraine</td>
							<td  class="font-weight900">Czech Republic</td>
							<td  class="font-weight900">Russia</td>
							<td  class="font-weight900">Scotland</td>
							<td  class="font-weight900">Wales</td>
							<td  >Turkey</td>
							<td  class="font-weight900">Sweden</td>
							<td  class="font-weight900">Poland</td>
							<td  class="font-weight900">Austria</td>
							<td  class="font-weight900">Switzerland</td>
							<td  class="font-weight900">Croatia</td>
							<td  class="font-weight900">Denmark</td>
							<td  class="font-weight900">Netherlands</td>
							<td  class="font-weight900">Italy</td>
							<td  class="font-weight900">Belgium</td>
							<td  class="font-weight900">Germany</td>
							<td  class="font-weight900">Portugal</td>
							<td  class="font-weight900">Spain</td>
							<td  class="font-weight900">England</td>
							<td  class="font-weight900">France</td>           
						</tr>  -->
					</table>
				</div>
			</div>

    <div class="clear"></div>
    
</div>
<?php include 'js.php'; ?>

</body>
</html>