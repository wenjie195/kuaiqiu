<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// date_default_timezone_set('Asia/Kuala_Lumpur');
// $date = date('Y-m-d H:i', time());

// $dateOnly = date('Y-m-d', time());
$dateOnly = "2021-12-29";

$No = 0;
$curl = curl_init();

curl_setopt_array($curl, [
	CURLOPT_URL => "https://football-prediction-api.p.rapidapi.com/api/v2/predictions?market=classic&iso_date=".$dateOnly."&federation=UEFA",
	// CURLOPT_URL => "https://football-prediction-api.p.rapidapi.com/api/v2/predictions?market=classic&iso_date=2021-12-19&federation=UEFA",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => [
		"x-rapidapi-host: football-prediction-api.p.rapidapi.com",
		"x-rapidapi-key: 16c81199b8msh057448939d0cc57p135fd8jsn8c6fb196197a"
	],
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	$response;
	$exchangeRates = json_decode($response, true);
}
?>

<?php include 'css.php'; ?>

<div class="width100 same-padding min-height grey-bg menu-distance overflow ow-same-padding">

    <div class="width100 overflow-auto margin-top30">
        <table class="odds-table2 td-center no-break-text resize-table"  border="1" cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <td class="font-weight900 text-center td-title" >ID</td>
                <td class="font-weight900 text-center td-title" >赛事国家 - 赛事名称</td>
                <td class="font-weight900 text-center td-title" >主队 (Name/Strength)</td>
				<td class="font-weight900 text-center td-title" >客队 (Name/Strength)</td>
				<td class="font-weight900 text-center td-title" >Market</td>
				<td class="font-weight900 text-center td-title" >Prediction</td>
				<td class="font-weight900 text-center td-title" >Season</td>
				<td class="font-weight900 text-center td-title" >日期 / 时间</td>
                <td class="font-weight900 text-center td-title" >Stadium Capcity</td>
				<td class="font-weight900 text-center td-title" >Field (Length /Width)</td>
				<td class="font-weight900 text-center td-title" >Probabilities (1, X, 2, 1X, X2, 12)</td>
				<td class="font-weight900 text-center td-title" >Odds (1, X, 2, 1X, X2, 12)</td>
				<td class="font-weight900 text-center td-title" >Result</td>
				<td class="font-weight900 text-center td-title" >Distance Between Teams</td>
				<td class="font-weight900 text-center td-title" >Prediction Data</td>
				<td class="font-weight900 text-center td-title" >Head To Head</td>
				<td class="font-weight900 text-center td-title" >Home Past 10</td>
				<td class="font-weight900 text-center td-title" >Away Past 10</td>
            </tr>

            <?php
            if ($exchangeRates)
            {
                for ($cnt=0; $cnt <count($exchangeRates['data']) ; $cnt++)
                {
                ?>

                    <tr>                            
                        <td><?php echo $exchangeRates['data'][$cnt]['id']; ?></td> 
                        <td><?php echo $exchangeRates['data'][$cnt]['competition_cluster'];?> - <?php echo $exchangeRates['data'][$cnt]['competition_name']; ?></td> 

						<?php $homeStrength = $exchangeRates['data'][$cnt]['home_strength']; ?>
						<?php $awayStrength = $exchangeRates['data'][$cnt]['away_strength']; ?>

                        <td><?php echo $exchangeRates['data'][$cnt]['home_team']; ?> / <?php echo $homeStr = sprintf("%.5f", $homeStrength); ?></td> 
						<td><?php echo $exchangeRates['data'][$cnt]['away_team']; ?> / <?php echo $awayStr =  sprintf("%.5f", $awayStrength); ?></td> 

						<td><?php echo $exchangeRates['data'][$cnt]['market']; ?></td> 
						<td><?php echo $exchangeRates['data'][$cnt]['prediction']; ?></td> 
						<td><?php echo $exchangeRates['data'][$cnt]['season']; ?></td> 
                        <?php $datetime = $exchangeRates['data'][$cnt]['start_date']; ?>
                        <td><?php echo $date = date("Y-m-d H:i",strtotime($datetime));?></td> 
						<td><?php echo $exchangeRates['data'][$cnt]['stadium_capacity']; ?></td> 
						<td><?php echo $exchangeRates['data'][$cnt]['field_length']; ?> ， <?php echo $exchangeRates['data'][$cnt]['field_width']; ?></td> 
						
						<td>
							<?php if (isset($exchangeRates['data'][$cnt]['probabilities']))
							{
								// echo "Available Markets : ";
								foreach ($exchangeRates['data'][$cnt]['probabilities'] as $key => $market)
								{
									$asd = $market;
									$stringOne = $asd;
									$updatedCompName = str_replace('_', ' ', trim($stringOne));
									// echo "Available Markets :";
									echo $updatedCompName . ', ';
								}
							}
							?>
						</td> 

						<td>
							<?php if (isset($exchangeRates['data'][$cnt]['odds']))
							{
								// echo "Available Markets : ";
								foreach ($exchangeRates['data'][$cnt]['odds'] as $key => $market)
								{
									$asd = $market;
									$stringOne = $asd;
									$updatedCompName = str_replace('_', ' ', trim($stringOne));
									// echo "Available Markets :";
									// echo "1, X, 2, 1X, X2, 12";
									echo $updatedCompName . ', ';
								}
							}
							?>
						</td> 

						<td><?php echo $exchangeRates['data'][$cnt]['result']; ?></td> 
						<td><?php echo $exchangeRates['data'][$cnt]['distance_between_teams']; ?></td> 

						<td>
							<form method="POST" action="apiMatchPrediction.php" target="_blank">
								<input class="input-name clean" type="hidden" value="<?php echo $exchangeRates['data'][$cnt]['id']; ?>" name="match_id" id="match_id" readonly>
								<button class="clean rec-button" name="submit">Next</button>    
							</form>
						</td> 

						<td>
							<form method="POST" action="apiMatchHeadToHead.php" target="_blank">
								<input class="input-name clean" type="hidden" value="<?php echo $exchangeRates['data'][$cnt]['id']; ?>" name="match_id" id="match_id" readonly>
								<button class="clean rec-button" name="submit">Next</button>    
							</form>
						</td> 

						<td>
							<form method="POST" action="apiMatchHome10.php" target="_blank">
								<input class="input-name clean" type="hidden" value="<?php echo $exchangeRates['data'][$cnt]['id']; ?>" name="match_id" id="match_id" readonly>
								<button class="clean rec-button" name="submit">Next</button>    
							</form>
						</td> 
						<td>
							<form method="POST" action="apiMatchAway10.php" target="_blank">
								<input class="input-name clean" type="hidden" value="<?php echo $exchangeRates['data'][$cnt]['id']; ?>" name="match_id" id="match_id" readonly>
								<button class="clean rec-button" name="submit">Next</button>    
							</form>
						</td> 

                    </tr>

                <?php
                }
            }
            ?>

            </tbody>	
        </table>
    </div>

</div>