<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/PageView.php';
require_once dirname(__FILE__) . '/classes/Prediction.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// if($_SERVER['REQUEST_METHOD'] == 'POST')
// {
//     $conn = connDB();

//     $matchID = rewrite($_POST["match_id"]);
// 	$_SESSION['match_id'] = $matchID;

// 	// $allPrediction = getPrediction($conn);
// 	// $predictionDetails = getPrediction($conn,"WHERE fixture_id = ? ", array("fixture_id") ,array($matchID),"s");

// 	$originalUri = "https://football-prediction-api.p.rapidapi.com/api/v2/predictions/".$matchID."";
// }
// else 
// {
//     header('Location: ../index.php');
// }

date_default_timezone_set('Asia/Kuala_Lumpur');
$date = date('Y-m-d H:i', time());
$dateOnly = date('Y-m-d', time());
$timeOnly = date('H:i', time());

function addSession($conn,$ip_address,$page,$matchID,$matchName)
{
    if(insertDynamicData($conn,"pageview",array("ip_address","page","match_id","match_name"),
    array($ip_address,$page,$matchID,$matchName),"ssss") === null)
    {
        echo "gg";
    }
    else{    }
    return true;
}

//whether ip is from share internet
if (!empty($_SERVER['HTTP_CLIENT_IP']))   
{
    $ip_address = $_SERVER['HTTP_CLIENT_IP'];
}
//whether ip is from proxy
elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  
{
    $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
}
//whether ip is from remote address
else
{
    $ip_address = $_SERVER['REMOTE_ADDR'];
}
$ip_address;

$page = $_SERVER['REQUEST_URI'];

if(isset($_GET['id']))
{
	$conn = connDB();

	$matchID = $_GET['id'];
	$_SESSION['match_id'] = $matchID;

	$originalUri = "https://football-prediction-api.p.rapidapi.com/api/v2/predictions/".$matchID."";

	$matchDetails = getPrediction($conn,"WHERE fixture_id = ? ",array("fixture_id"),array($matchID), "s");
	$matchName = $matchDetails[0]->getNameEn();

	if(addSession($conn,$ip_address,$page,$matchID,$matchName))
    {}
}

// $matchID = $_SESSION['match_id'];
// $originalUri = "https://football-prediction-api.p.rapidapi.com/api/v2/predictions/".$matchID."";

$curl = curl_init();

curl_setopt_array($curl, [

	CURLOPT_URL => $originalUri,
	// CURLOPT_URL => "https://football-prediction-api.p.rapidapi.com/api/v2/predictions/172963",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => [
		"x-rapidapi-host: football-prediction-api.p.rapidapi.com",
		"x-rapidapi-key: 16c81199b8msh057448939d0cc57p135fd8jsn8c6fb196197a"
	],
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	// echo $response;
	$exchangeRates = json_decode($response, true);
}

?>

<!doctype html>
<html>

<!-- <head>
<?php //include 'meta.php'; ?>
<meta property="og:url" content="https://kuaiqiu.tech/matchData.php" />
<link rel="canonical" href="https://kuaiqiu.tech/matchData.php" />
<meta property="og:title" content="Prediction | Kuai Qiu" />
<title>Prediction | Kuai Qiu</title>
<?php //include 'css.php'; ?>
</head> -->

	<?php
    if($matchDetails)
    {
        for($cnt = 0;$cnt < count($matchDetails) ;$cnt++)
        {
        ?>

			<head>
				<?php include 'meta.php'; ?>
				<meta property="og:url" content="https://kuaiqiu.tech/matchPrediction.php" />
				<link rel="canonical" href="https://kuaiqiu.tech/matchPrediction.php" />
				<meta property="og:title" content="推荐 <?php echo $matchDetails[$cnt]->getNameEn();?> | MMQT" />
				<title>推荐 <?php echo $matchDetails[$cnt]->getNameEn();?> | MMQT</title>   
				<?php include 'css.php'; ?>
			</head>

        <?php
        }
        ?>
    <?php
    }
    ?>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding min-height grey-bg menu-distance overflow ow-same-padding">

	<?php
	if ($exchangeRates)
	{
		for ($cnt=0; $cnt <count($exchangeRates['data']) ; $cnt++)
		{
		?>
			<h1 class="black-text stadium-title text-center margin-bottom0">
				<?php echo $exchangeRates['data'][$cnt]['competition_cluster']; ?> - <?php echo $exchangeRates['data'][$cnt]['competition_name']; ?>
			</h1>

			<?php $datetime = $exchangeRates['data'][$cnt]['start_date']; ?>

			<?php $date = date("Y-m-d",strtotime($datetime));?>
			<?php $time = date("H:i",strtotime($datetime));?>

			<!-- <div class="text-center width100"></div> -->

			<div class="overflow-div width100\">
				<div class="width100 overflow text-center">
                    <h1 class="team-title team-title-left ow-black-text"><?php echo $exchangeRates['data'][$cnt]['home_team']; ?></h1>
                    <img src="img/vs.png" class="vs" alt="vs" title="vs">
                    <h1 class="team-title team-title-right ow-black-text"><?php echo $exchangeRates['data'][$cnt]['away_team']; ?></h1>
				</div>
			</div>

		<?php
		}
	}
	?>

    <div class="clear"></div>
    
	<div class="chat-section">
		<div id="divStatHomeAway"></div>
	</div>

    <div class="clear"></div>

	<?php
	// if($dateOnly > $datetime)
	if($dateOnly > $date)
	{
	?>
	
		<h1 class="black-text stadium-title text-center margin-bottom0">
		赛事成绩 : <?php echo $matchDetails[0]->getMatchResult();?>
		</h1>

	<?php
	}
	else
	{
		if($timeOnly > $time)
		{}
		else
		{
		?>
		
			<!-- <div class="chat-section">
				<div id="divStatHomeAway"></div>
			</div> -->

			<div class="clear"></div>

			<div class="chat-section">
				<div id="divPast10Home"></div>
			</div>

			<div class="clear"></div>

			<div class="chat-section">
				<div id="divPast10Away"></div>
			</div>
		
		<?php
		}
	}
	?>

	<!-- <div class="chat-section">
		<div id="divStatHomeAway"></div>
	</div>

	<div class="clear"></div>

	<div class="chat-section">
		<div id="divPast10Home"></div>
	</div>

	<div class="clear"></div>

	<div class="chat-section">
		<div id="divPast10Away"></div>
	</div> -->

	<div class="clear"></div>

	<div class="chat-section">
		<div id="prediction"></div>
	</div>

	<!-- <div class="chat-section">
		<div id="divStatHomeAway"></div>
	</div> -->
	<!-- AddToAny BEGIN -->
<!--    <div class="a2a_kit a2a_kit_size_32 a2a_default_style addtoany same-padding">
    <h2 class="share-h2">分享:</h2>
    <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
    <a class="a2a_button_copy_link"></a>
    <a class="a2a_button_facebook"></a>
    <a class="a2a_button_twitter"></a>
    <a class="a2a_button_email"></a>
    <a class="a2a_button_linkedin"></a>
    <a class="a2a_button_telegram"></a>
    <a class="a2a_button_facebook_messenger"></a>
    <a class="a2a_button_whatsapp"></a>
    <a class="a2a_button_line"></a>
    <a class="a2a_button_wechat"></a>
    </div>
    <script async src="https://static.addtoany.com/menu/page.js"></script>
    <style>
    	.addtoany{
			width:100%;
			overflow:hidden;
			text-align:center;}
		.addtoany a{
			display:inline-block !important;
			float:none !important;
			margin-bottom:10px;}
    </style>-->
    <!-- AddToAny END -->

</div>

<?php include 'js.php'; ?>

<script type="text/javascript">
    $(document).ready(function()
    {
        $("#divStatHomeAway").load("matchPrediction2.php");
    setInterval(function()
    {
        $("#divStatHomeAway").load("matchPrediction2.php");
    }, 100000);
    });
</script>

<script type="text/javascript">
    $(document).ready(function()
    {
        $("#divPast10Home").load("matchPrediction3.php");
    setInterval(function()
    {
        $("#divPast10Home").load("matchPrediction3.php");
    }, 100000);
    });
</script>

<script type="text/javascript">
    $(document).ready(function()
    {
        $("#divPast10Away").load("matchPrediction4.php");
    setInterval(function()
    {
        $("#divPast10Away").load("matchPrediction4.php");
    }, 100000);
    });
</script>

<script type="text/javascript">
    $(document).ready(function()
    {
        $("#prediction").load("matchPrediction5.php");
    setInterval(function()
    {
        $("#prediction").load("matchPrediction5.php");
    }, 100000);
    });
</script>

</body>
</html>