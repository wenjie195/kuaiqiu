<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/PageView.php';
require_once dirname(__FILE__) . '/classes/Prediction.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

// $allPrediction = getPrediction($conn, "ORDER BY date_created DESC");
$allPrediction = getPrediction($conn, "WHERE eat_let != '' ORDER BY date_created DESC");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://kuaiqiu.tech/adminDashboard.php" />
<link rel="canonical" href="https://kuaiqiu.tech/adminDashboard.php" />
<meta property="og:title" content="Admin Dashboard | Kuai Qiu" />
<title>Admin Dashboard | Kuai Qiu</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding min-height grey-bg menu-distance overflow">
    <h1 class="green-text h1-title margin-bottom0 text-center">Admin Dashboard</h1>  
    <div class="width100 text-center">
        <div class="green-border margin-bottom30 margin-auto"></div>
    </div>

    <form action="adminMatchAll.php" method="POST">
        <div class="width100 text-center margin-bottom30">
            <p class="input-top-p ow-grey-text">Date Selection</p>
            <input class="select-width" type="date" placeholder="Date" name="date" id="date" required>
        </div>
        <div class="width100 text-center">    
            <button class="pill-button green-bg white-text clean fix-width-button opacity-hover"  name="submit">提交</button> 
        </div>
    </form>      

    <div class="clear"></div>

    <?php
    $conn = connDB();
    $prediction = getPrediction($conn);
    // $predictionD2 = getPrediction($conn, " WHERE prediction_result = '-2' ");
    // $predictionD1 = getPrediction($conn, " WHERE prediction_result = '-1' ");
    // $predictionP1 = getPrediction($conn, " WHERE prediction_result = '+1' ");
    // $predictionP2 = getPrediction($conn, " WHERE prediction_result = '+2' ");

    // if($predictionD2)
    // {   
    //     $totalD2 = count($predictionD2);
    // }
    // else
    // {   $totalD2 = 0;   }

    if ($prediction)
    {
        $totalPts = 0; // initital
        for ($b=0; $b <count($prediction) ; $b++)
        {
            $totalPts += $prediction[$b]->getPredictionResult();
        }
    }

    // $predictionWin = getPrediction($conn, " WHERE prediction_result > 0 ");
    // $predictionDraw = getPrediction($conn, " WHERE prediction_result  = 0 ");
    // $predictionLose = getPrediction($conn, " WHERE prediction_result < 0 ");

    $predictionWin = getPrediction($conn, " WHERE prediction_result > 0 ");
    $predictionDraw = getPrediction($conn, " WHERE prediction_result  = 0 ");
    $predictionLose = getPrediction($conn, " WHERE prediction_result < 0 ");

    if($predictionWin)
    {   
        $totalWin = count($predictionWin);
    }
    else
    {   $totalWin = 0;   }

    if($predictionDraw)
    {   
        $totalDraw = count($predictionDraw);
    }
    else
    {   $totalDraw = 0;   }

    if($predictionLose)
    {   
        $totalLose = count($predictionLose);
    }
    else
    {   $totalLose = 0;   }


    // if ($predictionD2)
    // {
    //     $totalD2 = 0; // initital
    //     for ($b=0; $b <count($predictionD2) ; $b++)
    //     {
    //         $totalD2 += $predictionD2[$b]->getPredictionResult();
    //     }
    // }

    // if ($predictionD1)
    // {
    //     $totalD1 = 0; // initital
    //     for ($b=0; $b <count($predictionD1) ; $b++)
    //     {
    //         $totalD1 += $predictionD1[$b]->getPredictionResult();
    //     }
    // }

    // if ($predictionP1)
    // {
    //     $totalP1 = 0; // initital
    //     for ($b=0; $b <count($predictionP1) ; $b++)
    //     {
    //         $totalP1 += $predictionP1[$b]->getPredictionResult();
    //     }
    // }

    // if ($predictionP2)
    // {
    //     $totalP2 = 0; // initital
    //     for ($b=0; $b <count($predictionP2) ; $b++)
    //     {
    //         $totalP2 += $predictionP2[$b]->getPredictionResult();
    //     }
    // }
    // $totalPoints = $totalD2 + $totalD1 + $totalP1 + $totalP2;
    ?>

    <!-- <div class="width100 text-center">
        <h1 class="green-text stadium-title text-center">All Prediction (Total Points : <?php echo $totalPts;?> Pts | <?php echo $totalWin;?> W , <?php echo $totalDraw;?> D, <?php echo $totalLose;?> L )</h1>
    </div> -->

    <div class="width100 text-center">
        <h1 class="green-text stadium-title text-center">All Prediction ( <?php echo $totalWin;?> W , <?php echo $totalDraw;?> D, <?php echo $totalLose;?> L )</h1>
    </div>


    <div class="overflow-div width100 win-table-div">
        <table class="odds-table2 win-table">	
            <thead>
                <tr class="tr2">
                    <td class="td-title font-weight900" colspan="3"></td>
                    <td class="td-title font-weight900" colspan="3">Prediction</td>
                    <!-- <td class="td-title font-weight900" colspan="1"></td> -->
                    <td class="td-title font-weight900" colspan="4">Details</td>
                </tr>
                <tr class="tr2">
                    <!-- <td class="font-weight900 left-td1"></td> -->
                    <td class="td-title font-weight900">No.</td>
                    <td class="td-title font-weight900">Match Name</td>
                    <td class="td-title font-weight900">Date</td>
                    <!-- <td class="td-title font-weight900">Match ID</td> -->
                    <!-- <td class="td-title font-weight900">Winning Team</td> -->
                    <td class="td-title font-weight900">Eat/Let</td>
                    <td class="td-title font-weight900">Big/Small</td>
                    <!-- <td class="td-title font-weight900">CS</td> -->
                    <!-- <td class="td-title font-weight900">Date</td> -->
                    <td class="td-title font-weight900">Result</td>
                    <td class="td-title font-weight900">Total Views</td>
                    <td class="td-title font-weight900">Stats</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($allPrediction)
                    {
                        for($cnt = 0;$cnt < count($allPrediction) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $allPrediction[$cnt]->getNameEn();?></td>
                                <!-- <td><?php //echo $allPrediction[$cnt]->getFixtureId();?></td> -->

                                <td>
                                    <!-- <?php //echo date("Y-m-d H:i",strtotime($allPrediction[$cnt]->getDateCreated()));?> -->
                                    <?php echo date("Y-m-d",strtotime($allPrediction[$cnt]->getDateCreated()));?>
                                </td>

                                <!-- <td><?php echo $allPrediction[$cnt]->getWinningTeam();?></td> -->
                                <td><?php echo $allPrediction[$cnt]->getEatLet();?></td>
                                <td><?php echo $allPrediction[$cnt]->getBigSmall();?></td>
                                <!-- <td><?php //echo $allPrediction[$cnt]->getScore();?></td> -->

                                <td>
                                    <?php 
                                        $result =  $allPrediction[$cnt]->getPredictionResult();
                                        $matchResult =  $allPrediction[$cnt]->getMatchResult();
                                        // if($result != '' || $matchResult =='postponed')
                                        // if($result != '')
                                        if($matchResult != '')
                                        {
                                            // echo $result;
                                            echo $matchResult;
                                        }
                                        elseif($matchResult =='postponed')
                                        {
                                            // echo "Postponed";
                                            echo "赛事取消";
                                        }
                                        else
                                        {
                                        ?>
                                        
                                            <form action="utilities/adminUpdateResultFunction.php" method="POST" class="hover1">
                                                <!-- <p class="input-top-p ow-grey-text">Result</p> -->
                                                <input class="clean" type="text" placeholder="Match Result" name="match_result" id="match_result" required>
                                                <div class="clear"></div>
                                                <input class="clean" type="text" placeholder="Prediciton Result" name="prediciton_result" id="prediciton_result" required>
                                                <div class="clear"></div>
                                                <input class="clean" type="hidden" value="<?php echo $allPrediction[$cnt]->getUid();?>" name="item_uid" id="item_uid" readonly>
                                                <div class="clear"></div>
                                                <button class="clean rec-button" type="submit">
                                                    Submit
                                                </button>
                                            </form> 

                                            <div class="clear"></div>

                                            <!-- <form action="utilities/adminMatchPostponeFunction.php" method="POST" class="hover1">
                                                <button class="clean rec-button" type="submit">
                                                    Postponed
                                                </button>
                                            </form>  -->

                                        <?php
                                        }
                                    ?>
                                </td>

                                <td>
                                    <?php
                                    $conn = connDB();
                                    $fixtureId = $allPrediction[$cnt]->getFixtureId();
                                    $viewsDetails = getPageview($conn, "WHERE match_id = ? ",array("match_id"),array($fixtureId),"s");
                                    if($viewsDetails)
                                    {   
                                        $totalViews = count($viewsDetails);
                                    }
                                    else
                                    {   $totalViews = 0;   }
                                    ?>
                                    <?php echo $totalViews;?>
                                </td>

                                <td>
                                    <form action="adminPredictionDetails.php" method="POST" class="hover1">
                                        <button class="clean blue-ow-btn" type="submit" name="item_uid" value="<?php echo $allPrediction[$cnt]->getFixtureId();?>">
                                           Check
                                        </button>
                                    </form> 
                                </td>
                            </tr>
                        <?php
                        }
                    }
                ?>                                 
            </tbody>
        </table>	
    </div>

    <div class="clear"></div>
</div>

<?php include 'js.php'; ?>
<?php unset($_SESSION['date']);?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Prediction Result Updated ! "; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "FAIL !"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "ERROR !"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>