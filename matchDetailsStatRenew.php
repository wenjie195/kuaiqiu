<?php
if (session_id() == "")
{
    session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$matchID = $_SESSION['match_id'];
$originalUri = "https://football-prediction-api.p.rapidapi.com/api/v2/predictions/".$matchID."";

$curl = curl_init();

curl_setopt_array($curl, [

	CURLOPT_URL => $originalUri,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => [
		"x-rapidapi-host: football-prediction-api.p.rapidapi.com",
		"x-rapidapi-key: 16c81199b8msh057448939d0cc57p135fd8jsn8c6fb196197a"
	],
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	// echo $response;
	$exchangeRates = json_decode($response, true);
}

?>

<div class="overflow-div width100">
	<?php
	if ($exchangeRates)
	{
		for ($cnt=0; $cnt <count($exchangeRates['data']) ; $cnt++)
		{
		?>
			<h1 class="black-text stadium-title text-center margin-bottom0">
				<?php echo $exchangeRates['data'][$cnt]['competition_cluster']; ?> - <?php echo $exchangeRates['data'][$cnt]['competition_name']; ?>
			</h1>

			<?php $datetime = $exchangeRates['data'][$cnt]['start_date']; ?>

			<div class="text-center width100"></div>

			<div class="overflow-div width100\">
                    <h1 class="team-title team-title-left ow-black-text"><?php echo $exchangeRates['data'][$cnt]['home_team']; ?></h1>
                    <img src="img/vs.png" class="vs" alt="vs" title="vs">
                    <h1 class="team-title team-title-right ow-black-text"><?php echo $exchangeRates['data'][$cnt]['away_team']; ?></h1>
			</div>

		<?php
		}
	}
	?>

    <div class="clear"></div>
    
</div>
