<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// date_default_timezone_set('Asia/Kuala_Lumpur');
// $date = date('Y-m-d H:i', time());
// $dateOnly = date('Y-m-d', time());

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();
    $dateOnly = rewrite($_POST["date"]);
    $_SESSION['date'] = $dateOnly;
}
else
{
    // date_default_timezone_set('Asia/Kuala_Lumpur');
    // $dateOnly = date('Y-m-d', time());
    $dateOnly = $_SESSION['date'];
}

$No = 0;
$curl = curl_init();

curl_setopt_array($curl, [
	CURLOPT_URL => "https://football-prediction-api.p.rapidapi.com/api/v2/predictions?market=classic&iso_date=".$dateOnly."&federation=UEFA",
	// CURLOPT_URL => "https://football-prediction-api.p.rapidapi.com/api/v2/predictions?market=classic&iso_date=2021-12-19&federation=UEFA",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => [
		"x-rapidapi-host: football-prediction-api.p.rapidapi.com",
		"x-rapidapi-key: 16c81199b8msh057448939d0cc57p135fd8jsn8c6fb196197a"
	],
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	// echo $response;
	$exchangeRates = json_decode($response, true);
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://kuaiqiu.tech/match.php" />
<link rel="canonical" href="https://kuaiqiu.tech/match.php" />
<meta property="og:title" content="Match | Kuai Qiu" />
<title>Match | Kuai Qiu</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding min-height grey-bg menu-distance overflow ow-same-padding">

    <div class="width100 overflow-auto margin-top30">
        <table class="odds-table2 td-center no-break-text resize-table"  border="1" cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <td class="font-weight900 text-center td-title" >ID</td>
                <td class="font-weight900 text-center td-title" >赛事国家</td>
                <td class="font-weight900 text-center td-title" >赛事名称</td>
                <td class="font-weight900 text-center td-title" >主队vs客队</td>
                <td class="font-weight900 text-center td-title" >日期 / 时间</td>
                <!-- <td class="font-weight900 text-center td-title" >时间</td> -->
                <td class="font-weight900 text-center td-title" >推荐</td>
            </tr>

            <?php
            if ($exchangeRates)
            {
                for ($cnt=0; $cnt <count($exchangeRates['data']) ; $cnt++)
                {
                ?>

                    <tr>                            
                        <td><?php echo $exchangeRates['data'][$cnt]['id']; ?></td> 
                        <td><?php echo $exchangeRates['data'][$cnt]['competition_cluster']; ?></td> 
                        <td><?php echo $exchangeRates['data'][$cnt]['competition_name']; ?></td> 
                        <td><?php echo $exchangeRates['data'][$cnt]['home_team']; ?> vs <?php echo $exchangeRates['data'][$cnt]['away_team']; ?></td> 
                        <?php $datetime = $exchangeRates['data'][$cnt]['start_date']; ?>
                        <td><?php echo $date = date("Y-m-d H:i",strtotime($datetime));?></td> 
                        <td>

                        <form method="POST" action="utilities/adminMatchAddFunction.php" >
                            <input class="input-name clean" type="hidden" value="<?php echo $exchangeRates['data'][$cnt]['id']; ?>" name="match_id" id="match_id" readonly>
                            <input class="input-name clean" type="hidden" value="<?php echo $exchangeRates['data'][$cnt]['competition_cluster']; ?>" name="country" id="country" readonly>
                            <input class="input-name clean" type="hidden" value="<?php echo $exchangeRates['data'][$cnt]['competition_name']; ?>" name="league" id="league" readonly>
                            <input class="input-name clean" type="hidden" value="<?php echo $exchangeRates['data'][$cnt]['home_team']; ?>" name="home_team" id="home_team" readonly>
                            <input class="input-name clean" type="hidden" value="<?php echo $exchangeRates['data'][$cnt]['away_team']; ?>" name="away_team" id="away_team" readonly>
                            
                            <?php $homeStrength = $exchangeRates['data'][$cnt]['home_strength']; ?>
                            <?php $awayStrength = $exchangeRates['data'][$cnt]['away_strength']; ?>

                            <input class="input-name clean" type="hidden" value="<?php echo $homeStr = sprintf("%.3f", $homeStrength); ?>" name="home_strength" id="home_strength" readonly>
                            <input class="input-name clean" type="hidden" value="<?php echo $awayStr =  sprintf("%.3f", $awayStrength); ?>" name="away_strength" id="away_strength" readonly>
                            
                            <input class="input-name clean" type="hidden" value="<?php echo $date = date("Y-m-d",strtotime($datetime));?>" name="match_date" id="match_date" readonly>
                            <input class="input-name clean" type="hidden" value="<?php echo $date = date("H:i",strtotime($datetime));?>" name="match_time" id="match_time" readonly>
                            <button class="clean rec-button" name="submit">添加赛事</button>    
                        </form>

                        </td> 
                    </tr>

                <?php
                }
            }
            ?>

            </tbody>	
        </table>
    </div>

</div>

<?php include 'js.php'; ?>
<?php unset($_SESSION['match_id']);?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Prediction Added ! "; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Fail to submit Prediction !"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "The match already predicted, Pls Check !"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>