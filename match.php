<?php
if (session_id() == "")
{
    session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// if($_SERVER['REQUEST_METHOD'] == 'POST')
// {
//     $conn = connDB();

//     $federationType = rewrite($_POST["federation_type"]);}
// else 
// {
//     header('Location: ../index.php');
// }

date_default_timezone_set('Asia/Kuala_Lumpur');
$date = date('Y-m-d H:i', time());
$dateOnly = date('Y-m-d', time());

$No = 0;
$curl = curl_init();

curl_setopt_array($curl, [
	// CURLOPT_URL => "https://football-prediction-api.p.rapidapi.com/api/v2/predictions?market=classic&iso_date=2021-06-14&federation=AFC",
	CURLOPT_URL => "https://football-prediction-api.p.rapidapi.com/api/v2/predictions?market=classic&iso_date=".$dateOnly."&federation=UEFA",
    // CURLOPT_URL => "https://football-prediction-api.p.rapidapi.com/api/v2/predictions?market=classic&iso_date=2021-12-22&federation=UEFA",
    // CURLOPT_URL => "https://football-prediction-api.p.rapidapi.com/api/v2/predictions?market=classic&iso_date=".$dateOnly."&federation=".$federationType."",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => [
		"x-rapidapi-host: football-prediction-api.p.rapidapi.com",
		"x-rapidapi-key: 16c81199b8msh057448939d0cc57p135fd8jsn8c6fb196197a"
	],
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	// echo $response;
	$exchangeRates = json_decode($response, true);
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://kuaiqiu.tech/match.php" />
<link rel="canonical" href="https://kuaiqiu.tech/match.php" />
<meta property="og:title" content="Match | Kuai Qiu" />
<title>Match | Kuai Qiu</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding min-height stadium-bg menu-distance overflow">


    <?php
    if ($exchangeRates)
    {
        for ($cnt=0; $cnt <count($exchangeRates['data']) ; $cnt++)
        {
        ?>
        <!-- <h1 class="white-text stadium-title text-center margin-bottom0">ID: <?php //echo $exchangeRates['data'][$cnt]['id']; ?></h1> -->

        <!-- <div class="text-center width100">	
            <div class="white-border margin-auto"></div>  
        </div> -->

        <div class="width100 overflow glass vs-div">

            <h1 class="team-title team-title-left"><?php echo $exchangeRates['data'][$cnt]['home_team']; ?></h1>
                <img src="img/vs.png" class="vs" alt="vs" title="vs">
            <h1 class="team-title team-title-right"><?php echo $exchangeRates['data'][$cnt]['away_team']; ?></h1>

            <div class="clear"></div>

            <?php $datetime = $exchangeRates['data'][$cnt]['start_date']; ?>

            <div class="time-div">
                <table class="transparent-table time-table">
                    <tr>
                        <td><img src="img/date.png" class="time-png" alt="Date" title="Date"><p class="time-text"></td>
                        <td><p class="time-text"><?php echo $date = date("Y-m-d",strtotime($datetime));?></p></td>
                    </tr>
                    <tr>
                        <td><img src="img/time.png" class="time-png" alt="Date" title="Date"><p class="time-text"></td>
                        <td><p class="time-text"><?php echo $time = date("H:i",strtotime($datetime));?></p></td>
                    </tr>                
                </table>
            </div>

            <div class="clear"></div>

            <div class="width100 text-center">
                <!-- <form method="POST" action="matchDetails.php"> -->
                <form method="POST" action="matchData.php">
                <!-- <form method="POST" action="matchData.php" target="_blank"> -->
                <!-- <form method="POST" action="test2_edit.php" target="_blank"> -->
                    <!-- <button class="pill-button odds-button clean register-button opacity-hover" name="match_id" value="<?php echo $exchangeRates['data'][$cnt]['id']; ?>">
                        Check Odds
                    </button> -->
                    <button class="pill-button odds-button clean register-button opacity-hover" name="match_id" value="<?php echo $exchangeRates['data'][$cnt]['id']; ?>">
                        查看赔率
                    </button>
                </form>
            </div>  

        </div>
        
        <?php
        }
    }
    ?>

    <div class="clear"></div>
    
</div>
<?php include 'js.php'; ?>
<?php unset($_SESSION['match_id']);?>
</body>
</html>