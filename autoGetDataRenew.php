<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Matches.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// date_default_timezone_set('Asia/Kuala_Lumpur');
// $currentDate = date('Y-m-d H:i', time());

// $dateOnly = date('Y-m-d', time());

$currentDate = date('Y-m-d', time());
$dateOnly = date('Y-m-d',strtotime('+1 day ',strtotime($currentDate)));

function addPrediction($conn,$uid,$matchId,$homeTeam,$awayTeam,$matchDate,$matchTime,$datetimeDT,$country,$league,$homeStrength,$awayStrength)
{
     if(insertDynamicData($conn,"matches",array("uid","fixture_id","home_team","away_team","date","time","date_time","country","league","home_team_ch","away_team_ch"),
          array($uid,$matchId,$homeTeam,$awayTeam,$matchDate,$matchTime,$datetimeDT,$country,$league,$homeStrength,$awayStrength),"sssssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

// function addPrediction($conn,$uid,$s_matchID,$s_homeTeam,$s_awayTeam,$s_matchDate,$s_matchTime,$s_dateTime,$s_country,$s_league,$s_homeStrength,$s_awayStrength)
// {
//      if(insertDynamicData($conn,"matches",array("uid","fixture_id","home_team","away_team","date","time","date_time","country","league","home_team_ch","away_team_ch"),
//           array($uid,$s_matchID,$s_homeTeam,$s_awayTeam,$s_matchDate,$s_matchTime,$s_dateTime,$s_country,$s_league,$s_homeStrength,$s_awayStrength),"sssssssssss") === null)
//      {
//           echo "gg";
//      }
//      else{    }
//      return true;
// }

// if($_SERVER['REQUEST_METHOD'] == 'POST')
// {
//     $conn = connDB();
//     $dateOnly = rewrite($_POST["date"]);
//     $_SESSION['date'] = $dateOnly;
// }
// else
// {
//     // date_default_timezone_set('Asia/Kuala_Lumpur');
//     // $dateOnly = date('Y-m-d', time());
//     $dateOnly = $_SESSION['date'];
// }

$No = 0;
$curl = curl_init();

curl_setopt_array($curl, [
	CURLOPT_URL => "https://football-prediction-api.p.rapidapi.com/api/v2/predictions?market=classic&iso_date=".$dateOnly."&federation=UEFA",
    // CURLOPT_URL => "https://football-prediction-api.p.rapidapi.com/api/v2/predictions?market=classic&iso_date=2021-12-29&federation=UEFA",
	// CURLOPT_URL => "https://football-prediction-api.p.rapidapi.com/api/v2/predictions?market=classic&iso_date=2021-12-19&federation=UEFA",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => [
		"x-rapidapi-host: football-prediction-api.p.rapidapi.com",
		"x-rapidapi-key: 16c81199b8msh057448939d0cc57p135fd8jsn8c6fb196197a"
	],
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	$response;
	$exchangeRates = json_decode($response, true);
}
?>
		
<?php
if ($exchangeRates)
{
	for ($cnt=0; $cnt <count($exchangeRates['data']) ; $cnt++)
	{
		$conn = connDB();
		$uid = md5(uniqid());

		$matchId = $exchangeRates['data'][$cnt]['id']; 
		$country = $exchangeRates['data'][$cnt]['competition_cluster']; 
		$league = $exchangeRates['data'][$cnt]['competition_name']; 
		$homeTeam = $exchangeRates['data'][$cnt]['home_team']; 
		$awayTeam = $exchangeRates['data'][$cnt]['away_team']; 

		$datetime = $exchangeRates['data'][$cnt]['start_date']; 
		$matchDate = date("Y-m-d",strtotime($datetime));
		$matchTime = date("H:i",strtotime($datetime));
		$datetimeDT = date("Y-m-d H:i",strtotime($datetime));

		$homeStr = $exchangeRates['data'][$cnt]['home_strength'];
		$awayStr = $exchangeRates['data'][$cnt]['away_strength'];
		$homeStrength = sprintf("%.3f", $homeStr);
		$awayStrength =  sprintf("%.3f", $awayStr);

		// $s_matchID = $matchIDes;
		// $s_homeTeam = $homeTeam[$index];
		// $s_awayTeam = $awayTeam[$index];
		// $s_country = $country[$index];
		// $s_league = $league[$index];
		// $s_matchDate = $matchDate[$index];
		// $s_matchTime = $matchTime[$index];
		// $s_dateTime = $datetimeDT[$index];
		// $s_homeStrength = $homeStrength[$index];
		// $s_awayStrength = $awayStrength[$index];

		// if(addPrediction($conn,$uid,$s_matchID,$s_homeTeam,$s_awayTeam,$s_matchDate,$s_matchTime,$s_dateTime,$s_country,$s_league,$s_homeStrength,$s_awayStrength))
		// if(addPrediction($conn,$uid,$matchId,$homeTeam,$awayTeam,$matchDate,$matchTime,$datetimeDT,$country,$league,$homeStrength,$awayStrength))
		// {
		// 	echo "SUCCESS";
		// 	echo "<br>";
		// }
		// else
		// {
		// 	echo "FAIL";
		// }
		
		$fixtureRows = getMatches($conn," WHERE fixture_id = ? ",array("fixture_id"),array($matchId),"s");
		$existingFixtureId = $fixtureRows[0];
   
		if (!$existingFixtureId)
		{
			if(addPrediction($conn,$uid,$matchId,$homeTeam,$awayTeam,$matchDate,$matchTime,$datetimeDT,$country,$league,$homeStrength,$awayStrength))
			{
				echo "SUCCESS";
				echo "<br>";
			}
			else
			{
				echo "ERROR";
				echo "<br>";
			}
		}
		else
		{
			echo "Prediction Made, Pls Check";
			echo "<br>";
		}

	}

	// foreach($matchID as $index => $matchIDes)
	// {
	// 	$s_matchID = $matchIDes;
	// 	$s_homeTeam = $homeTeam[$index];
	// 	$s_awayTeam = $awayTeam[$index];
	// 	$s_country = $country[$index];
	// 	$s_league = $league[$index];

	// 	$s_matchDate = $matchDate[$index];
	// 	$s_matchTime = $matchTime[$index];
	// 	$s_dateTime = $datetimeDT[$index];
	// 	$s_homeStrength = $homeStrength[$index];
	// 	$s_awayStrength = $awayStrength[$index];

	// 	// if(addPrediction($conn,$uid,$s_matchID,$s_homeTeam,$s_awayTeam,$s_matchDate,$s_matchTime,$s_dateTime,$s_country,$s_league,$s_homeStrength,$s_awayStrength))
	// 	// {
	// 	// 	echo "SUCCESS";
	// 	// }
	// 	// else
	// 	// {
	// 	// 	echo "FAIL";
	// 	// }

	// }
}

// foreach($matchID as $index => $matchIDes)
// {
// 	$s_matchID = $matchIDes;
// 	$s_homeTeam = $homeTeam[$index];
// 	$s_awayTeam = $awayTeam[$index];
// 	$s_country = $country[$index];
// 	$s_league = $league[$index];

// 	$s_matchDate = $matchDate[$index];
// 	$s_matchTime = $matchTime[$index];
// 	$s_dateTime = $datetimeDT[$index];
// 	$s_homeStrength = $homeStrength[$index];
// 	$s_awayStrength = $awayStrength[$index];

// 	if(addPrediction($conn,$uid,$s_matchID,$s_homeTeam,$s_awayTeam,$s_matchDate,$s_matchTime,$s_dateTime,$s_country,$s_league,$s_homeStrength,$s_awayStrength))
// 	{
// 		echo "SUCCESS";
// 	}
// 	else
// 	{
// 		echo "FAIL";
// 	}

// }

// if($query_run)
// {
// 	echo "SUCCCESS";
// 	// header("Location: adminDashboard.php");
// }
// else
// {
// 	echo "FAIL";
// }
?>
	
