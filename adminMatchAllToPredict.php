<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Matches.php';
require_once dirname(__FILE__) . '/classes/Prediction.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

date_default_timezone_set('Asia/Kuala_Lumpur');
// $date = date('Y-m-d H:i', time());
// $dateSQL = '2021-12-08';
// $dateSQL = '2021-12-09';
// $dateSQL = '2021-12-19';
// $dateSQL = '2021-12-22';
$dateSQL = date('Y-m-d', time());
$dateDisplay = date('d/m/Y', time());

$twoWeeks = date('Y-m-d',strtotime('-14 days ',strtotime($dateSQL)));
$oneMonth = date('Y-m-d',strtotime('-1 month ',strtotime($dateSQL)));

$allMatches = getMatches($conn, " ORDER BY date_created DESC ");

// $epl = getMatches($conn, " WHERE country = 'England' AND league = 'Premier League' AND date = '$dateSQL' ");
// $laLiga = getMatches($conn, " WHERE country = 'Spain' AND league = 'Primera Division' AND date = '$dateSQL' ");
// $serieA = getMatches($conn, " WHERE country = 'Italy' AND league = 'Serie A' AND date = '$dateSQL' ");
// $bundesliga = getMatches($conn, " WHERE country = 'Germany' AND league = 'Bundesliga' AND date = '$dateSQL' ");
// $ligueOne = getMatches($conn, " WHERE country = 'France' AND league = 'Ligue 1' AND date = '$dateSQL' ");
// $ucl = getMatches($conn, " WHERE country = 'Champions League' AND date = '$dateSQL' ");
// $uel = getMatches($conn, " WHERE country = 'Europa League' AND date = '$dateSQL' ");
// $championship = getMatches($conn, " WHERE country = 'England' AND league = 'Championship' AND date = '$dateSQL' ");


// $allPrediction = getPrediction($conn, " WHERE eat_let != '' ");
// $predictionWin = getPrediction($conn, " WHERE prediction_result > 0 ");
// $twoWeeksPrediction = getPrediction($conn, "WHERE eat_let != '' AND date_created >= '$twoWeeks' AND date_created <= '$dateSQL' ");
// $twoWeeksPercentage = getPrediction($conn, "WHERE prediction_result = '+1' AND date_created >= '$twoWeeks' AND date_created <= '$dateSQL' ");
// $oneMonthPrediction = getPrediction($conn, "WHERE eat_let != '' AND date_created >= '$oneMonth' AND date_created <= '$dateSQL' ");
// $oneMonthPercentage = getPrediction($conn, "WHERE prediction_result = '+1' AND date_created >= '$oneMonth' AND date_created <= '$dateSQL' ");


$allPrediction = getPrediction($conn, " WHERE prediction_result != '' ");
$predictionWin = getPrediction($conn, " WHERE prediction_result > 0 ");
$twoWeeksPrediction = getPrediction($conn, "WHERE prediction_result != '' AND date_created >= '$twoWeeks' AND date_created <= '$dateSQL' ");
$twoWeeksPercentage = getPrediction($conn, "WHERE prediction_result = '+1' AND date_created >= '$twoWeeks' AND date_created <= '$dateSQL' ");
$oneMonthPrediction = getPrediction($conn, "WHERE prediction_result != '' AND date_created >= '$oneMonth' AND date_created <= '$dateSQL' ");
$oneMonthPercentage = getPrediction($conn, "WHERE prediction_result = '+1' AND date_created >= '$oneMonth' AND date_created <= '$dateSQL' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://kuaiqiu.tech/matches.php" />
<link rel="canonical" href="https://kuaiqiu.tech/matches.php" />
<meta property="og:title" content="赛事 | 超级智能预判赛果" />
<title>赛事 | 超级智能预判赛果</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding min-height grey-bg menu-distance overflow ow-same-padding">

    <?php
    if($allPrediction)
    {   
        $totalPrediction = count($allPrediction);
    }
    else
    {   $totalPrediction = 0;   }

    if($predictionWin)
    {   
        $totalWin = count($predictionWin);
    }
    else
    {   $totalWin = 0;   }
    $winPercentage = ($totalWin / $totalPrediction) * 100;



    if($twoWeeksPrediction)
    {   
        $totalPrediction2Weeks = count($twoWeeksPrediction);
    }
    else
    {   $totalPrediction2Weeks = 0;   }

    if($twoWeeksPercentage)
    {   
        $totalWin2Weeks = count($twoWeeksPercentage);
    }
    else
    {   $totalWin2Weeks = 0;   }
    $winPercentage2Weeks = (($totalWin2Weeks / $totalPrediction2Weeks) * 100);



    if($oneMonthPrediction)
    {   
        $totalPrediction1Month = count($oneMonthPrediction);
    }
    else
    {   $totalPrediction1Month = 0;   }

    if($oneMonthPercentage)
    {   
        $totalWinOneMonth = count($oneMonthPercentage);
    }
    else
    {   $totalWinOneMonth = 0;   }
    $winPercentageOneMonth = (($totalWinOneMonth / $totalPrediction1Month) * 100);
    ?>

    <table class="odds-table2 odds-table3 resize-table">
        <tbody>
            <tr>
                <td class="font-weight900 text-center td-title" >赢率（累计）</td>
                <td class="font-weight900 text-center td-title" >赢率（近一个月）</td>
                <td class="font-weight900 text-center td-title" >赢率（近两周）</td>
            </tr>
            <tr>
 

                <td class="text-center bold-td"><?php echo number_format((float)$winPercentage, 2, '.', '');?>%</td>
                <td class="text-center bold-td"><?php echo number_format((float)$winPercentageOneMonth, 2, '.', '');?>%</td>
                <td class="text-center bold-td"><?php echo number_format((float)$winPercentage2Weeks, 2, '.', '');?>%</td>
            </tr>
        </tbody>	
    </table>

    <h1 class="black-text stadium-title text-center resize-title">今日日期： <?php echo $dateSQL;?></h1>

    <?php
        if($allMatches)
        {
        ?>
            <div class="width100 overflow-auto margin-top30">
                <table class="odds-table2 td-center no-break-text resize-table"  border="1" cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td class="font-weight900 text-center td-title" >No.</td>
                        <td class="font-weight900 text-center td-title" >赛事名称</td>
                        <td class="font-weight900 text-center td-title" >主队vs客队</td>

                        <td class="font-weight900 text-center td-title" >日期</td>
                        <td class="font-weight900 text-center td-title" >时间</td>
                      
                        <td class="font-weight900 text-center td-title" >推荐</td>
                    </tr>
                    <?php
                        for($cnt = 0;$cnt < count($allMatches) ;$cnt++)
                        {
                        ?>    
                            <tr>                            
                                <td>比赛 <?php echo ($cnt+1)?></td>
                                <td><?php echo $allMatches[$cnt]->getLeague();?></td> 
                                <td><?php echo $allMatches[$cnt]->getHomeTeam();?> vs <?php echo $allMatches[$cnt]->getAwayTeam();?></td>
                                <td><?php echo $allMatches[$cnt]->getDate();?></td> 
                                <td><?php echo $allMatches[$cnt]->getTime();?></td> 

                                <td>
                                    <?php 
                                        $fixutreId = $allMatches[$cnt]->getFixtureId();
                                        $fixtureDate = $allMatches[$cnt]->getDate();

                                        $conn = connDB();
                                        $prediction = getPrediction($conn," WHERE fixture_id = ? ",array("fixture_id"),array($fixutreId),"s"); 

                                        if($dateSQL > $fixtureDate)
                                        {
                                            if($prediction)
                                            {
                                            ?>
                                                <a href='matchPrediction.php?id=<?php echo $allMatches[$cnt]->getFixtureId();?>'>
                                                    <button class="clean rec-button">
                                                        查看推荐
                                                    </button>
                                                </a>
                                            <?php
                                            }
                                            else
                                            {
                                                echo "无推荐";
                                            }   
                                        }
                                        else
                                        {
                                            if($prediction)
                                            {
                                                // echo "查看推荐 / 点击查看";
                                            ?>

                                                <a href='matchPrediction.php?id=<?php echo $allMatches[$cnt]->getFixtureId();?>'>
                                                    <button class="clean rec-button">
                                                        查看推荐
                                                    </button>
                                                </a>
                                            <?php
                                            }  
                                            else
                                            {
                                            ?>
                                            
                                                <form method="POST" action="adminMatchAddPrediction.php">
                                                    <button class="clean rec-button" name="match_id" value="<?php echo $allMatches[$cnt]->getFixtureId();?>">
                                                        发布推荐
                                                    </button>
                                                </form>

                                            <?php
                                            }  
                                        }                                     
                                    ?>
                                </td> 
                                
                            </tr>
                        <?php
                        }
                    ?>
                    </tbody>	
                </table>
            </div>
        <?php
        }
        else
        {   }
    ?>  

</div>

<?php include 'js.php'; ?>

<?php unset($_SESSION['match_id']);?>
<?php unset($_SESSION['date']);?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Prediction Added ! "; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Fail to submit Prediction !"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "The match already predicted, Pls Check !"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<style>
::-webkit-input-placeholder { /* Edge */
  color: #eaeaea;
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: #eaeaea;
}

::placeholder {
  color: #eaeaea;
}
</style>

</body>
</html>