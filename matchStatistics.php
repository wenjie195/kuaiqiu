<?php
if (session_id() == "")
{
    session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Matches.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

date_default_timezone_set('Asia/Kuala_Lumpur');
$dateOnly = date('Y-m-d', time());

if(isset($_GET['id']))
{
	$conn = connDB();

	$matchID = $_GET['id'];
	$_SESSION['match_id'] = $matchID;

    $matchDetails = getMatches($conn,"WHERE fixture_id = ? ",array("fixture_id"),array($matchID), "s");
	$matchDate = $matchDetails[0]->getDate();
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://kuaiqiu.tech/matchData.php" />
<link rel="canonical" href="https://kuaiqiu.tech/matchData.php" />
<meta property="og:title" content="数据 | Kuai Qiu" />
<title>数据 | Kuai Qiu</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

	<div class="width100 same-padding min-height grey-bg menu-distance overflow ow-same-padding">

		<div class="chat-section">
			<div id="divStat"></div>
		</div>

        <div class="chat-section">
            <div id="divStatHomeAway"></div>
        </div>

        <?php
        if($dateOnly > $matchDate)
        {
        ?>
        

        <?php
        }
        else
        {
        ?>
            <div class="chat-section">
                <div id="divPast10Home"></div>
            </div>

            <div class="clear"></div>

            <div class="chat-section">
                <div id="divPast10Away"></div>
            </div>
        <?php
        }
        ?>
        
        <!-- <div class="chat-section">
            <div id="divPast10Home"></div>
        </div>

        <div class="clear"></div>

        <div class="chat-section">
            <div id="divPast10Away"></div>
        </div> -->

	</div>

<?php include 'js.php'; ?>

<!-- <script type="text/javascript">
    $(document).ready(function()
    {
        $("#divStat").load("matchDetailsStat.php");
    setInterval(function()
    {
        $("#divStat").load("matchDetailsStat.php");
    }, 100000);
    });
</script> -->

<script type="text/javascript">
    $(document).ready(function()
    {
        $("#divStat").load("matchDetailsStatRenew.php");
    setInterval(function()
    {
        $("#divStat").load("matchDetailsStatRenew.php");
    }, 100000);
    });
</script>


<script type="text/javascript">
    $(document).ready(function()
    {
        $("#divStatHomeAway").load("matchDataHomeAwayRenew.php");
    setInterval(function()
    {
        $("#divStatHomeAway").load("matchDataHomeAwayRenew.php");
    }, 100000);
    });
</script>

<script type="text/javascript">
    $(document).ready(function()
    {
        $("#divPast10Home").load("matchPrediction3.php");
    setInterval(function()
    {
        $("#divPast10Home").load("matchPrediction3.php");
    }, 100000);
    });
</script>

<script type="text/javascript">
    $(document).ready(function()
    {
        $("#divPast10Away").load("matchPrediction4.php");
    setInterval(function()
    {
        $("#divPast10Away").load("matchPrediction4.php");
    }, 100000);
    });
</script>

</body>
</html>