<?php
if (session_id() == "")
{
    session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $matchID = "150712";
$matchID = $_SESSION['match_id'];

$originalUri = "https://football-prediction-api.p.rapidapi.com/api/v2/head-to-head/".$matchID."?limit=10";

$curl = curl_init();

curl_setopt_array($curl, [
	// CURLOPT_URL => "https://football-prediction-api.p.rapidapi.com/api/v2/head-to-head/150712?limit=10",
	CURLOPT_URL => $originalUri,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => [
		"x-rapidapi-host: football-prediction-api.p.rapidapi.com",
		"x-rapidapi-key: 16c81199b8msh057448939d0cc57p135fd8jsn8c6fb196197a"
	],
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err)
{
	echo "cURL Error #:" . $err;
}
else
{
	// echo $response;
	// echo '<br><br>';
	$exchangeRates = json_decode($response, true);
}
?>

<!-- <div class="overflow-div width100 margin-top50"> -->
<div class="overflow-div width100">
	<table class="odds-table2 odds-table3">
		<tr><td class="font-weight900 text-center td-title" colspan="100%">近 10 场交手记录</td></tr>
		<?php
			foreach ($exchangeRates as $key => $value)
			{
				foreach ($value['stats']['overall'] as $key2 => $value2)
				{
					$asd = $key2;
					$string = $asd;
					$updatedKey2 = str_replace('_', ' ', trim($string));
					echo '<td><p class="td-top">'.$updatedKey2 . '</p><p class="td-bottom">'.$value2 . '</p></td>';
				}
			}
		?>
	</table>
</div>
<div class="overflow-div width100">
    <table class="odds-table2 odds-table3 string-table ow-oddss ow-oddssxx">
        <tr>
			<?php
				foreach ($exchangeRates as $key => $value)
				{
					foreach ($value['stats']['home_team'] as $key3 => $value3)
					{
						// $asd = $key3;
						// $stringOne = $asd;
						// $updatedKey3 = str_replace('_', ' ', trim($stringOne));
						echo '<td class="data-td1"><p class="ow-tdp">'.$value3 . '</p></td>';
						echo '';
					}
				}
			?>
            
			</tr>
			
			<!-- <tr>
				<?php
					foreach ($exchangeRates as $key => $value)
					{
						foreach ($value['stats']['home_team'] as $key3 => $value3)
						{
							$asd = $key3;
							$stringOne = $asd;
							$updatedKey3 = str_replace('_', ' ', trim($stringOne));
							echo '<td class="font-weight900 text-center td-left1 td-title white-text"><p class="ow-tdp ow-tdtitle">'.$updatedKey3 . '</p></td>';
							echo '';
						}
					}
				?>
			</tr> -->
			
			<tr>
				<td class="font-weight900 text-center td-left1 td-title white-text"><p class="ow-tdp ow-tdtitle">队名</p></td>
				<td class="font-weight900 text-center td-left1 td-title white-text"><p class="ow-tdp ow-tdtitle">进球</p></td>
				<td class="font-weight900 text-center td-left1 td-title white-text"><p class="ow-tdp ow-tdtitle">失球</p></td>
				<td class="font-weight900 text-center td-left1 td-title white-text"><p class="ow-tdp ow-tdtitle">赢</p></td>
				<td class="font-weight900 text-center td-left1 td-title white-text"><p class="ow-tdp ow-tdtitle">和</p></td>
				<td class="font-weight900 text-center td-left1 td-title white-text"><p class="ow-tdp ow-tdtitle">输</p></td>
				<td class="font-weight900 text-center td-left1 td-title white-text"><p class="ow-tdp ow-tdtitle">不失球场次</p></td>
				<td class="font-weight900 text-center td-left1 td-title white-text"><p class="ow-tdp ow-tdtitle">上半场获胜</p></td>
				<td class="font-weight900 text-center td-left1 td-title white-text"><p class="ow-tdp ow-tdtitle">上半场打和</p></td>
				<td class="font-weight900 text-center td-left1 td-title white-text"><p class="ow-tdp ow-tdtitle">上半场输球</p></td>
				<td class="font-weight900 text-center td-left1 td-title white-text"><p class="ow-tdp ow-tdtitle">平均进球率</p></td>
				<td class="font-weight900 text-center td-left1 td-title white-text"><p class="ow-tdp ow-tdtitle">平均失球率</p></td>
				<td class="font-weight900 text-center td-left1 td-title white-text"><p class="ow-tdp ow-tdtitle">获胜平均率</p></td>
				<td class="font-weight900 text-center td-left1 td-title white-text"><p class="ow-tdp ow-tdtitle">打和平均率</p></td>
				<td class="font-weight900 text-center td-left1 td-title white-text"><p class="ow-tdp ow-tdtitle">输球平均率</p></td>
			</tr>

			<tr>   

 			<?php
				foreach ($exchangeRates as $key => $value)
				{           
					foreach ($value['stats']['away_team'] as $key4 => $value4)
					{
						$asd2 = $key4;
						$stringTwo = $asd2;
						$updatedKey4 = str_replace('_', ' ', trim($stringTwo));
						echo '<td class="data-td1"><p class="ow-tdp">'.$value4 . '</p></td>';
					echo '';
					}
				}
			?>
				</tr>
    </table>
</div>
<div class="overflow-div width100">	
    
    
    <!-- <table class="odds-table2 odds-table3 center-text-padding"><tr><td class="font-weight900 text-center td-title" colspan="100%">Head To Head</td></tr> -->
	<table class="odds-table2 odds-table3 center-text-padding"><tr><td class="font-weight900 text-center td-title" colspan="100%">交手记录</td></tr>
			<tr>
            <!-- <td><p class="font-weight900">First Half Result</p></td>
			<td><p class="font-weight900">Season</p></td>
			<td><p class="font-weight900">Start Date</p></td>
			<td><p class="font-weight900">Home Team</p></td>
			<td><p class="font-weight900">Away Team</p></td>
			<td><p class="font-weight900">Competition Name</p></td>
			<td><p class="font-weight900">Fulltime Result</p></td>
			<td><p class="font-weight900">Competition Cluster</p></td> -->
			<td><p class="font-weight900">上半场成绩</p></td>
			<td><p class="font-weight900">赛季</p></td>
			<td><p class="font-weight900">赛事名称</p></td>
			<td><p class="font-weight900">日期</p></td>
			<td><p class="font-weight900">客队</p></td>
			<td><p class="font-weight900">客队</p></td>
			<td><p class="font-weight900">最终成绩</p></td>
			<td><p class="font-weight900">赛事主办协会</p></td>
            </tr>
    
    	<?php
			

			foreach ($exchangeRates['data']['encounters'] as $key5 => $value5)
			{
				// echo '<table class="odds-table2 odds-table3"><tr><td class="font-weight900 text-center td-title" colspan="100%">Head To Head</td></tr>';
				echo '<tr></tr>';
				foreach ($value5 as $key6 => $value6)
				{
					$asd6 = $key6;
					$stringSix = $asd6;
					$updatedKey6 = str_replace('_', ' ', trim($stringSix));
					// echo '<td><p class="td-top">'.$updatedKey6 . '</p><p class="td-bottom">'.$value6 . '</p></td>';
					echo '<td><p class="td-bottom">'.$value6 . '</p></td>';
				}
			}
		?>
	</table>
</div>
	