<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://kuaiqiu.tech/indexEn.php" />
<link rel="canonical" href="https://kuaiqiu.tech/indexEn.php" />
<meta property="og:title" content="Kuai Qiu | Super Intelligence Gives Steady and Strong Prediction" />
<title>Kuai Qiu | Super Intelligence Gives Steady and Strong Prediction</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 min-height grey-bg overflow menu-distance">
	<img src="img/banner-en.jpg" class="width100">
    <div class=" same-padding width100 overflow">
    	<div class="width100 margin-top50 text-center en-ow">
        	<p class="link-p"><a href="index.php" class="green-text opacity-hover">切换成中文版</a></p>
            <div class="clear"></div>
        	<div class="left-div1">
            	<img src="img/pic2.png" class="index-img">
            </div>
            <div class="right-div1">
            	<p class="index-content-p">KuaiQiu uses the latest Super Intelligence which is one step ahead of Artificial Intelligence which uses emotions, feelings and also news regarding the players’ performance and then it will have its own predictions on how the team will perform.</p>
            </div>
            <div class="clear"></div>
        	<div class="left-div1 left-div2">
            	<img src="img/pic1.png" class="index-img">
            </div>
            <div class="right-div1 right-div2">
            	<p class="index-content-p">Additionally, KuaiQiu uses a mixed statistical and probabilistic model from past economic models to shape the AI where it has its brains based on data alone.  Why we call KuaiQiu a Super Intelligence is because it knows how to think and compare all information on the hand before making any decision that it thinks will happen.</p>
            </div>            
            <div class="clear"></div>
        	<div class="left-div1">
            	<img src="img/pic3.png" class="index-img">
            </div>
            <div class="right-div1">
            	<p class="index-content-p">From the statistics data, player information, team form, home advantage and also other various factors, KuaiQiu Super Intelligence is able to give steady and strong prediction.</p>
            </div>            
            <div class="clear"></div>            
        </div>
    	<div class="width100 text-center margin-top50">
        	<h1 class="green-text h1-title margin-bottom0 text-center">Statistical Models used</h1>  
            <div class="width100 text-center">
                <div class="green-border margin-bottom30 margin-auto"></div>
            </div>
            <div class="width100 overflow margin-top50">
            	<div class="three-div">
                	<img src="img/icon1.png" class="three-div-img" alt="Black-Litterman" title="Black-Litterman">
                    <p class="three-div-title">Black-Litterman</p>
                    <p class="three-div-content">Base performance and team scoring</p>
                </div>
            	<div class="three-div">
                	<img src="img/icon2.png" class="three-div-img" alt="Random-Forest" title="Random-Forest">
                    <p class="three-div-title">Random-Forest</p>
                    <p class="three-div-content">Deterministic decision making</p>
                </div>                
             	<div class="three-div">
                	<img src="img/icon3.png" class="three-div-img" alt="Random-Cloud Sampling" title="Random-Cloud Sampling">
                    <p class="three-div-title">Random-Cloud Sampling</p>
                    <p class="three-div-content">Shaper AI Data Pruning and Selection</p>
                </div>               
            	<div class="three-div">
                	<img src="img/icon4.png" class="three-div-img" alt="Bayesian Interpretations" title="Bayesian Interpretations">
                    <p class="three-div-title">Bayesian Interpretations</p>
                    <p class="three-div-content">Philosophical Understanding and Statistics</p>
                </div>
            	<div class="three-div">
                	<img src="img/icon5.png" class="three-div-img" alt="Weighted-Swarm Selection" title="Weighted-Swarm Selection">
                    <p class="three-div-title">Weighted-Swarm Selection</p>
                    <p class="three-div-content">Latest AI Capabilities</p>
                </div>                
             	<div class="three-div">
                	<img src="img/icon6.png" class="three-div-img" alt="Weighted-Average News Indicators" title="Weighted-Average News Indicators">
                    <p class="three-div-title">Weighted-Average News Indicators</p>
                    <p class="three-div-content">News Reader</p>
                </div>  
                <div class="clear"></div>                  
                <h1 class="green-text h1-title margin-bottom0 text-center margin-top50">Games predicted correctly on 1st June 2021</h1>  
                <div class="width100 text-center">
                    <div class="green-border margin-bottom30 margin-auto"></div>
                </div>                
                <img src="img/61.png" class="three-div-img ow-big-img" >
                <p class="ow-big-text">(Out of 100 Match)</p>
            </div>
        </div>
    
    
    </div>
</div>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully ! "; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Your registration is fail !"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<style>
::-webkit-input-placeholder { /* Edge */
  color: #eaeaea;
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: #eaeaea;
}

::placeholder {
  color: #eaeaea;
}
</style>

</body>
</html>